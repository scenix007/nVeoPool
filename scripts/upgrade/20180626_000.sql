----
---- Drop indexes
----
DROP INDEX "public"."IDX_AcceptedShare_MinerPublicKeyMinerWorkerId";

DROP INDEX "public"."IDX_AcceptedShareArchive_MinerPublicKeyMinerWorkerId";

----
---- Add indexes
----
CREATE INDEX CONCURRENTLY "IDX_AcceptedShare_MinerWorkerId" ON "public"."AcceptedShare" USING "btree" ("MinerWorkerId" "text_pattern_ops");

CREATE INDEX CONCURRENTLY "IDX_AcceptedShareArchive_MinerWorkerId" ON "public"."AcceptedShareArchive" USING "btree" ("MinerWorkerId" "text_pattern_ops");

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180626_000.sql', EXTRACT(epoch FROM now()));