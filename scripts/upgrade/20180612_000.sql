----
---- Add indexes
----
CREATE INDEX CONCURRENTLY "IDX_AcceptedShareArchive_BlockHeight" ON "public"."AcceptedShareArchive" USING "btree" ("BlockHeight" DESC NULLS LAST);

CREATE INDEX CONCURRENTLY "IDX_AcceptedShareArchive_MinerPublicKeyMinerWorkerId" ON "public"."AcceptedShareArchive" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE INDEX CONCURRENTLY "IDX_AcceptedShareArchive_SubmittedOn_DESC" ON "public"."AcceptedShareArchive" USING "btree" ("SubmittedOn" DESC NULLS LAST);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180612_000.sql', EXTRACT(epoch FROM now()));