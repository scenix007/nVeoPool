----
---- Create tables
----
--
-- BlockBonus
--
CREATE TABLE "public"."BlockBonus" (
    "Amount" numeric NOT NULL,
    "AmountIsPercent" boolean NOT NULL,
    "Id" smallint NOT NULL,
    "IsActive" boolean NOT NULL,
    "Remaining" numeric NOT NULL,
    "TotalPaid" numeric NOT NULL
);
ALTER TABLE "public"."BlockBonus" OWNER TO "postgres";
CREATE SEQUENCE "public"."BlockBonus_Id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."BlockBonus_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."BlockBonus_Id_seq" OWNED BY "public"."BlockBonus"."Id";
ALTER TABLE ONLY "public"."BlockBonus" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."BlockBonus_Id_seq"'::"regclass");

--
-- BlockRewardBonus
--
CREATE TABLE "public"."BlockRewardBonus" (
    "BlockBonusId" smallint NOT NULL,
    "BlockHeight" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Reward" numeric NOT NULL
);
ALTER TABLE "public"."BlockRewardBonus" OWNER TO "postgres";

--
-- SeriesHashrate3600
--
CREATE TABLE "public"."SeriesHashrate3600" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate3600" OWNER TO "postgres";

--
-- SeriesHashrate21600
--
CREATE TABLE "public"."SeriesHashrate21600" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate21600" OWNER TO "postgres";

--
-- SeriesHashrate43200
--
CREATE TABLE "public"."SeriesHashrate43200" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate43200" OWNER TO "postgres";

--
-- SeriesHashrate86400
--
CREATE TABLE "public"."SeriesHashrate86400" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate86400" OWNER TO "postgres";

--
-- SeriesPoolHashrate3600
--
CREATE TABLE "public"."SeriesPoolHashrate3600" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate3600" OWNER TO "postgres";

--
-- SeriesPoolHashrate21600
--
CREATE TABLE "public"."SeriesPoolHashrate21600" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate21600" OWNER TO "postgres";

--
-- SeriesPoolHashrate43200
--
CREATE TABLE "public"."SeriesPoolHashrate43200" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate43200" OWNER TO "postgres";

--
-- SeriesPoolHashrate86400
--
CREATE TABLE "public"."SeriesPoolHashrate86400" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate86400" OWNER TO "postgres";

--
-- SeriesPoolShares3600
--
CREATE TABLE "public"."SeriesPoolShares3600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares3600" OWNER TO "postgres";

--
-- SeriesPoolShares21600
--
CREATE TABLE "public"."SeriesPoolShares21600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares21600" OWNER TO "postgres";

--
-- SeriesPoolShares43200
--
CREATE TABLE "public"."SeriesPoolShares43200" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares43200" OWNER TO "postgres";

--
-- SeriesPoolShares86400
--
CREATE TABLE "public"."SeriesPoolShares86400" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares86400" OWNER TO "postgres";

--
-- SeriesPoolWorkers3600
--
CREATE TABLE "public"."SeriesPoolWorkers3600" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers3600" OWNER TO "postgres";

--
-- SeriesPoolWorkers21600
--
CREATE TABLE "public"."SeriesPoolWorkers21600" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers21600" OWNER TO "postgres";

--
-- SeriesPoolWorkers43200
--
CREATE TABLE "public"."SeriesPoolWorkers43200" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers43200" OWNER TO "postgres";

--
-- SeriesPoolWorkers86400
--
CREATE TABLE "public"."SeriesPoolWorkers86400" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers86400" OWNER TO "postgres";

--
-- SeriesShares3600
--
CREATE TABLE "public"."SeriesShares3600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares3600" OWNER TO "postgres";

--
-- SeriesShares21600
--
CREATE TABLE "public"."SeriesShares21600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares21600" OWNER TO "postgres";

--
-- SeriesShares43200
--
CREATE TABLE "public"."SeriesShares43200" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares43200" OWNER TO "postgres";

--
-- SeriesShares86400
--
CREATE TABLE "public"."SeriesShares86400" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares86400" OWNER TO "postgres";

--
-- SeriesWorkers3600
--
CREATE TABLE "public"."SeriesWorkers3600" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers3600" OWNER TO "postgres";

--
-- SeriesWorkers21600
--
CREATE TABLE "public"."SeriesWorkers21600" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers21600" OWNER TO "postgres";

--
-- SeriesWorkers43200
--
CREATE TABLE "public"."SeriesWorkers43200" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers43200" OWNER TO "postgres";

--
-- SeriesWorkers86400
--
CREATE TABLE "public"."SeriesWorkers86400" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers86400" OWNER TO "postgres";

----
---- Modify existing tables
----
ALTER TABLE "public"."BlockReward" DROP CONSTRAINT "PK_BlockReward";

ALTER TABLE "public"."BlockReward" ADD COLUMN "IsFeeRecipient" BOOLEAN NOT NULL DEFAULT false;

ALTER TABLE "public"."PaymentTransaction" DROP CONSTRAINT "PK_PaymentTransaction";

----
---- Add constraints
----
ALTER TABLE ONLY "public"."BlockBonus" ADD CONSTRAINT "CHK_BlockBonus_Amount" CHECK ("Amount" > 0);

ALTER TABLE ONLY "public"."BlockBonus" ADD CONSTRAINT "CHK_BlockBonus_Remaining" CHECK ("Remaining" >= 0);

ALTER TABLE ONLY "public"."BlockBonus" ADD CONSTRAINT "PK_BlockBonus" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."BlockReward" ADD CONSTRAINT "PK_BlockReward" PRIMARY KEY ("BlockHeight", "MinerPublicKey", "IsFeeRecipient");

ALTER TABLE ONLY "public"."BlockRewardBonus" ADD CONSTRAINT "FK_BlockRewardBonus_BlockBonus" FOREIGN KEY ("BlockBonusId") REFERENCES "public"."BlockBonus"("Id");

ALTER TABLE ONLY "public"."BlockRewardBonus" ADD CONSTRAINT "PK_BlockRewardBonus" PRIMARY KEY ("BlockHeight", "MinerPublicKey", "BlockBonusId");

ALTER TABLE ONLY "public"."PaymentTransaction" ADD CONSTRAINT "PK_PaymentTransaction" PRIMARY KEY ("MinerPublicKey", "TransactionId");

----
---- Add indexes
----
CREATE INDEX "IDX_SeriesHashrate3600_COVER" ON "public"."SeriesHashrate3600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesHashrate21600_COVER" ON "public"."SeriesHashrate21600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesHashrate43200_COVER" ON "public"."SeriesHashrate43200" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesHashrate86400_COVER" ON "public"."SeriesHashrate86400" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate3600_COVER" ON "public"."SeriesPoolHashrate3600" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate21600_COVER" ON "public"."SeriesPoolHashrate21600" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate43200_COVER" ON "public"."SeriesPoolHashrate43200" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate86400_COVER" ON "public"."SeriesPoolHashrate86400" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares3600_COVER" ON "public"."SeriesPoolShares3600" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares21600_COVER" ON "public"."SeriesPoolShares21600" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares43200_COVER" ON "public"."SeriesPoolShares43200" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares86400_COVER" ON "public"."SeriesPoolShares86400" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers3600_COVER" ON "public"."SeriesPoolWorkers3600" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers21600_COVER" ON "public"."SeriesPoolWorkers21600" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers43200_COVER" ON "public"."SeriesPoolWorkers43200" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers86400_COVER" ON "public"."SeriesPoolWorkers86400" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares3600_COVER" ON "public"."SeriesShares3600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares21600_COVER" ON "public"."SeriesShares21600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares43200_COVER" ON "public"."SeriesShares43200" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares86400_COVER" ON "public"."SeriesShares86400" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers3600_COVER" ON "public"."SeriesWorkers3600" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers21600_COVER" ON "public"."SeriesWorkers21600" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers43200_COVER" ON "public"."SeriesWorkers43200" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers86400_COVER" ON "public"."SeriesWorkers86400" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180814_000.sql', EXTRACT(epoch FROM now()));