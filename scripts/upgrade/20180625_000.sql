----
---- Drop materialized views
----
DROP MATERIALIZED VIEW "public"."AggregateMinerStats3600";

DROP MATERIALIZED VIEW "public"."AggregateMinerStats21600";

DROP MATERIALIZED VIEW "public"."AggregateMinerStats43200";

DROP MATERIALIZED VIEW "public"."AggregateMinerStats86400";

----
---- Modify existing tables
----
WITH "UniqueAcceptedShare" AS (SELECT DISTINCT ON ("MinerPublicKey", "Nonce") * FROM "AcceptedShare") DELETE FROM "AcceptedShare" WHERE "Id" NOT IN (SELECT "Id" FROM "UniqueAcceptedShare");

WITH "UniqueAcceptedShareArchive" AS (SELECT DISTINCT ON ("MinerPublicKey", "Nonce") * FROM "AcceptedShareArchive") DELETE FROM "AcceptedShareArchive" WHERE "Id" NOT IN (SELECT "Id" FROM "UniqueAcceptedShareArchive");

ALTER TABLE "public"."AcceptedShare" DROP CONSTRAINT "PK_AcceptedShare";

ALTER TABLE "public"."AcceptedShare" DROP COLUMN "Id";

ALTER TABLE "public"."AcceptedShareArchive" DROP CONSTRAINT "PK_AcceptedShareArchive";

ALTER TABLE "public"."AcceptedShareArchive" DROP COLUMN "Id";

----
---- Add constraints
----
ALTER TABLE "public"."AcceptedShare" ADD CONSTRAINT "PK_AcceptedShare" PRIMARY KEY ("MinerPublicKey", "Nonce");

ALTER TABLE "public"."AcceptedShareArchive" ADD CONSTRAINT "PK_AcceptedShareArchive" PRIMARY KEY ("MinerPublicKey", "Nonce");

----
---- Create materialized views
----
--
-- AggregateMinerStats3600
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats3600" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 3600::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 3600::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 3600::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 3600::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats3600" OWNER TO "postgres";

--
-- AggregateMinerStats21600
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats21600" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 21600::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 21600::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 21600::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 21600::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats21600" OWNER TO "postgres";

--
-- AggregateMinerStats43200
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats43200" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 43200::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 43200::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 43200::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 43200::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats43200" OWNER TO "postgres";

--
-- AggregateMinerStats86400
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats86400" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 86400::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 86400::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 86400::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 86400::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats86400" OWNER TO "postgres";

----
---- Add materialized view indexes
----
CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats3600_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats3600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats21600_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats21600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats43200_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats43200" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats86400_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats86400" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180625_000.sql', EXTRACT(epoch FROM now()));