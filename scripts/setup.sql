----
---- Drop existing database
----
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "nVeoPool";
----
---- Create new database
----
CREATE DATABASE "nVeoPool" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
ALTER DATABASE "nVeoPool" OWNER TO "postgres";

\connect "nVeoPool"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

COMMENT ON SCHEMA "public" IS 'standard public schema';
CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
SET default_tablespace = '';
SET default_with_oids = false;

----
---- Create tables
----
--
-- AcceptedShare
--
CREATE TABLE "public"."AcceptedShare" (
    "BlockDifficulty" integer NOT NULL,
    "BlockHash" "text" NOT NULL,
    "BlockHeight" integer NOT NULL,
    "HashCount" double precision NOT NULL,
    "IsBlockCandidate" boolean NOT NULL,
    "JobDifficulty" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text",
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."AcceptedShare" OWNER TO "postgres";

--
-- AcceptedShareArchive
--
CREATE TABLE "public"."AcceptedShareArchive" (
    "BlockDifficulty" integer NOT NULL,
    "BlockHash" "text" NOT NULL,
    "BlockHeight" integer NOT NULL,
    "HashCount" double precision NOT NULL,
    "IsBlockCandidate" boolean NOT NULL,
    "JobDifficulty" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text",
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."AcceptedShareArchive" OWNER TO "postgres";

--
-- BlockBonus
--
CREATE TABLE "public"."BlockBonus" (
    "Amount" numeric NOT NULL,
    "AmountIsPercent" boolean NOT NULL,
    "Id" smallint NOT NULL,
    "IsActive" boolean NOT NULL,
    "Remaining" numeric NOT NULL,
    "TotalPaid" numeric NOT NULL
);
ALTER TABLE "public"."BlockBonus" OWNER TO "postgres";
CREATE SEQUENCE "public"."BlockBonus_Id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."BlockBonus_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."BlockBonus_Id_seq" OWNED BY "public"."BlockBonus"."Id";
ALTER TABLE ONLY "public"."BlockBonus" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."BlockBonus_Id_seq"'::"regclass");

--
-- BlockCandidate
--
CREATE TABLE "public"."BlockCandidate" (
    "BlockHeight" integer NOT NULL,
    "IsPaid" boolean NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Nonce" "text" NOT NULL,
    "StatusId" smallint NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."BlockCandidate" OWNER TO "postgres";

--
-- BlockCandidateStatus
--
CREATE TABLE "public"."BlockCandidateStatus" (
    "Id" smallint NOT NULL,
    "Name" "text" NOT NULL
);
ALTER TABLE "public"."BlockCandidateStatus" OWNER TO "postgres";
CREATE SEQUENCE "public"."BlockCandidateStatus_Id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."BlockCandidateStatus_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."BlockCandidateStatus_Id_seq" OWNED BY "public"."BlockCandidateStatus"."Id";
ALTER TABLE ONLY "public"."BlockCandidateStatus" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."BlockCandidateStatus_Id_seq"'::"regclass");

--
-- BlockReward
--
CREATE TABLE "public"."BlockReward" (
    "BlockHeight" integer NOT NULL,
    "FeePercent" numeric NOT NULL,
    "IsFeeRecipient" boolean NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Reward" numeric NOT NULL
);
ALTER TABLE "public"."BlockReward" OWNER TO "postgres";

--
-- BlockRewardBonus
--
CREATE TABLE "public"."BlockRewardBonus" (
    "BlockBonusId" smallint NOT NULL,
    "BlockHeight" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Reward" numeric NOT NULL
);
ALTER TABLE "public"."BlockRewardBonus" OWNER TO "postgres";

--
-- InvalidShare
--
CREATE TABLE "public"."InvalidShare" (
    "IsDuplicate" boolean NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."InvalidShare" OWNER TO "postgres";

--
-- MinerBalance
--
CREATE TABLE "public"."MinerBalance" (
    "MinerPublicKey" "text" NOT NULL,
    "MinimumPayoutThreshold" numeric,
    "PendingBalance" numeric NOT NULL,
    "TotalPaid" numeric NOT NULL
);
ALTER TABLE "public"."MinerBalance" OWNER TO "postgres";

--
-- MinerWhitelist
--
CREATE TABLE "public"."MinerWhitelist" (
    "MinerPublicKey" text NOT NULL
);
ALTER TABLE "public"."MinerWhitelist" OWNER TO "postgres";

--
-- PaymentTransaction
--
CREATE TABLE "public"."PaymentTransaction" (
    "Amount" numeric NOT NULL,
    "IsConfirmed" boolean,
    "MinerPublicKey" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL,
    "SubmittedOnBlockHeight" integer NOT NULL,
    "TransactionFee" numeric NOT NULL,
    "TransactionId" "text" NOT NULL
);
ALTER TABLE "public"."PaymentTransaction" OWNER TO "postgres";

--
-- SeriesHashrate3600
--
CREATE TABLE "public"."SeriesHashrate3600" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate3600" OWNER TO "postgres";

--
-- SeriesHashrate21600
--
CREATE TABLE "public"."SeriesHashrate21600" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate21600" OWNER TO "postgres";

--
-- SeriesHashrate43200
--
CREATE TABLE "public"."SeriesHashrate43200" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate43200" OWNER TO "postgres";

--
-- SeriesHashrate86400
--
CREATE TABLE "public"."SeriesHashrate86400" (
    "GHs" double precision NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesHashrate86400" OWNER TO "postgres";

--
-- SeriesPoolHashrate3600
--
CREATE TABLE "public"."SeriesPoolHashrate3600" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate3600" OWNER TO "postgres";

--
-- SeriesPoolHashrate21600
--
CREATE TABLE "public"."SeriesPoolHashrate21600" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate21600" OWNER TO "postgres";

--
-- SeriesPoolHashrate43200
--
CREATE TABLE "public"."SeriesPoolHashrate43200" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate43200" OWNER TO "postgres";

--
-- SeriesPoolHashrate86400
--
CREATE TABLE "public"."SeriesPoolHashrate86400" (
    "GHs" double precision NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolHashrate86400" OWNER TO "postgres";

--
-- SeriesPoolShares3600
--
CREATE TABLE "public"."SeriesPoolShares3600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares3600" OWNER TO "postgres";

--
-- SeriesPoolShares21600
--
CREATE TABLE "public"."SeriesPoolShares21600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares21600" OWNER TO "postgres";

--
-- SeriesPoolShares43200
--
CREATE TABLE "public"."SeriesPoolShares43200" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares43200" OWNER TO "postgres";

--
-- SeriesPoolShares86400
--
CREATE TABLE "public"."SeriesPoolShares86400" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolShares86400" OWNER TO "postgres";

--
-- SeriesPoolWorkers3600
--
CREATE TABLE "public"."SeriesPoolWorkers3600" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers3600" OWNER TO "postgres";

--
-- SeriesPoolWorkers21600
--
CREATE TABLE "public"."SeriesPoolWorkers21600" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers21600" OWNER TO "postgres";

--
-- SeriesPoolWorkers43200
--
CREATE TABLE "public"."SeriesPoolWorkers43200" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers43200" OWNER TO "postgres";

--
-- SeriesPoolWorkers86400
--
CREATE TABLE "public"."SeriesPoolWorkers86400" (
    "MinerCount" bigint NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesPoolWorkers86400" OWNER TO "postgres";

--
-- SeriesShares3600
--
CREATE TABLE "public"."SeriesShares3600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares3600" OWNER TO "postgres";

--
-- SeriesShares21600
--
CREATE TABLE "public"."SeriesShares21600" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares21600" OWNER TO "postgres";

--
-- SeriesShares43200
--
CREATE TABLE "public"."SeriesShares43200" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares43200" OWNER TO "postgres";

--
-- SeriesShares86400
--
CREATE TABLE "public"."SeriesShares86400" (
    "AcceptedShareCount" bigint NOT NULL,
    "InvalidShareCount" bigint NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "Timestamp" bigint NOT NULL
);
ALTER TABLE "public"."SeriesShares86400" OWNER TO "postgres";

--
-- SeriesWorkers3600
--
CREATE TABLE "public"."SeriesWorkers3600" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers3600" OWNER TO "postgres";

--
-- SeriesWorkers21600
--
CREATE TABLE "public"."SeriesWorkers21600" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers21600" OWNER TO "postgres";

--
-- SeriesWorkers43200
--
CREATE TABLE "public"."SeriesWorkers43200" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers43200" OWNER TO "postgres";

--
-- SeriesWorkers86400
--
CREATE TABLE "public"."SeriesWorkers86400" (
    "MinerPublicKey" "text" NOT NULL,
    "Timestamp" bigint NOT NULL,
    "WorkerCount" bigint NOT NULL
);
ALTER TABLE "public"."SeriesWorkers86400" OWNER TO "postgres";

--
-- UnrecordedBlockScan
--
CREATE TABLE "public"."UnrecordedBlockScan" (
    "LastBlockHeight" integer NOT NULL
);
ALTER TABLE "public"."UnrecordedBlockScan" OWNER TO "postgres";

--
-- UpgradeHistory
--
CREATE TABLE "public"."UpgradeHistory" (
    "Id" bigint NOT NULL,
    "ScriptName" "text" NOT NULL,
    "CompletedOn" bigint NOT NULL
);
ALTER TABLE "public"."UpgradeHistory" OWNER TO "postgres";
CREATE SEQUENCE "public"."UpgradeHistory_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."UpgradeHistory_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."UpgradeHistory_Id_seq" OWNED BY "public"."UpgradeHistory"."Id";
ALTER TABLE ONLY "public"."UpgradeHistory" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."UpgradeHistory_Id_seq"'::"regclass");

----
---- Add constraints
----
ALTER TABLE ONLY "public"."AcceptedShare"
    ADD CONSTRAINT "PK_AcceptedShare" PRIMARY KEY ("MinerPublicKey", "Nonce");

ALTER TABLE ONLY "public"."AcceptedShareArchive"
    ADD CONSTRAINT "PK_AcceptedShareArchive" PRIMARY KEY ("MinerPublicKey", "Nonce");

ALTER TABLE ONLY "public"."BlockBonus"
    ADD CONSTRAINT "PK_BlockBonus" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."BlockBonus"
    ADD CONSTRAINT "CHK_BlockBonus_Amount" CHECK ("Amount" > 0);

ALTER TABLE ONLY "public"."BlockBonus"
    ADD CONSTRAINT "CHK_BlockBonus_Remaining" CHECK ("Remaining" >= 0);

ALTER TABLE ONLY "public"."BlockCandidate"
    ADD CONSTRAINT "PK_BlockCandidate" PRIMARY KEY ("BlockHeight", "Nonce");

ALTER TABLE ONLY "public"."BlockCandidateStatus"
    ADD CONSTRAINT "PK_BlockCandidateStatus" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."BlockCandidate"
    ADD CONSTRAINT "FK_BlockCandidate_BlockCandidateStatus" FOREIGN KEY ("StatusId") REFERENCES "public"."BlockCandidateStatus"("Id");

ALTER TABLE "public"."BlockCandidate"
    ADD CONSTRAINT "CHK_BlockCandidate_IsPaid" CHECK ("IsPaid" = false OR ("IsPaid" = true AND "StatusId" = 1));

ALTER TABLE ONLY "public"."BlockReward"
    ADD CONSTRAINT "PK_BlockReward" PRIMARY KEY ("BlockHeight", "MinerPublicKey", "IsFeeRecipient");

ALTER TABLE ONLY "public"."BlockRewardBonus"
    ADD CONSTRAINT "FK_BlockRewardBonus_BlockBonus" FOREIGN KEY ("BlockBonusId") REFERENCES "public"."BlockBonus"("Id");

ALTER TABLE ONLY "public"."BlockRewardBonus"
    ADD CONSTRAINT "PK_BlockRewardBonus" PRIMARY KEY ("BlockHeight", "MinerPublicKey", "BlockBonusId");

ALTER TABLE ONLY "public"."MinerBalance"
    ADD CONSTRAINT "PK_MinerBalance" PRIMARY KEY ("MinerPublicKey");

ALTER TABLE "public"."MinerBalance"
    ADD CONSTRAINT "CHK_MinerBalance_MinimumPayoutThreshold" CHECK ("MinimumPayoutThreshold" IS NULL OR "MinimumPayoutThreshold" >= 0);

ALTER TABLE "public"."MinerBalance"
    ADD CONSTRAINT "CHK_MinerBalance_PendingBalance" CHECK ("PendingBalance" >= 0);

ALTER TABLE ONLY "public"."MinerWhitelist"
    ADD CONSTRAINT "PK_MinerWhitelist" PRIMARY KEY ("MinerPublicKey");

ALTER TABLE ONLY "public"."PaymentTransaction"
    ADD CONSTRAINT "PK_PaymentTransaction" PRIMARY KEY ("MinerPublicKey", "TransactionId");

ALTER TABLE ONLY "public"."UnrecordedBlockScan"
    ADD CONSTRAINT "PK_UnrecordedBlockScan" PRIMARY KEY ("LastBlockHeight");

ALTER TABLE ONLY "public"."UpgradeHistory"
    ADD CONSTRAINT "PK_UpgradeHistory" PRIMARY KEY ("Id");

----
---- Add indexes
----
CREATE INDEX "IDX_AcceptedShare_BlockHeight" ON "public"."AcceptedShare" USING "btree" ("BlockHeight" DESC NULLS LAST);

CREATE INDEX "IDX_AcceptedShare_MinerWorkerId" ON "public"."AcceptedShare" USING "btree" ("MinerWorkerId" "text_pattern_ops");

CREATE INDEX "IDX_AcceptedShare_SubmittedOn_DESC" ON "public"."AcceptedShare" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_AcceptedShareArchive_BlockHeight" ON "public"."AcceptedShareArchive" USING "btree" ("BlockHeight" DESC NULLS LAST);

CREATE INDEX "IDX_AcceptedShareArchive_MinerWorkerId" ON "public"."AcceptedShareArchive" USING "btree" ("MinerWorkerId" "text_pattern_ops");

CREATE INDEX "IDX_AcceptedShareArchive_SubmittedOn_DESC" ON "public"."AcceptedShareArchive" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_BlockCandidate_SubmittedOn_DESC" ON "public"."BlockCandidate" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_FK_BlockCandidate_BlockCandidateStatus" ON "public"."BlockCandidate" USING "btree" ("StatusId");

CREATE INDEX "IDX_InvalidShare_MinerPublicKey" ON "public"."InvalidShare" USING "btree" ("MinerPublicKey" "text_pattern_ops");

CREATE INDEX "IDX_InvalidShare_SubmittedOn_DESC" ON "public"."InvalidShare" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_PaymentTransaction_MinerPublicKey" ON "public"."PaymentTransaction" USING "btree" ("MinerPublicKey" "text_pattern_ops");

CREATE INDEX "IDX_PaymentTransaction_SubmittedOn_DESC" ON "public"."PaymentTransaction" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_PaymentTransaction_SubmittedOnBlockHeight_DESC" ON "public"."PaymentTransaction" USING "btree" ("SubmittedOnBlockHeight" DESC NULLS LAST);

CREATE INDEX "IDX_SeriesHashrate3600_COVER" ON "public"."SeriesHashrate3600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesHashrate21600_COVER" ON "public"."SeriesHashrate21600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesHashrate43200_COVER" ON "public"."SeriesHashrate43200" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesHashrate86400_COVER" ON "public"."SeriesHashrate86400" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate3600_COVER" ON "public"."SeriesPoolHashrate3600" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate21600_COVER" ON "public"."SeriesPoolHashrate21600" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate43200_COVER" ON "public"."SeriesPoolHashrate43200" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolHashrate86400_COVER" ON "public"."SeriesPoolHashrate86400" USING "btree" ("GHs", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares3600_COVER" ON "public"."SeriesPoolShares3600" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares21600_COVER" ON "public"."SeriesPoolShares21600" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares43200_COVER" ON "public"."SeriesPoolShares43200" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolShares86400_COVER" ON "public"."SeriesPoolShares86400" USING "btree" ("AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers3600_COVER" ON "public"."SeriesPoolWorkers3600" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers21600_COVER" ON "public"."SeriesPoolWorkers21600" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers43200_COVER" ON "public"."SeriesPoolWorkers43200" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesPoolWorkers86400_COVER" ON "public"."SeriesPoolWorkers86400" USING "btree" ("MinerCount", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares3600_COVER" ON "public"."SeriesShares3600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares21600_COVER" ON "public"."SeriesShares21600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares43200_COVER" ON "public"."SeriesShares43200" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesShares86400_COVER" ON "public"."SeriesShares86400" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops", "AcceptedShareCount", "InvalidShareCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers3600_COVER" ON "public"."SeriesWorkers3600" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers21600_COVER" ON "public"."SeriesWorkers21600" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers43200_COVER" ON "public"."SeriesWorkers43200" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

CREATE INDEX "IDX_SeriesWorkers86400_COVER" ON "public"."SeriesWorkers86400" USING "btree" ("MinerPublicKey", "WorkerCount", "Timestamp" DESC);

----
---- Add default data
----
INSERT INTO "public"."BlockCandidateStatus" ("Id", "Name") VALUES (1, 'Confirmed'), (2, 'Orphaned'), (3, 'Unconfirmed (Accepted)'), (4, 'Unconfirmed (Rejected)');

----
---- Create materialized views
----
--
-- AggregateMinerStats3600
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats3600" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 3600::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 3600::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 3600::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 3600::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats3600" OWNER TO "postgres";

--
-- AggregateMinerStats21600
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats21600" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 21600::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 21600::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 21600::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 21600::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats21600" OWNER TO "postgres";

--
-- AggregateMinerStats43200
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats43200" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 43200::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 43200::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 43200::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 43200::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats43200" OWNER TO "postgres";

--
-- AggregateMinerStats86400
--
CREATE MATERIALIZED VIEW "public"."AggregateMinerStats86400" AS
  SELECT a."MinerPublicKey",
    a."MinerWorkerId",
    SUM(a."HashCount") / 86400::double precision / 1000000000::double precision AS "GHs",
    AVG(a."JobDifficulty") AS "AverageJobDifficulty",
    COUNT(*) AS "AcceptedShareCount",
    MAX(a."SubmittedOn") AS "LastShareAcceptedOn",
    (SELECT COUNT(*) FROM "public"."InvalidShare" i WHERE i."MinerPublicKey" = a."MinerPublicKey" AND i."MinerWorkerId" = a."MinerWorkerId" AND i."SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 86400::double precision)) AS "InvalidShareCount" 
  FROM (SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShareArchive" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 86400::double precision)
    UNION
        SELECT "BlockDifficulty",
            "BlockHash",
            "BlockHeight",
            "HashCount",
            "IsBlockCandidate",
            "JobDifficulty",
            "MinerPublicKey",
            "MinerWorkerId",
            "Nonce",
            "SubmittedOn"
           FROM "public"."AcceptedShare" WHERE "SubmittedOn"::double precision > (date_part('epoch'::text, now()) - 86400::double precision)) AS a
  GROUP BY a."MinerPublicKey", a."MinerWorkerId" WITH DATA;
ALTER TABLE "public"."AggregateMinerStats86400" OWNER TO "postgres";

----
---- Add materialized view indexes
----
CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats3600_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats3600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats21600_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats21600" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats43200_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats43200" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE UNIQUE INDEX "IDX_MV_AggregateMinerStats86400_MinerPublicKeyMinerWorkerId" ON "public"."AggregateMinerStats86400" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

----
---- All done!
----
GRANT ALL ON SCHEMA "public" TO PUBLIC;

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180814_000.sql', EXTRACT(epoch FROM now()));