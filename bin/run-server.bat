@echo off

set dir=%~dp0
set project=""
if "%~1" == "api" (
	set project="nVeoPool.ApiServer\nVeoPool.ApiServer.csproj"
	if "%~2" == "" (
		if "%NVEOPOOL_API_SETTINGS%" == "" (
			set NVEOPOOL_API_SETTINGS="%dir%..\src\nVeoPool.ApiServer\bin\Release\netcoreapp2.0\api-settings.json"
		)
	) else (
		set NVEOPOOL_API_SETTINGS="%~2"
	)
)
if "%~1" == "pool" (
	set project="nVeoPool.PoolServer\nVeoPool.PoolServer.csproj"
	if "%~2" == "" (
		if "%NVEOPOOL_POOL_SETTINGS%" == "" (
			set NVEOPOOL_POOL_SETTINGS="%dir%..\src\nVeoPool.PoolServer\bin\Release\netcoreapp2.0\pool-settings.json"
		)
	) else (
		set NVEOPOOL_POOL_SETTINGS="%~2"
	)
)

if %project% == "" (
	echo USAGE: 
	echo   %0 api [CONFIG_FILE_PATH]
	echo     Start the API server, optionally specifying the path to the API server settings file
	echo   %0 pool [CONFIG_FILE_PATH]
	echo     Start the pool server, optionally specifying the path to the pool server settings file
) else (
	cd "%dir%..\src"
	dotnet build --configuration Release && dotnet run --project "%project%" --configuration Release
)