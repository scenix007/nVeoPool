#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.Banning {
	internal class BanManager : IBanManager {
		public BanManager(IDistributedCacheAdapter cache, ILogger<BanManager> logger, IPoolServerConfiguration poolServerConfiguration) {
			_cache = cache;
			_logger = logger;
			_whitelistEnabled = poolServerConfiguration.BanningConfiguration != null && poolServerConfiguration.BanningConfiguration.WhitelistEnabled;
		}

		#region IBanManager
		public async Task<bool> MinerIsAllowedAsync(String minerPublicKey) {
			if (!_whitelistEnabled) {
				_logger.LogDebug("Whitelist disabled in {Type}, allowing access for {MinerPublicKey}", nameof(BanManager), minerPublicKey);

				return true;
			}

			try {
				var isAllowed = await _cache.MinerIsWhitelistedAsync(minerPublicKey.ToLowerInvariant());

				_logger.LogDebug("{Type} found {IsAllowed} when checking access for {MinerPublicKey}", nameof(BanManager), isAllowed, minerPublicKey);

				return isAllowed;
			} catch (Exception e) {
				_logger.LogError(e, "Exception in {Type} while attempting to verify {MinerPublicKey}", nameof(BanManager), minerPublicKey);
			}

			return false;
		}
		#endregion

		private readonly IDistributedCacheAdapter _cache;
		private readonly ILogger<BanManager> _logger;
		private readonly bool _whitelistEnabled;
	}
}
