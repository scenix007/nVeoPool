#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Extensions;
using nVeoPool.Common.Rpc;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;

namespace nVeoPool.PoolServer.Services {
	internal class PaymentConfirmationService : ReportingBackgroundServiceBase {
		public PaymentConfirmationService(IDistributedCacheAdapter cache, ILogger<PaymentConfirmationService> logger, IPaymentRepository paymentRepository, IPoolServerConfiguration poolServerConfiguration, IRpcClientAdapterFactory rpcClientAdapterFactory) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.PaymentConfirmationServiceUpdateIntervalSeconds, ClusterServerType.Master, nameof(PaymentConfirmationService)) {
			_paymentRepository = paymentRepository;
			_poolServerConfiguration = poolServerConfiguration;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					var currentBlockHeight = await _rpcClientAdapterFactory.GetClientAdapter(false).GetCurrentHeightAsync(cancellationToken);
					var unconfirmedPayments = (await _paymentRepository.GetUnconfirmedPaymentsAsync(currentBlockHeight - _poolServerConfiguration.PaymentConfiguration.PaymentMaturityDepth)).ToList();

					if (unconfirmedPayments.Any()) {
						var startBlockHeight = unconfirmedPayments.Min(p => p.SubmittedOnBlockHeight) + 1;
						var blockTransactions = await GetTransactionsForBlocksAsync(Enumerable.Range(startBlockHeight, currentBlockHeight - startBlockHeight + 1), cancellationToken);

						if (blockTransactions != null) {
							var blockTransactionsList = blockTransactions.ToList();
							var confirmedPayments = new List<String>();
							var paymentsForResend = new List<MinerPayment>();

							foreach (var payment in unconfirmedPayments) {
								if (blockTransactionsList.Contains(payment.TransactionId)) {
									confirmedPayments.Add(payment.TransactionId);

									_logger.LogDebug("{BackgroundService} found transaction id {TransactionId}", _serviceName, payment.TransactionId);
								} else {
									paymentsForResend.Add(payment);

									_logger.LogWarning(GlobalEventList.Payment.TransactionNotFound, "{BackgroundService} did not find transaction with id {TransactionId} for {Amount} to {MinerPublicKey}", _serviceName, payment.TransactionId, payment.Amount, payment.MinerPublicKey);
								}
							}

							try {
								await _paymentRepository.UpdateTransactionStatusesAsync(confirmedPayments, paymentsForResend);

								_logger.LogInformation("{BackgroundService} successfully marked payments {@ConfirmedPayments} as confirmed and {@UnconfirmedPayments} as unconfirmed", _serviceName, confirmedPayments, paymentsForResend);
							} catch (Exception e) {
								_logger.LogWarning(e, "{BackgroundService} was unable to mark payments {@ConfirmedPayments} as confirmed and {@UnconfirmedPayments} as unconfirmed", _serviceName, confirmedPayments, paymentsForResend);
							}
						}
					} else {
						_logger.LogInformation("{BackgroundService} found no unconfirmed payments", _serviceName);
					}
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private async Task<IEnumerable<String>> GetTransactionsForBlocksAsync(IEnumerable<int> blockHeights, CancellationToken cancellationToken) {
			var transactions = new List<String>();

			try {
				foreach (var blockHeightBatch in blockHeights.Split(MAX_SIMULTANEOUS_REQUESTS)) {
					var blockTransactions = await Task.WhenAll(blockHeightBatch.Select(b => _rpcClientAdapterFactory.GetClientAdapter(false).GetTransactionsForBlockAsync(b, cancellationToken)));

					transactions.AddRange(blockTransactions.SelectMany(b => b));
				}

				return transactions;
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} was unable to retrieve transactions for blocks {BlockHeights}", _serviceName, blockHeights);
			}

			return null;
		}

		private const int MAX_SIMULTANEOUS_REQUESTS = 5;
		private readonly IPaymentRepository _paymentRepository;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
	}
}
