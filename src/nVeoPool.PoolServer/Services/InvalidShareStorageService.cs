#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Repositories;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.WorkProtocol;

namespace nVeoPool.PoolServer.Services {
	internal class InvalidShareStorageService : ShareStorageServiceBase {
		public InvalidShareStorageService(IAddressParser addressParser, IDistributedCacheAdapter cache, ILogger<InvalidShareStorageService> logger, IPoolServerConfiguration poolServerConfiguration, IShareRepository shareRepository, IShareQueueFactory shareQueueFactory) : base(addressParser, cache, GlobalEventList.Mining.ErrorSavingInvalidShares, logger, poolServerConfiguration.WorkProtocolConfiguration.MaximumInvalidShareQueueSize, GlobalEventList.Mining.MaximumInvalidShareQueueSizeExceeded, poolServerConfiguration, poolServerConfiguration.ServicesConfiguration.InvalidShareStorageServiceUpdateIntervalSeconds, $"{nameof(InvalidShareStorageService)}_{poolServerConfiguration.ClusteringConfiguration.ServerId}", shareRepository, shareQueueFactory.GetInvalidShareQueue()) {
		}

		protected override Task<int> SaveShareBufferAsync() {
			return _shareRepository.SaveInvalidSharesAsync(_shareBuffer);
		}
	}
}
