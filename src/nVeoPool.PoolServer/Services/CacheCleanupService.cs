#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.Services {
	internal class CacheCleanupService : ReportingBackgroundServiceBase {
		public CacheCleanupService(IAddressParser addressParser, IDistributedCacheAdapter cache, ILogger<CacheCleanupService> logger, IPoolServerConfiguration poolServerConfiguration) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.CacheCleanupServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(CacheCleanupService)) {
			_addressParser = addressParser;
			_poolServerConfiguration = poolServerConfiguration;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					var minersForCleanup = new List<String>();
					var minersForRemoval = new List<String>();
					foreach (var listener in _poolServerConfiguration.WorkProtocolConfiguration.Listeners) {
						var (portMinersForCleanup, portMinersForRemoval) = await CleanupWorkerDataAsync(listener.ListenPort, listener.VarDiffConfiguration.IntervalSampleCount);

						minersForCleanup.AddRange(portMinersForCleanup);
						minersForRemoval.AddRange(portMinersForRemoval);
					}

					minersForCleanup = minersForCleanup.Distinct().ToList();
					minersForRemoval = minersForRemoval.Distinct().Where(m => !minersForCleanup.Contains(m)).ToList();
					await CleanupMinerDataAsync(minersForCleanup, minersForRemoval);

					await CleanupServerDataAsync();
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private async Task CleanupMinerDataAsync(IEnumerable<String> minersForCleanup, IEnumerable<String> minersForRemoval) {
			foreach (var minerPublicKey in minersForCleanup) {
				try {
					await _cache.RemoveMinerRecentSharesAsync(minerPublicKey, true);

					_logger.LogDebug("{BackgroundService} successfully cleaned cached data for active miner {MinerPublicKey}", _serviceName, minerPublicKey);
				} catch (Exception e) {
					_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup for active miner {MinerPublicKey}", _serviceName, minerPublicKey);
				}
			}

			foreach (var minerPublicKey in minersForRemoval) {
				try {
					await _cache.RemoveMinerRecentSharesAsync(minerPublicKey, false);

					_logger.LogDebug("{BackgroundService} successfully cleared cached data for inactive miner {MinerPublicKey}", _serviceName, minerPublicKey);
				} catch (Exception e) {
					_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup for inactive miner {MinerPublicKey}", _serviceName, minerPublicKey);
				}
			}
		}

		private async Task CleanupServerDataAsync() {
			foreach (var serverCategory in Enum.GetValues(typeof(ClusterServerCategory)).Cast<ClusterServerCategory>()) {
				try {
					var serverIds = await _cache.GetRegisteredServersAsync(serverCategory);

					foreach (var serverId in serverIds) {
						try {
							var serverStatus = await _cache.GetServerStatusAsync(serverId);
							var cutoffTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromSeconds(MAXIMUM_SERVER_HEARTBEAT_AGE_SECONDS)).ToUnixTimeSeconds();

							if ((serverStatus.Heartbeat ?? 0) <= cutoffTime) {
								await CleanupServiceDataAsync(serverId);

								await _cache.UnregisterServerAsync(serverCategory, serverId);

								_logger.LogDebug("{BackgroundService} successfully unregistered inactive server {ServerId}", _serviceName, serverId);
							}
						} catch (Exception e) {
							_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup data for server {ServerId}", _serviceName, serverId);
						}
					}
				} catch (Exception e) {
					_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup data for server category {ServerCategory}", _serviceName, serverCategory);
				}
			}
		}

		private async Task CleanupServiceDataAsync(String serverId) {
			try {
				var services = await _cache.GetRegisteredServicesForServerAsync(serverId);

				foreach (var service in services) {
					try {
						await _cache.UnregisterServiceAsync(serverId, service, true);

						_logger.LogDebug("{BackgroundService} successfully unregistered service {ServiceName} on inactive server {ServerId}", _serviceName, service, serverId);
					} catch (Exception e) {
						_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to unregister service {ServiceName} on inactive server {ServerId}", _serviceName, service, serverId);
					}
				}
			} catch (Exception e) {
				_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup service data for inactive server {ServerId}", _serviceName, serverId);
			}
		}

		private async Task<(List<String> MinersForCleanup, List<String> MinersForRemoval)> CleanupWorkerDataAsync(int port, int intervalSampleCount) {
			try {
				var activeWorkers = (await _cache.GetActiveWorkersAsync(port)).ToList();
				var minersForCleanup = new List<String>();
				var minersForRemoval = new List<String>();

				foreach (var worker in activeWorkers) {
					try {
						var cutoffWorkerTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromSeconds(_runInterval)).ToUnixTimeSeconds();
						var lastSeenTime = await _cache.GetWorkerLastSeenTimeAsync(worker, port);
						var minerPublicKey = _addressParser.ParseMinerAddress(worker).PublicKey;

						if (!lastSeenTime.HasValue || lastSeenTime <= cutoffWorkerTime) {
							await _cache.ClearWorkerInfoAsync(worker, port);

							_logger.LogDebug("{BackgroundService} successfully cleared cached data for inactive worker {MinerAddress} on port {ListenPort}", _serviceName, worker, port);

							minersForRemoval.Add(minerPublicKey);
						} else {
							await _cache.ClearWorkerRecentShareIntervalsAsync(worker, port, intervalSampleCount);

							_logger.LogDebug("{BackgroundService} successfully cleaned cached data for active worker {MinerAddress} on port {ListenPort}", _serviceName, worker, port);

							minersForCleanup.Add(minerPublicKey);
						}
					} catch (Exception e) {
						_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup for worker {MinerAddress} on port {ListenPort}", _serviceName, worker, port);
					}
				}

				minersForCleanup = minersForCleanup.Distinct().ToList();

				return (minersForCleanup, minersForRemoval.Distinct().Where(m => !minersForCleanup.Contains(m)).ToList());
			} catch (Exception e) {
				_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to cleanup worker data on port {ListenPort}", _serviceName, port);
			}

			return (new List<String>(), new List<String>());
		}

		private const int MAXIMUM_SERVER_HEARTBEAT_AGE_SECONDS = 30*60;
		private readonly IAddressParser _addressParser;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
	}
}
