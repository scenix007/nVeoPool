#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Configuration.WorkProtocol;
using nVeoPool.PoolServer.Models;

namespace nVeoPool.PoolServer.Services {
	internal class DifficultyCalculatorService : ReportingBackgroundServiceBase {
		public DifficultyCalculatorService(IDistributedCacheAdapter cache, ICacheConnectionFactory cacheConnectionFactory, ILogger<DifficultyCalculatorService> logger, IPoolServerConfiguration poolServerConfiguration) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.DifficultyCalculatorServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(DifficultyCalculatorService)) {
			_cacheConnectionFactory = cacheConnectionFactory;
			_poolServerConfiguration = poolServerConfiguration;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			if (_poolServerConfiguration.WorkProtocolConfiguration.FixedDifficultyListenPorts.Any()) {
				foreach (var listenPort in _poolServerConfiguration.WorkProtocolConfiguration.FixedDifficultyListenPorts) {
					_logger.LogInformation("{BackgroundService} detected fixed difficulty on port {ListenPort}", _serviceName, listenPort);

					try {
						var activeWorkers = await _cache.GetActiveWorkersAsync(listenPort);

						foreach (var worker in activeWorkers) {
							try {
								_cache.ClearWorkerCurrentJobDifficultyImmediate(worker, listenPort);
							} catch (Exception e) {
								_logger.LogWarning(e, "{BackgroundService} failed to clear job difficulty for {MinerAddress} on port {ListenPort}", _serviceName, worker, listenPort);
							}
						}
					} catch (Exception e) {
						_logger.LogError(e, "{BackgroundService} failed to clear job difficulties on port {ListenPort} on startup, miners may be using incorrect job difficulty", _serviceName, listenPort);
					}
				}
			}

			if (!_poolServerConfiguration.WorkProtocolConfiguration.VariableDifficultyListeners.Any()) {
				await ReportServiceAsStoppingAsync();
			} else {
				cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

				while (!cancellationToken.IsCancellationRequested) {
					try {
						await ReportServiceAsExecutingAsync();

						await Task.WhenAll(_poolServerConfiguration.WorkProtocolConfiguration.VariableDifficultyListeners.Select(AdjustListenerDifficultyAsync));
					} catch (Exception e) {
						ReportUnhandledServiceException(e);
					}

					await ReportServiceAsSleepingAsync();
					await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
				}
			}
		}
		#endregion

		private async Task AdjustListenerDifficultyAsync(IndividualWorkProtocolConfiguration workProtocolConfiguration) {
			try {
				var variance = workProtocolConfiguration.VarDiffConfiguration.IntervalVariancePercent / 100m * workProtocolConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds;
				var maxShareIntervalSeconds = (int)(workProtocolConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds + variance);
				var minShareIntervalSeconds = (int)(workProtocolConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds - variance);

				foreach (var worker in await _cache.GetActiveWorkersAsync(workProtocolConfiguration.ListenPort)) {
					try {
						var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(worker, workProtocolConfiguration.ListenPort);
						var lastShareTimeTask = _cache.GetWorkerLastShareTimeAsync(worker, workProtocolConfiguration.ListenPort);
						var recentShareIntervals = (await _cache.GetWorkerRecentShareIntervalsAsync(worker, workProtocolConfiguration.ListenPort)).Reverse().Take(workProtocolConfiguration.VarDiffConfiguration.IntervalSampleCount).ToList();
						var currentTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
						var lastShareTime = await lastShareTimeTask ?? 0;
						var timeSinceLastShare = (int)(currentTime - lastShareTime);
						if (timeSinceLastShare > maxShareIntervalSeconds) {
							recentShareIntervals.Add(timeSinceLastShare);
						}
						var averageShareInterval = recentShareIntervals.Any() ? recentShareIntervals.Average() : 1;

						var newDifficulty = 0;
						var direction = 0;
						var currentJobDifficulty = await currentJobDifficultyTask ?? workProtocolConfiguration.VarDiffConfiguration.InitialDifficulty;

						if (averageShareInterval > maxShareIntervalSeconds && currentJobDifficulty > workProtocolConfiguration.VarDiffConfiguration.MinimumDifficulty.Value) {
							newDifficulty = (int)(workProtocolConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds / averageShareInterval * currentJobDifficulty);
							newDifficulty = newDifficulty > workProtocolConfiguration.VarDiffConfiguration.MinimumDifficulty.Value ? newDifficulty : workProtocolConfiguration.VarDiffConfiguration.MinimumDifficulty.Value;
							direction = -1;
						} else if (averageShareInterval < minShareIntervalSeconds && currentJobDifficulty < workProtocolConfiguration.VarDiffConfiguration.MaximumDifficulty.Value) {
							newDifficulty = (int)(workProtocolConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds / averageShareInterval * currentJobDifficulty);
							newDifficulty = newDifficulty < workProtocolConfiguration.VarDiffConfiguration.MaximumDifficulty.Value ? newDifficulty : workProtocolConfiguration.VarDiffConfiguration.MaximumDifficulty.Value;
							direction = 1;
						} else {
							continue;
						}

						if (Math.Abs(newDifficulty - currentJobDifficulty) / (decimal)currentJobDifficulty * 100 > workProtocolConfiguration.VarDiffConfiguration.MaximumDifficultyJumpPercent) {
							var change = (int)Math.Floor(workProtocolConfiguration.VarDiffConfiguration.MaximumDifficultyJumpPercent / 100m * currentJobDifficulty * direction);
							newDifficulty = currentJobDifficulty + change;
						}

						_cache.UpdateWorkerCurrentJobDifficultyImmediate(worker, workProtocolConfiguration.ListenPort, newDifficulty);

						if (newDifficulty != currentJobDifficulty) {
							_cacheConnectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.WorkerDifficultyChanged(workProtocolConfiguration.ListenPort), new WorkerDifficulty {
								FormattedMinerAddress = worker,
								NewDifficulty = newDifficulty
							});
						}

						_logger.LogDebug("{BackgroundService} successfully updated difficulty from {OldJobDifficulty} to {JobDifficulty} for {MinerAddress} on port {ListenPort}", _serviceName, currentJobDifficulty, newDifficulty, worker, workProtocolConfiguration.ListenPort);
					} catch (Exception e) {
						_logger.LogWarning(e, "Exception in {BackgroundService} while attempting to update difficulty for {MinerAddress} on port {ListenPort}", _serviceName, worker, workProtocolConfiguration.ListenPort);
					}
				}

				_logger.LogInformation("{BackgroundService} successfully updated difficulty for all workers on port {ListenPort}", _serviceName, workProtocolConfiguration.ListenPort);
			} catch (Exception e) {
				_logger.LogError(e, "Exception in {BackgroundService} while attempting to update difficulty on port {ListenPort}", _serviceName, workProtocolConfiguration.ListenPort);
			}
		}

		private readonly ICacheConnectionFactory _cacheConnectionFactory;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
	}
}
