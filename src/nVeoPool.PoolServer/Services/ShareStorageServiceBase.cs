#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Services.RuntimeData;
using nVeoPool.PoolServer.WorkProtocol;

namespace nVeoPool.PoolServer.Services {
	internal abstract class ShareStorageServiceBase : ReportingBackgroundServiceBase {
		protected ShareStorageServiceBase(IAddressParser addressParser, IDistributedCacheAdapter cache, EventId errorSavingSharesEventId, ILogger logger, int maximumShareQueueSize, EventId maximumShareQueueSizeExceededEventId, IPoolServerConfiguration poolServerConfiguration, uint runInterval, String serviceName, IShareRepository shareRepository, IShareQueue shareQueue) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, runInterval, new [] { ClusterServerType.Master, ClusterServerType.Slave }, serviceName) {
			_addressParser = addressParser;
			_errorSavingSharesEventId = errorSavingSharesEventId;
			_lastSaveTime = DateTime.UtcNow + MAX_WAIT_BETWEEN_SAVES;
			_maximumShareQueueSize = maximumShareQueueSize;
			_maximumShareQueueSizeExceededEventId = maximumShareQueueSizeExceededEventId;
			_poolServerConfiguration = poolServerConfiguration;
			_runtimeData = new ShareStorageData();
			_shareBuffer = new List<Share>();
			_shareQueue = shareQueue;
			_shareRepository = shareRepository;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => {
				try {
					await ProcessShareQueueAsync(true);
				} finally {
					await ReportServiceAsStoppingAsync();
				}
			});

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					await ProcessShareQueueAsync(false);

					if (_maximumShareQueueSize > 0 && _shareQueue.Count > _maximumShareQueueSize) {
						_logger.LogWarning(_maximumShareQueueSizeExceededEventId, "Current share queue size of {CurrentShareQueueSize} in {BackgroundService} exceeded maximum size {MaximumShareQueueSize}", _shareQueue.Count, _serviceName, _maximumShareQueueSize);
					}

					await UpdateRuntimeDataAsync(0);
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync(false);
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		protected abstract Task<int> SaveShareBufferAsync();

		protected virtual void SetShareFields(Share share) {
			(share.MinerPublicKey, share.MinerWorkerId) = _addressParser.ParseMinerAddress(share.MinerAddress);
		}

		private async Task ProcessShareQueueAsync(bool fullFlush) {
			try {
				while (!_shareQueue.IsEmpty) {
					if (_shareQueue.TryDequeue(out var share)) {
						SetShareFields(share);
						_shareBuffer.Add(share);
						
						if (_shareBuffer.Count >= _poolServerConfiguration.DatabaseConfiguration.MaxBatchSize) {
							await SaveShareBufferAndUpdateRuntimeDataAsync();
						}
					}
				}

				if ((DateTime.UtcNow - _lastSaveTime >= MAX_WAIT_BETWEEN_SAVES) || (fullFlush && _shareBuffer.Count > 0)) {
					await SaveShareBufferAndUpdateRuntimeDataAsync();
				} else {
					await UpdateRuntimeDataAsync(0);
				}
			} catch (Exception e) {
				_logger.LogError(_errorSavingSharesEventId, e, "Exception in {BackgroundService} while attempting to save {ShareBufferSize} shares, current queue size is {CurrentShareQueueSize}", _serviceName, _shareBuffer.Count, _shareQueue.Count);
			}
		}

		private async Task SaveShareBufferAndUpdateRuntimeDataAsync() {
			await UpdateRuntimeDataAsync(await SaveShareBufferInternalAsync());
		}

		private async Task<int> SaveShareBufferInternalAsync() {
			var sharesSaved = await SaveShareBufferAsync();

			_shareBuffer.Clear();
			_lastSaveTime = DateTime.UtcNow;

			return sharesSaved;
		}

		private async Task UpdateRuntimeDataAsync(int sharesSaved) {
			_runtimeData.BufferCount = _shareBuffer.Count;
			_runtimeData.LastSavedOn = sharesSaved > 0 ? new DateTimeOffset(_lastSaveTime).ToUnixTimeSeconds() : _runtimeData.LastSavedOn;
			_runtimeData.QueueCount = _shareQueue.Count;
			_runtimeData.TotalSavedCount += (ulong)sharesSaved;

			await UpdateServiceRuntimeDataAsync(_runtimeData);
		}

		private readonly TimeSpan MAX_WAIT_BETWEEN_SAVES = TimeSpan.FromSeconds(15);
		private readonly IAddressParser _addressParser;
		private readonly EventId _errorSavingSharesEventId;
		private DateTime _lastSaveTime;
		private readonly int _maximumShareQueueSize;
		private readonly EventId _maximumShareQueueSizeExceededEventId;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly ShareStorageData _runtimeData;
		protected readonly List<Share> _shareBuffer;
		private readonly IShareQueue _shareQueue;
		protected readonly IShareRepository _shareRepository;
	}
}
