using System;
using System.Security.Cryptography;
using Microsoft.Extensions.Logging;

namespace nVeoPool.PoolServer.PoW {
	internal class AmoveoPoWAlgorithm : IPoWAlgorithm {
		public AmoveoPoWAlgorithm(ILogger<AmoveoPoWAlgorithm> logger) {
			_logger = logger;
		}

		#region IPoWAlgorithm
		public double GetHashCountForDifficulty(int difficulty) {
			return GetHashCountForDifficulty((uint)difficulty);
		}

		public double GetHashCountForDifficulty(uint difficulty) {
			var a = difficulty / 256;
			var b = difficulty % 256;

			return Math.Pow(2, a) * (256 + b) / 256;
		}

		public (uint Difficulty, bool IsValid) ValidatePoW(String nonce, String blockHash, int blockDifficulty, int jobDifficulty) {
			try {
				var nonceBytes = Convert.FromBase64String(nonce);
				var blockHashBytes = Convert.FromBase64String(blockHash);
				var text = new byte[55];

				for (var i = 0; i < 32; i++) {
					text[i] = blockHashBytes[i];
				}
				for (var i = 0; i < 23; i++) {
					text[i + 32] = nonceBytes[i];
				}

				using(var hasher = SHA256.Create()) {
					var hash = hasher.ComputeHash(text);
					var difficulty = HashToInteger(hash);

					return (difficulty, difficulty >= jobDifficulty);
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(ValidatePoW));

				return (0, false);
			}
		}
		#endregion

		private uint HashToInteger(byte[] hashBytes) {
			uint x = 0, z = 0;
			var y = new uint[2];

			for (int i = 0; i < 31; i++) {
				if (hashBytes[i] == 0) {
					x += 8;
					continue;
				} else if (hashBytes[i] < 2) {
					x += 7;
					z = hashBytes[i+1];
				} else if (hashBytes[i] < 4) {
					x += 6;
					z = (hashBytes[i+1] / 2u) + ((hashBytes[i] % 2u) * 128);
				} else if (hashBytes[i] < 8) {
					x += 5;
					z = (hashBytes[i+1] / 4u) + ((hashBytes[i] % 4u) * 64);
				} else if (hashBytes[i] < 16) {
					x += 4;
					z = (hashBytes[i+1] / 8u) + ((hashBytes[i] % 8u) * 32);
				} else if (hashBytes[i] < 32) {
					x += 3;
					z = (hashBytes[i+1] / 16u) + ((hashBytes[i] % 16u) * 16);
				} else if (hashBytes[i] < 64) {
					x += 2;
					z = (hashBytes[i+1] / 32u) + ((hashBytes[i] % 32u) * 8);
				} else if (hashBytes[i] < 128) {
					x += 1;
					z = (hashBytes[i+1] / 64u) + ((hashBytes[i] % 64u) * 4);
				} else {
					z = (hashBytes[i+1] / 128u) + ((hashBytes[i] % 128u) * 2);
				}

				break;
			}

			y[0] = x;
			y[1] = z;

			return PairToSci(y);
		}

		private uint PairToSci(uint[] pair) {
			return (256 * pair[0]) + pair[1];
		}

		private readonly ILogger<AmoveoPoWAlgorithm> _logger;
	}
}