#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.Notifications {
	internal class NotificationHandlerRegistry : INotificationHandlerRegistry {
		public NotificationHandlerRegistry(ILogger<DiscordNotificationHandler> discordNotificationLogger, IPoolServerConfiguration poolServerConfiguration) {
			_discordNotificationLogger = discordNotificationLogger;
			_notificationHandlerMap = new Dictionary<int, IList<INotificationHandler>>();
			_poolServerConfiguration = poolServerConfiguration;
			
			TotalNotificationHandlers = RegisterDiscordNotificationHandlers();
		}

		#region INotificationHandlerRegistry
		public uint TotalNotificationHandlers { get; }

		public IEnumerable<INotificationHandler> GetRegisteredHandlers(int eventId) {
			return _notificationHandlerMap.TryGetValue(eventId, out var notificationHandlers) ? notificationHandlers : new List<INotificationHandler>();
		}
		#endregion

		private uint RegisterDiscordNotificationHandlers() {
			var discordConfigurationCount = _poolServerConfiguration.NotificationsConfiguration?.DiscordSinkConfigurations?.Count ?? 0;
			var registeredNotificationHandlers = 0u;

			if (discordConfigurationCount > 0) {
				foreach (var discordConfiguration in _poolServerConfiguration.NotificationsConfiguration.DiscordSinkConfigurations.Where(d => d.EventIds.Count > 0)) {
					var notificationHandler = new DiscordNotificationHandler(discordConfiguration.DelayOnThrottleMilliseconds, discordConfiguration.IncludeEventContext, _discordNotificationLogger, discordConfiguration.MaximumRetries, discordConfiguration.WebHookId, discordConfiguration.WebHookToken);

					foreach (var eventId in discordConfiguration.EventIds) {
						if (_notificationHandlerMap.TryGetValue(eventId, out var notificationHandlers)) {
							notificationHandlers.Add(notificationHandler);
						} else {
							_notificationHandlerMap[eventId] = new List<INotificationHandler> { notificationHandler };
						}
					}

					registeredNotificationHandlers++;
				}
			}

			return registeredNotificationHandlers;
		}

		private readonly ILogger<DiscordNotificationHandler> _discordNotificationLogger;
		private readonly IDictionary<int, IList<INotificationHandler>> _notificationHandlerMap;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
	}
}