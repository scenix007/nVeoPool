#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Serilog.Events;
using nVeoPool.Common.Rpc;
using nVeoPool.PoolServer.Models;

namespace nVeoPool.PoolServer.Notifications {
	internal class DiscordNotificationHandler : ApiClientBase, INotificationHandler {
		public DiscordNotificationHandler(uint delayOnThrottleMilliseconds, bool includeEventContext, ILogger<DiscordNotificationHandler> logger, uint maximumRetries, String webHookId, String webHookToken) : base(new Uri($"https://discordapp.com/api/webhooks/{webHookId}/{webHookToken}")) {
			_delayOnThrottleMilliseconds = delayOnThrottleMilliseconds;
			_includeEventContext = includeEventContext;
			_logger = logger;
			_maximumRetries = maximumRetries;
			_webHook = (webHookId, webHookToken);
		}

		#region INotificationHandler
		public String TypeId => "Discord";
		
		public Task<bool> HandleAsync(Notification notification) {
			return HandleInternalAsync(notification, _maximumRetries + 1);
		}
		#endregion

		private async Task<bool> HandleInternalAsync(Notification notification, uint retriesRemaining) {
			if (retriesRemaining > 0) {
				try {
					var logMessage = notification.Event.RenderMessage();
					var messagePrefix = _includeEventContext ? $"{notification.Event.Timestamp.UtcDateTime:yyyy-MM-dd HH:mm:ss.fff zzz} [{notification.ServerId}]{(notification.Event.Properties.TryGetValue("SourceContext", out var context) ? $"[{context.ToString()}]" : String.Empty)}[{_eventLevelStringMap[notification.Event.Level]}] " : String.Empty;
					var fullMessageContent = $"{messagePrefix}{logMessage}";

					await GetNodeResponseAsync("{\"content\": \"" + fullMessageContent.Replace("\"", String.Empty) + "\"}", "application/json");

					_logger.LogInformation("{NotificationHandler} successfully handled notification {NotificationEventId} for WebHook {DiscordWebHook}", nameof(DiscordNotificationHandler), notification.EventId, _webHook);

					return true;
				} catch (Exception e) {
					if (e is HttpRequestException && e.Message.Contains(": 429 ")) {
						_logger.LogWarning(e, "{NotificationHandler} throttled while attempting to handle notification {NotificationEventId} for WebHook {DiscordWebHook}, {RetriesRemaining} retries remaining", nameof(DiscordNotificationHandler), notification.EventId, _webHook, retriesRemaining - 1);

						if (retriesRemaining > 1 && _delayOnThrottleMilliseconds > 0) {
							_logger.LogInformation("{NotificationHandler} pausing for {ThrottleDelay} milliseconds", nameof(DiscordNotificationHandler), _delayOnThrottleMilliseconds);

							await Task.Delay(TimeSpan.FromMilliseconds(_delayOnThrottleMilliseconds));
						}
					} else {
						_logger.LogWarning(e, "Exception in {NotificationHandler} while attempting to handle notification {NotificationEventId} for WebHook {DiscordWebHook}, {RetriesRemaining} retries remaining", nameof(DiscordNotificationHandler), notification.EventId, _webHook, retriesRemaining - 1);
					}
				}

				return await HandleInternalAsync(notification, retriesRemaining - 1);
			}

			_logger.LogError("{NotificationHandler} failed to handle notification {NotificationEventId} for WebHook {DiscordWebHook}, no retries remaining", nameof(DiscordNotificationHandler), notification.EventId, _webHook);

			return false;
		}
		
		private static readonly IDictionary<LogEventLevel, String> _eventLevelStringMap = new Dictionary<LogEventLevel, String> {
			{ LogEventLevel.Debug, "DBG" },
			{ LogEventLevel.Error, "ERR" },
			{ LogEventLevel.Fatal, "FTL" },
			{ LogEventLevel.Information, "INF" },
			{ LogEventLevel.Verbose, "TRC" },
			{ LogEventLevel.Warning, "WRN" }
		};
		private readonly uint _delayOnThrottleMilliseconds;
		private readonly bool _includeEventContext;
		private readonly ILogger<DiscordNotificationHandler> _logger;
		private readonly uint _maximumRetries;
		private readonly (String Id, String Token) _webHook;
	}
}