#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using Microsoft.Extensions.Logging;

namespace nVeoPool.PoolServer.Notifications {
	internal static class GlobalEventList {
		public class Blocks {
			public static EventId BlockCandidateAcceptedByNode = new EventId(1000);

			public static EventId BlockCandidateRejectedByNode = new EventId(2000);

			public static EventId BlockCandidatesProcessed = new EventId(3000);

			public static EventId ErrorProcessingBlockCandidates = new EventId(3250);

			public static EventId ErrorProcessingIndividualBlockCandidate = new EventId(3500);

			public static EventId UnrecordedBlockCandidateSaved = new EventId(4000);
		}

		public class Mining {
			public static EventId ErrorRetrievingHeight = new EventId(5000);

			public static EventId ErrorRetrievingMiningData = new EventId(6000);

			public static EventId ErrorSavingAcceptedShares = new EventId(6250);

			public static EventId ErrorSavingInvalidShares = new EventId(6500);

			public static EventId MaximumAcceptedShareQueueSizeExceeded = new EventId(6750);

			public static EventId MaximumInvalidShareQueueSizeExceeded = new EventId(6875);

			public static EventId NoWorkAvailable = new EventId(7000);

			public static EventId PossibleBackupNodeSyncIssue = new EventId(8000);

			public static EventId PossiblePrimaryNodeSyncIssue = new EventId(9000);

			public static EventId PrimaryNodeRecovered = new EventId(10000);
		}

		public class Payment {
			public static EventId ErrorProcessingBlockRewards = new EventId(10250);

			public static EventId ErrorProcessingIndividualBlockReward = new EventId(10500);

			public static EventId ErrorSavingSuccessfullySentPayments = new EventId(11000);

			public static EventId ErrorSendingIndividualPayment = new EventId(12000);

			public static EventId NoSharesFoundForBlockReward = new EventId(12500);

			public static EventId PaymentSenderServiceExecuting = new EventId(13000);

			public static EventId RewardsSavedForBlock = new EventId(14000);

			public static EventId StoppedPaymentsUntilPoolRestart = new EventId(15000);

			public static EventId TransactionNotFound = new EventId(16000);
		}
	}
}
