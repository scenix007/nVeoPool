#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog.Core;
using Serilog.Debugging;
using Serilog.Events;
using nVeoPool.PoolServer.Models;

namespace nVeoPool.PoolServer.Notifications {
	internal class EventNotificationSink : ILogEventSink {
		public EventNotificationSink(INotificationQueue notificationQueue) {
			_notificationQueue = notificationQueue;
		}

		#region ILogEventSink
		public void Emit(LogEvent logEvent) {
			try {
				if (logEvent.Properties.ContainsKey("EventId")) {
					var eventId = JsonConvert.DeserializeObject<EventId>(logEvent.Properties["EventId"].ToString());

					if (eventId.Id >= MINIMUM_CUSTOM_EVENT_ID) {
						_notificationQueue.Enqueue(new Notification {
							Event = logEvent,
							EventId = eventId.Id
						});

						SelfLog.WriteLine("{0} successfully added {1} to the notification queue", nameof(EventNotificationSink), eventId.Id);
					}
				}
			} catch (Exception e) {
				SelfLog.WriteLine("Exception in {0} while attempting to queue event: {1}", nameof(EventNotificationSink), e);
			}
		}
		#endregion

		private const int MINIMUM_CUSTOM_EVENT_ID = 1000;
		private readonly INotificationQueue _notificationQueue;
	}
}
