#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Rpc;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.PoW;

namespace nVeoPool.PoolServer.WorkProtocol {
	internal class ShareProcessorFactory : IShareProcessorFactory {
		public ShareProcessorFactory(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, ILogger<ShareProcessor> logger, IPoolServerConfiguration poolServerConfiguration, IPoWAlgorithm powAlgorithm, IRpcClientAdapterFactory rpcClientAdapterFactory, IShareQueueFactory shareQueueFactory) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_logger = logger;
			_poolServerConfiguration = poolServerConfiguration;
			_powAlgorithm = powAlgorithm;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
			_shareQueueFactory = shareQueueFactory;
		}

		#region IShareProcessorFactory
		public IShareProcessor GetShareProcessor(int port, int portInitialDifficulty, int portMinimumJobDifficulty) {
			return new ShareProcessor(_addressParser, _banManager, _cache, _logger, _poolServerConfiguration, port, portInitialDifficulty, portMinimumJobDifficulty, _powAlgorithm, _rpcClientAdapterFactory, _shareQueueFactory);
		}
		#endregion

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly ILogger<ShareProcessor> _logger;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IPoWAlgorithm _powAlgorithm;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
		private readonly IShareQueueFactory _shareQueueFactory;
	}
}