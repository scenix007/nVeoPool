#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Configuration.WorkProtocol;
using nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging;

namespace nVeoPool.PoolServer.WorkProtocol.Stratum {
	internal class StratumServerFactory : IStratumServerFactory {
		public StratumServerFactory(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, ICacheConnectionFactory cacheConnectionFactory, ILogger<StratumServerFactory> logger, ILoggerFactory loggerFactory, IPoolServerConfiguration poolServerConfiguration, IShareProcessorFactory shareProcessorFactory, IStratumMessageParser stratumMessageParser) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_cacheConnectionFactory = cacheConnectionFactory;
			_logger = logger;
			_loggerFactory = loggerFactory;
			_poolServerConfiguration = poolServerConfiguration;
			_shareProcessorFactory = shareProcessorFactory;
			_stratumMessageParser = stratumMessageParser;
		}

		#region StratumServerFactory
		public IStratumServer GetStratumServer(StratumProtocolConfiguration stratumProtocolConfiguration) {
			var shareProcessor = _shareProcessorFactory.GetShareProcessor(stratumProtocolConfiguration.ListenPort, stratumProtocolConfiguration.VarDiffConfiguration.InitialDifficulty, stratumProtocolConfiguration.VarDiffConfiguration.IsFixedDifficulty ? stratumProtocolConfiguration.VarDiffConfiguration.InitialDifficulty : stratumProtocolConfiguration.VarDiffConfiguration.MinimumDifficulty.Value);
			var stratumServer = new StratumServer(_addressParser, _banManager, _cache, _cacheConnectionFactory, _loggerFactory.CreateLogger<StratumServer>(), _loggerFactory, _poolServerConfiguration, shareProcessor, _stratumMessageParser, stratumProtocolConfiguration);

			_logger.LogInformation("Created Stratum server on {ListenAddress}:{ListenPort}", stratumProtocolConfiguration.ListenAddress, stratumProtocolConfiguration.ListenPort);

			return stratumServer;
		}
		#endregion

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly ICacheConnectionFactory _cacheConnectionFactory;
		private readonly ILogger<StratumServerFactory> _logger;
		private readonly ILoggerFactory _loggerFactory;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IShareProcessorFactory _shareProcessorFactory;
		private readonly IStratumMessageParser _stratumMessageParser;
	}
}