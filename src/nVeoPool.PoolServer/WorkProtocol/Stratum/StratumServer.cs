#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Net.Sockets;
using System.Reactive.Concurrency;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using nVeoPool.Common;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Configuration.WorkProtocol;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging;
using nVeoPool.TcpServer;
using nVeoPool.TcpServer.Events;

namespace nVeoPool.PoolServer.WorkProtocol.Stratum {
	internal class StratumServer : TcpServerBase, IStratumServer {
		public StratumServer(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, ICacheConnectionFactory cacheConnectionFactory, ILogger<StratumServer> logger, ILoggerFactory loggerFactory, IPoolServerConfiguration poolServerConfiguration, IShareProcessor shareProcessor, IStratumMessageParser stratumMessageParser, StratumProtocolConfiguration stratumProtocolConfiguration) : base(stratumProtocolConfiguration.BacklogCount, stratumProtocolConfiguration.BufferSize, stratumProtocolConfiguration.InitialBufferCount, logger, new EventLoopScheduler(t => new Thread(t) { IsBackground = false } )) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_cancellationTokenSource = new CancellationTokenSource();
			_loggerFactory = loggerFactory;
			_shareProcessor = shareProcessor;
			_stratumMessageParser = stratumMessageParser;
			_stratumProtocolConfiguration = stratumProtocolConfiguration;
			_workIsAvailable = true;

			EventStream.Subscribe(e => Task.Run(async () => await HandleEventAsync(e)), HandleError);

			cacheConnectionFactory.GetSubscriber().SubscribeImmediate<Work>(poolServerConfiguration.ChannelConfiguration.NewMiningJob, OnNewWorkMessageReceived);
			cacheConnectionFactory.GetSubscriber().SubscribeImmediate<WorkerDifficulty>(poolServerConfiguration.ChannelConfiguration.WorkerDifficultyChanged(stratumProtocolConfiguration.ListenPort), OnDifficultyChangedMessageReceived);
			cacheConnectionFactory.GetSubscriber().SubscribeImmediate<long>(poolServerConfiguration.ChannelConfiguration.WorkUnavailable, OnWorkUnavailbleMessageReceived);

			StartRemoveInactiveClientsLoop(_cancellationTokenSource.Token);
		}

		#region IDisposable
		public void Dispose() {
			_cancellationTokenSource.Cancel();

			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion

		#region TcpServerBase
		protected override TcpClientBase CreateClient(ulong clientId, Socket socket) {
			return new StratumClient(_addressParser, _banManager, _cache, clientId, _loggerFactory.CreateLogger<StratumClient>(), _shareProcessor, socket, _stratumMessageParser, _stratumProtocolConfiguration);
		}
		#endregion

		private void HandleError(Exception ex) {
			_logger.LogError(ex, "Exception during {ServerType} execution on port {ListenPort}", nameof(StratumServer), _stratumProtocolConfiguration.ListenPort);
		}

		private async Task HandleEventAsync(IServerEvent @event) {
			switch (@event) {
				case ClientConnectedEvent clientConnectedEvent:
					_logger.LogInformation("Accepted new client with id {ClientId} on port {ListenPort}", clientConnectedEvent.Client.Id, _stratumProtocolConfiguration.ListenPort);
				break;

				case ClientDisconnectedEvent clientDisconnectedEvent:
					_logger.LogInformation("Client with id {ClientId} disconnected on port {ListenPort}", clientDisconnectedEvent.Client.Id, _stratumProtocolConfiguration.ListenPort);
				break;

				case DataReceivedEvent dataReceivedEvent:
					var client = (StratumClient)dataReceivedEvent.Client;
					var serverMessage = await client.ProcessMessageAsync(dataReceivedEvent.Message);

					if (serverMessage != null) {
						SendMessage(client, serverMessage);
					}
				break;
			}
		}

		private void OnDifficultyChangedMessageReceived(String channelName, WorkerDifficulty workerDifficulty) {
			var desiredClient = Clients.Cast<StratumClient>().SingleOrDefault(c => workerDifficulty.FormattedMinerAddress.Equals(c.FormattedLogin));

			if (desiredClient != null) {
				var newJob = desiredClient.UpdateCurrentDifficulty(workerDifficulty.NewDifficulty);

				if (newJob != null && _workIsAvailable) {
					SendMessage(desiredClient, new StratumServerMessage {
						Method = StratumMethodType.NewJobDifficulty,
						Result = new StratumServerMessageResult { JobDifficulty = newJob.JobDifficulty }
					});
				}
			}
		}

		private void OnNewWorkMessageReceived(String channelName, Work newWork) {
			foreach (var client in Clients.Cast<StratumClient>().Where(c => !String.IsNullOrWhiteSpace(c.FormattedLogin))) {
				var newJob = client.UpdateCurrentJob(newWork);

				if (newJob != null) {
					SendMessage(client, new StratumServerMessage {
						Method = StratumMethodType.NewBlockHash,
						Result = new StratumServerMessageResult { BlockHash =  newJob.BlockHash }
					});
				}
			}

			_workIsAvailable = true;
		}

		private void OnWorkUnavailbleMessageReceived(String channelName, long lastAvailableWorkReceivedOn) {
			if (_workIsAvailable) {
				_workIsAvailable = false;

				foreach (var client in Clients.Cast<StratumClient>().Where(c => !String.IsNullOrWhiteSpace(c.FormattedLogin))) {
					SendMessage(client, new StratumServerMessage {
						Error = new StratumError { ErrorCode = WorkProtocolErrorType.NoWorkAvailable },
						Method = StratumMethodType.NewBlockHash
					});
				}
			}
		}

		private void SendMessage(StratumClient client, StratumServerMessage message) {
			if (client.IsConnected) {
				var serializedMessageString = JsonConvert.SerializeObject(message, _jsonSerializerSettings);

				try {
					Send(client, Encoding.UTF8.GetBytes($"{serializedMessageString}\n"));
				} catch (Exception e) {
					_logger.LogError(e, "Exception while sending message {ServerMessage} to client {ClientId} on port {ListenPort}", serializedMessageString, client.Id, _stratumProtocolConfiguration.ListenPort);
				}
			} else {
				_logger.LogWarning("Attempted to send to disconnected client {ClientId} on port {ListenPort}: {Message}", client.Id, _stratumProtocolConfiguration.ListenPort, message);
			}
		}

		private void StartRemoveInactiveClientsLoop(CancellationToken cancellationToken) {
			if (_stratumProtocolConfiguration.IdleClientTimeoutSeconds > 0) {
				Task.Run(async () => {
					while (!cancellationToken.IsCancellationRequested) {
						var cutoffTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromSeconds(_stratumProtocolConfiguration.IdleClientTimeoutSeconds)).ToUnixTimeSeconds();

						foreach (var client in Clients.Cast<StratumClient>().Where(c => c.LastSubmittedOn > 0 && c.LastSubmittedOn < cutoffTime)) {
							_logger.LogInformation("Disconnecting idle client with id {ClientId} on port {ListenPort}", client.Id, _stratumProtocolConfiguration.ListenPort);

							try {
								client.Socket.Close(_stratumProtocolConfiguration.MaximumGracefulIdleDisconnectWaitSeconds);
							} catch (Exception e) {
								_logger.LogError(e, "Exception while disconnecting client with id {ClientId} on port {ListenPort}", client.Id, _stratumProtocolConfiguration.ListenPort);
							}
						}

						await Task.Delay(TimeSpan.FromSeconds(_stratumProtocolConfiguration.IdleClientTimeoutSeconds), cancellationToken);
					}
				}, cancellationToken);
			} else {
				_logger.LogInformation("Disabled idle client disconnect on port {ListenPort}", _stratumProtocolConfiguration.ListenPort);
			}
		}

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly CancellationTokenSource _cancellationTokenSource;
		private readonly ILoggerFactory _loggerFactory;
		private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
			NullValueHandling = NullValueHandling.Ignore
		};
		private readonly IShareProcessor _shareProcessor;
		private readonly IStratumMessageParser _stratumMessageParser;
		private readonly StratumProtocolConfiguration _stratumProtocolConfiguration;
		private bool _workIsAvailable;
	}
}