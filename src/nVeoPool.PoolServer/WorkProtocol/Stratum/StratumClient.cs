#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration.WorkProtocol;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging;
using nVeoPool.TcpServer;

namespace nVeoPool.PoolServer.WorkProtocol.Stratum {
	internal class StratumClient : TcpClientBase {
		public StratumClient(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, ulong id, ILogger<StratumClient> logger, IShareProcessor shareProcessor, Socket socket, IStratumMessageParser stratumMessageParser, StratumProtocolConfiguration stratumProtocolConfiguration) : base(id, socket, stratumProtocolConfiguration.MaximumMessageSizeBytes) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			FormattedLogin = String.Empty;
			_logger = logger;
			_shareProcessor = shareProcessor;
			_stratumMessageParser = stratumMessageParser;
			_stratumProtocolConfiguration = stratumProtocolConfiguration;
		}

		public String FormattedLogin { get; private set; }

		public long LastSubmittedOn { get; private set; }

		public async Task<StratumServerMessage> ProcessMessageAsync(String message) {
			try {
				var clientMessage = _stratumMessageParser.ParseMessage(message);
				if (clientMessage.Parameters == null) {
					return new StratumServerMessage {
						Error = new StratumError { ErrorCode = WorkProtocolErrorType.Generic, Message = MALFORMED_CLIENT_MESSAGE_ERROR_MESSAGE }
					};
				}

				switch (clientMessage.Method) {
					case StratumMethodType.Submit:
						return await HandleSubmitAsync(clientMessage);

					case StratumMethodType.Subscribe:
						return await HandleSubscribeAsync(clientMessage);

					default:
						_logger.LogWarning("Unknown method {UnknownMethod} from client with id {ClientId} on port {ListenPort}", clientMessage.Method, Id, _stratumProtocolConfiguration.ListenPort);

						return new StratumServerMessage {
							Error = new StratumError { ErrorCode = WorkProtocolErrorType.Generic, Message = MALFORMED_CLIENT_MESSAGE_ERROR_MESSAGE }
						};
				}
			} catch (Exception e) {
				_logger.LogError(e, "Malformed message from client with id {ClientId} on port {ListenPort}: {Message}", Id, _stratumProtocolConfiguration.ListenPort, message);
			}

			return null;
		}

		public StratumServerMessageResult UpdateCurrentDifficulty(int newDifficulty) {
			if (newDifficulty != _currentJob.JobDifficulty) {
				_currentJob = new StratumServerMessageResult {
					BlockHash = _currentJob.BlockHash,
					JobDifficulty = newDifficulty,
				};

				return _currentJob;
			}

			return null;
		}

		public StratumServerMessageResult UpdateCurrentJob(Work newWork) {
			if (!newWork.BlockHash.Equals(_currentJob.BlockHash)) {
				_currentJob = new StratumServerMessageResult {
					BlockHash = newWork.BlockHash,
					JobDifficulty = _currentJob.JobDifficulty,
				};

				return _currentJob;
			}

			return null;
		}

		private async Task<StratumServerMessage> HandleSubmitAsync(StratumClientMessage clientMessage) {
			var resultMessage = new StratumServerMessage { Id = clientMessage.Id };
			var (error, _, submittedOn) = await _shareProcessor.ProcessShareSubmitAsync(clientMessage.Parameters.Id, clientMessage.Parameters.Nonce);

			if (submittedOn.HasValue) {
				LastSubmittedOn = submittedOn.Value;

				resultMessage.Result = new StratumServerMessageResult { Accepted = 1 };
			} else if (error.HasValue) {
				resultMessage.Error = new StratumError { ErrorCode = error.Value };
			}

			return resultMessage;
		}

		private async Task<StratumServerMessage> HandleSubscribeAsync(StratumClientMessage clientMessage) {
			if (!_addressParser.ValidateMinerAddress(clientMessage.Parameters.Id)) {
				return new StratumServerMessage {
					Error = new StratumError { ErrorCode = WorkProtocolErrorType.InvalidAddress }
				};
			}

			var miningDataTask = _cache.GetNodeMiningDataAsync();
			var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(clientMessage.Parameters.Id);

			if (!await _banManager.MinerIsAllowedAsync(_addressParser.ParseMinerAddress(formattedMinerAddress).PublicKey)) {
				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = WorkProtocolErrorType.Banned }
				};
			}

			var currentJobDifficulty = await _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress, _stratumProtocolConfiguration.ListenPort);
			if (!currentJobDifficulty.HasValue) {
				currentJobDifficulty = _stratumProtocolConfiguration.VarDiffConfiguration.InitialDifficulty;
				_cache.UpdateWorkerCurrentJobDifficultyImmediate(formattedMinerAddress, _stratumProtocolConfiguration.ListenPort, _stratumProtocolConfiguration.VarDiffConfiguration.InitialDifficulty);
			}

			_currentJob = new StratumServerMessageResult { JobDifficulty = currentJobDifficulty };

			FormattedLogin = formattedMinerAddress;

			_logger.LogInformation("Subscribed miner {MinerAddress} with id {ClientId} on port {ListenPort}", clientMessage.Parameters.Id, Id, _stratumProtocolConfiguration.ListenPort);
			
			var miningData = await miningDataTask;
			if (String.IsNullOrWhiteSpace(miningData.BlockHash)) {
				_logger.LogWarning("Cache returned null mining data for {MinerAddress} on port {ListenPort}", clientMessage.Parameters.Id, _stratumProtocolConfiguration.ListenPort);

				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = WorkProtocolErrorType.NoWorkAvailable }
				};
			}

			_currentJob.BlockHash = miningData.BlockHash;

			_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress, _stratumProtocolConfiguration.ListenPort);
			
			return new StratumServerMessage {
				Id = clientMessage.Id,
				Result = _currentJob
			};
		}

		private const String MALFORMED_CLIENT_MESSAGE_ERROR_MESSAGE = "Malformed client message received";
		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private StratumServerMessageResult _currentJob;
		private readonly ILogger<StratumClient> _logger;
		private readonly IShareProcessor _shareProcessor;
		private readonly IStratumMessageParser _stratumMessageParser;
		private readonly StratumProtocolConfiguration _stratumProtocolConfiguration;
	}
}