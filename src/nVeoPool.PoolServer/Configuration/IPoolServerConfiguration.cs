#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using nVeoPool.Common.Configuration;
using nVeoPool.PoolServer.Configuration.Banning;
using nVeoPool.PoolServer.Configuration.Notifications;
using nVeoPool.PoolServer.Configuration.Payment;
using nVeoPool.PoolServer.Configuration.Services;
using nVeoPool.PoolServer.Configuration.VarDiff;
using nVeoPool.PoolServer.Configuration.WorkProtocol;

namespace nVeoPool.PoolServer.Configuration {
	internal interface IPoolServerConfiguration : IServerConfiguration {
		IBanningConfiguration BanningConfiguration { get; }

		INotificationsConfiguration NotificationsConfiguration { get; }

		IPaymentConfiguration PaymentConfiguration { get; }

		IServicesConfiguration ServicesConfiguration { get; }

		IVarDiffConfiguration VarDiffConfiguration { get; }

		IWorkProtocolConfiguration WorkProtocolConfiguration { get; }
	}
}
