#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;

namespace nVeoPool.PoolServer.Configuration.WorkProtocol {
	internal class WorkProtocolConfiguration : IWorkProtocolConfiguration {
		#region IWorkProtocolConfiguration
		[JsonIgnore]
		public List<int> FixedDifficultyListenPorts {
			get {
				var listenPorts = new List<int>();

				if (GetWorkProtocolConfiguration != null && GetWorkProtocolConfiguration.VarDiffConfiguration.IsFixedDifficulty) {
					listenPorts.Add(GetWorkProtocolConfiguration.ListenPort);
				}
				if (StratumProtocolConfiguration != null && StratumProtocolConfiguration.Any()) {
					listenPorts.AddRange(StratumProtocolConfiguration.Where(s => s.VarDiffConfiguration.IsFixedDifficulty).Select(s => s.ListenPort));
				}

				return listenPorts;
			}
		}
		
		[JsonProperty(PropertyName = "GetWork")]
		public GetWorkProtocolConfiguration GetWorkProtocolConfiguration { get; set; }

		[JsonIgnore]
		public List<IndividualWorkProtocolConfiguration> Listeners {
			get {
				var listenPorts = new List<IndividualWorkProtocolConfiguration>();

				if (GetWorkProtocolConfiguration != null) {
					listenPorts.Add(GetWorkProtocolConfiguration);
				}
				if (StratumProtocolConfiguration != null && StratumProtocolConfiguration.Any()) {
					listenPorts.AddRange(StratumProtocolConfiguration);
				}

				return listenPorts;
			}
		}

		[JsonIgnore]
		public List<int> ListenPorts {
			get {
				var listenPorts = new List<int>();

				if (GetWorkProtocolConfiguration != null) {
					listenPorts.Add(GetWorkProtocolConfiguration.ListenPort);
				}
				if (StratumProtocolConfiguration != null && StratumProtocolConfiguration.Any()) {
					listenPorts.AddRange(StratumProtocolConfiguration.Select(s => s.ListenPort));
				}

				return listenPorts;
			}
		}

		[DefaultValue(1000)]
		[JsonProperty(PropertyName = "MaximumShareBacklog")]
		public int MaximumAcceptedShareQueueSize { get; set; }

		[DefaultValue(1000)]
		[JsonProperty(PropertyName = "MaximumInvalidShareBacklog")]
		public int MaximumInvalidShareQueueSize { get; set; }

		[DefaultValue(5*60)]
		[JsonProperty(PropertyName = "MaximumStaleWorkDuration")]
		public uint MaximumStaleWorkDurationSeconds { get; set; }

		[DefaultValue(2)]
		public uint MaximumTrailingBlocks { get; set; }

		[JsonProperty(PropertyName = "RequireSubscribe")]
		public bool RequireSubscribeBeforeSubmit { get; set; }

		[JsonProperty(PropertyName = "Stratum")]
		public List<StratumProtocolConfiguration> StratumProtocolConfiguration { get; set; }

		[JsonIgnore]
		public List<IndividualWorkProtocolConfiguration> VariableDifficultyListeners {
			get {
				var listenPorts = new List<IndividualWorkProtocolConfiguration>();

				if (GetWorkProtocolConfiguration != null && !GetWorkProtocolConfiguration.VarDiffConfiguration.IsFixedDifficulty) {
					listenPorts.Add(GetWorkProtocolConfiguration);
				}
				if (StratumProtocolConfiguration != null && StratumProtocolConfiguration.Any()) {
					listenPorts.AddRange(StratumProtocolConfiguration.Where(s => !s.VarDiffConfiguration.IsFixedDifficulty));
				}

				return listenPorts;
			}
		}
		#endregion
	}
}