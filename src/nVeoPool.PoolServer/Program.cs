﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;
using nVeoPool.Common.Logging;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.WorkProtocol.Stratum;

namespace nVeoPool.PoolServer {
	public class Program {
		public static async Task Main(String[] args) {
			_webHost = new WebHostBuilder()
				.UseStartup<KestrelStartup>()
				#if DEBUG
				.UseSerilog((hostingContext, loggerConfiguration) => {
					SetSelfLogging();
					var loggerConfig = loggerConfiguration.MinimumLevel.Verbose().WriteTo.Console(LogEventLevel.Debug);

					SetHttpRequestLoggingFilter(loggerConfig);
				})
				#else
				.UseSerilog((hostingContext, loggerConfiguration) => {
					SetSelfLogging();
					var loggerConfig = loggerConfiguration.MinimumLevel.Verbose();
					var outputTemplate = SetExceptionEnricher(loggerConfig);

					loggerConfig.WriteTo.File(PoolServerConfiguration.Instance.LoggingConfiguration.FileName, PoolServerConfiguration.Instance.LoggingConfiguration.Level, buffered: true, flushToDiskInterval: TimeSpan.FromSeconds(30), outputTemplate: outputTemplate, retainedFileCountLimit: 365, rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true);

					SetEventNotificationSink(loggerConfig);
					SetHttpRequestLoggingFilter(loggerConfig);
				})
				#endif
				.UseKestrel(options => {
					if (PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration != null) {
						options.AddServerHeader = false;
						options.Limits.KeepAliveTimeout = TimeSpan.FromSeconds(PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.KeepAliveTimeoutSeconds);
						options.Limits.MaxConcurrentConnections = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumConcurrentConnections;
						options.Limits.MaxRequestBodySize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestBodySizeBytes;
						options.Limits.MaxRequestBufferSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestBufferSizeBytes;
						options.Limits.MaxRequestHeaderCount = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestHeaderCount;
						options.Limits.MaxRequestHeadersTotalSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestHeadersTotalSizeBytes;
						options.Limits.MaxRequestLineSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestLineSizeBytes;
						options.Limits.MaxResponseBufferSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumResponseBufferSizeBytes;
						options.Limits.RequestHeadersTimeout = TimeSpan.FromSeconds(PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.RequestHeadersTimeoutSeconds);
						options.Listen(PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenAddress, PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenPort);
					}
				})
				.Build();
			_webHost.Start();

			if (PoolServerConfiguration.Instance.WorkProtocolConfiguration.StratumProtocolConfiguration != null && PoolServerConfiguration.Instance.WorkProtocolConfiguration.StratumProtocolConfiguration.Any()) {
				var serviceProvider = KestrelStartup.ServiceProvider;
				var stratumServerFactory = (IStratumServerFactory)serviceProvider.GetService(typeof(IStratumServerFactory));

				foreach (var stratumProtocolConfiguration in PoolServerConfiguration.Instance.WorkProtocolConfiguration.StratumProtocolConfiguration) {
					var stratumServer = stratumServerFactory.GetStratumServer(stratumProtocolConfiguration);
					stratumServer.Start(new System.Net.IPEndPoint(stratumProtocolConfiguration.ListenAddress, stratumProtocolConfiguration.ListenPort));

					_stratumServers.Add(stratumServer);
				}
			}
			
			Console.CancelKeyPress += async (s,e) => await OnCancelKeyPress(s, e);
			Console.WriteLine($"{PoolServerConfiguration.Instance.ClusteringConfiguration.ServerType} pool server v{PoolServerConfiguration.Instance.Version} with id {PoolServerConfiguration.Instance.ClusteringConfiguration.ServerId} started...");
			Console.ReadLine();

			await Shutdown();
		}

		private static async Task OnCancelKeyPress(Object sender, ConsoleCancelEventArgs eventArgs) {
			await Shutdown();
		}

		private static void SetEventNotificationSink(LoggerConfiguration loggerConfiguration) {
			loggerConfiguration = loggerConfiguration.WriteTo.EventNotificationSink(new NotificationQueueFactory().GetNotificationQueue());
		}

		private static String SetExceptionEnricher(LoggerConfiguration loggerConfiguration) {
			if (!PoolServerConfiguration.Instance.LoggingConfiguration.IncludeStackTrace) {
				loggerConfiguration.Enrich.WithFormattedExceptions();
			}

			return "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{SourceContext}]{Scope}[{Level:u3}] {Message:lj}{NewLine}" + (PoolServerConfiguration.Instance.LoggingConfiguration.IncludeStackTrace ? "{Exception}" : "{FormattedException}");
		}

		private static void SetHttpRequestLoggingFilter(LoggerConfiguration loggerConfiguration) {
			if (PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration != null && !PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.EnableRequestLogging) {
				loggerConfiguration.Filter.ByExcluding(e => e.Properties.TryGetValue("SourceContext", out var sourceContext) && sourceContext.ToString().Contains("Microsoft.AspNetCore"));
			}
		}

		private static void SetSelfLogging() {
			if (PoolServerConfiguration.Instance.LoggingConfiguration.EnableSelfLogging) {
				var fileInfo = new FileInfo(PoolServerConfiguration.Instance.LoggingConfiguration.FileName);
				var selfLogPath = fileInfo.FullName.Replace(fileInfo.Name, $"self_{fileInfo.Name}");
				var streamWriter = new StreamWriter(selfLogPath, true, Encoding.UTF8) { AutoFlush = true };

				Serilog.Debugging.SelfLog.Enable(TextWriter.Synchronized(streamWriter));
			}
		}

		private static async Task Shutdown() {
			var gracefulCloseWaitSeconds = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration?.MaximumGracefulCloseWaitSeconds ?? 0;

			try {
				if (_stratumServers.Any()) {
					foreach (var stratumServer in _stratumServers) {
						try {
							stratumServer?.Dispose();
						} catch (Exception e) {
							Console.WriteLine($"Exception stopping Stratum server: {e}");
						}
					}
				}

				await _webHost.StopAsync(gracefulCloseWaitSeconds > 0 ? TimeSpan.FromSeconds(gracefulCloseWaitSeconds) : TimeSpan.FromMilliseconds(int.MaxValue));

				_webHost.Dispose();
			} finally {
				Process.GetCurrentProcess().Close();
			}
		}

		private static List<IStratumServer> _stratumServers = new List<IStratumServer>();
		private static IWebHost _webHost;
	}
}
