﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.ApiServer.RequestHandlers;
using nVeoPool.ApiServer.Routes;
using nVeoPool.ApiServer.Services;
using nVeoPool.Common;
using nVeoPool.Common.Configuration;
using nVeoPool.Common.Routing;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Configuration;
using nVeoPool.Data.Context;
using nVeoPool.Data.Repositories;

namespace nVeoPool.ApiServer {
	internal class KestrelStartup {
		public KestrelStartup() {
			_apiServerConfiguration = ApiServerConfiguration.Instance;
		}

		public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider) {
			app.UseCors(builder => {
				var allowedAccessOrigins = _apiServerConfiguration.HttpConfiguration.AllowedAccessOrigins ?? new List<String>();

				(allowedAccessOrigins.Any() ? builder.WithOrigins(allowedAccessOrigins.ToArray()) : builder.AllowAnyOrigin()).WithMethods("GET");
			});

			app.UseRouter(serviceProvider.GetService<IRouteConfigurator>().Configure);
		}
		
		public void ConfigureServices(IServiceCollection services) {
			services.AddCors();
			services.AddRouting();

			services.AddSingleton<IAddressParser, AddressParser>();
			services.AddSingleton<IAdminRouteConfigurator, AdminRouteConfigurator>();
			services.AddSingleton<IAdminRouteRequestHandler, AdminRouteRequestHandler>();
			services.AddSingleton<IApiServerConfiguration, ApiServerConfiguration>(provider => ApiServerConfiguration.Instance);
			services.AddSingleton<IBlockRepository, BlockRepository>(provider => new BlockRepository(new PostgresDatabaseContext(_apiServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<ICacheConnectionFactory, RedisConnectionFactory>(provider => new RedisConnectionFactory(_apiServerConfiguration.CacheConfiguration.ConnectionString));
			services.AddSingleton<IDistributedCacheAdapter, DistributedCacheAdapter>();
			services.AddSingleton<IDistributedCacheAdapterFactory, DistributedCacheAdapterFactory>();
			services.AddSingleton<IDistributedCacheKeyConfiguration, DistributedCacheKeyConfiguration>();
			services.AddSingleton<IHostedService, DataCacherService>();
			services.AddSingleton<IHostedService, DataCacherSynchronizationService>();
			services.AddSingleton<IHostedService, ServerStatusService>();
			services.AddSingleton<IMinerRouteConfigurator, MinerRouteConfigurator>();
			services.AddSingleton<IMinerRouteRequestHandler, MinerRouteRequestHandler>();
			services.AddSingleton<IMultiCacheAdapter, MultiCacheAdapter>();
			services.AddSingleton<IPaymentRepository, PaymentRepository>(provider => new PaymentRepository(new PostgresDatabaseContext(_apiServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IPoolRouteConfigurator, PoolRouteConfigurator>();
			services.AddSingleton<IPoolRouteRequestHandler, PoolRouteRequestHandler>();
			services.AddSingleton<IReaderWriterLock, GlobalStatisticsLock>();
			services.AddSingleton<IRepositoryCacheAdapter, RepositoryCacheAdapter>();
			services.AddSingleton<IRouteConfigurator, CompleteApiRouteConfigurator>();
			services.AddSingleton<IServerConfiguration, ApiServerConfiguration>(provider => ApiServerConfiguration.Instance);
			services.AddSingleton<IStatsRepository, StatsRepository>(provider => new StatsRepository(new PostgresDatabaseContext(_apiServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<ITimeSeriesRouteConfigurator, TimeSeriesRouteConfigurator>();
			services.AddSingleton<ITimeSeriesRouteRequestHandler, TimeSeriesRouteRequestHandler>();
		}

		private readonly IApiServerConfiguration _apiServerConfiguration;
	}
}
