#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Services;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.Services {
	internal class ServerStatusService : BackgroundServiceBase {
		public ServerStatusService(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ILogger<ServerStatusService> logger) : base(apiServerConfiguration.ClusteringConfiguration, logger, apiServerConfiguration.ServicesConfiguration.ServerStatusServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, $"{nameof(ServerStatusService)}_{apiServerConfiguration.ClusteringConfiguration.ServerId}") {
			_apiServerConfiguration = apiServerConfiguration;
			_cache = cache;
			_serverIsRegistered = false;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => {
				try {
					if (_serverIsRegistered) {
						await _cache.UnregisterServerAsync(ClusterServerCategory.API, _apiServerConfiguration.ClusteringConfiguration.ServerId);
					}
				} finally {
					LogStoppingMessage();
				}
			});

			while (!cancellationToken.IsCancellationRequested) {
				try {
					LogExecutingMessage();

					if (!_serverIsRegistered) {
						await _cache.RegisterServerAsync(ClusterServerCategory.API, _apiServerConfiguration.ClusteringConfiguration.ServerId, _apiServerConfiguration.ClusteringConfiguration.ServerType, _apiServerConfiguration.Version, GetConfigurationSections());
						
						_serverIsRegistered = true;
					}

					await _cache.UpdateServerHeartbeatAsync(_apiServerConfiguration.ClusteringConfiguration.ServerId);
				} catch (Exception e) {
					LogExceptionMessage(e);
				}

				LogSleepingMessage();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private IEnumerable<(String ConfigurationSection, String ConfigurationValue)> GetConfigurationSections() {
			var typeInfo = _apiServerConfiguration.GetType().GetTypeInfo();
			var properties = typeInfo.DeclaredProperties.Union(typeInfo.BaseType.GetTypeInfo().DeclaredProperties).ToList();
			
			return properties.Select(p => {
				var propertyName = p.Name;
				var jsonProperty = p.CustomAttributes.SingleOrDefault(c => c.AttributeType == typeof(JsonPropertyAttribute));
				if (jsonProperty != null) {
					propertyName = (String)jsonProperty.NamedArguments.SingleOrDefault(a => a.MemberName == "PropertyName").TypedValue.Value;
				}

				var propertyValue = p.GetValue(_apiServerConfiguration);

				return (propertyName, propertyValue != null ? JsonConvert.SerializeObject(propertyValue, _jsonSerializerSettings) : null);
			}).Where(p => p.Item2 != null && !_excludedConfigurationSections.Contains(p.Item1));
		}

		private readonly IApiServerConfiguration _apiServerConfiguration;
		private readonly IDistributedCacheAdapter _cache;
		private readonly ISet<String> _excludedConfigurationSections = new HashSet<String> { "Cache", "ChannelConfiguration", "Database", "Http", "Instance", "NodeRpc", "Version" };
		private bool _serverIsRegistered;
	}
}
