#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.ApiServer.Services.RuntimeData;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;

namespace nVeoPool.ApiServer.Services {
	internal class DataCacherSynchronizationService : ReportingBackgroundServiceBase {
		public DataCacherSynchronizationService(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ICacheConnectionFactory cacheConnectionFactory, IReaderWriterLock globalStatisticsLock, ILogger<DataCacherSynchronizationService> logger) : base(cache, apiServerConfiguration.ClusteringConfiguration, logger, apiServerConfiguration.ServicesConfiguration.DataCacherSynchronizationServiceUpdateIntervalSeconds, ClusterServerType.Slave, nameof(DataCacherSynchronizationService)) {
			_apiServerConfiguration = apiServerConfiguration;
			_globalStatisticsLock = globalStatisticsLock;
			_runtimeData = new DataCacherSynchronizationData();

			if (_enabled) {
				cacheConnectionFactory.GetSubscriber().SubscribeSimpleImmediate(_apiServerConfiguration.ChannelConfiguration.StatisticsUpdateLockAcquired, OnStatisticsUpdateLockAcquiredMessageReceived);
				cacheConnectionFactory.GetSubscriber().SubscribeSimpleImmediate(_apiServerConfiguration.ChannelConfiguration.StatisticsUpdateLockReleased, OnStatisticsUpdateLockReleasedMessageReceived);
			}
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					if (_shouldEnterLock) {
						_shouldEnterLock = false;

						if (await _globalStatisticsLock.TryEnterWriteLockAsync(-1, cancellationToken)) {
							_runtimeData.WriteLockHeld = true;

							_logger.LogDebug("{BackgroundService} successfully acquired write lock", _serviceName);
						} else {
							_logger.LogWarning("{BackgroundService} unable to acquire write lock", _serviceName);
						}
					}
					if (_shouldExitLock) {
						_shouldExitLock = false;

						_globalStatisticsLock.ExitWriteLock();

						_runtimeData.WriteLockHeld = false;

						_logger.LogDebug("{BackgroundService} successfully released write lock", _serviceName);
					}

					await UpdateServiceRuntimeDataAsync(_runtimeData);
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync(false);
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private void OnStatisticsUpdateLockAcquiredMessageReceived(String channelName, long lockAcquiredOn) {
			if (_lastLockReleasedOn > lockAcquiredOn) {
				_logger.LogWarning("{BackgroundService} received lock acquired message for {LockAcquiredOn} but last lock was released on {LastLockReleasedOn}", _serviceName, lockAcquiredOn, _lastLockReleasedOn);
			} else {
				_lastLockAcquiredOn = lockAcquiredOn;
				_shouldEnterLock = true;

				_logger.LogDebug("{BackgroundService} marked lock for entry", _serviceName);
			}
		}

		private void OnStatisticsUpdateLockReleasedMessageReceived(String channelName, long lockReleasedOn) {
			if (_lastLockAcquiredOn > lockReleasedOn) {
				_logger.LogWarning("{BackgroundService} received lock released message for {LockReleasedOn} but last lock was acquired on {LastLockAcquiredOn}", _serviceName, lockReleasedOn, _lastLockAcquiredOn);
			} else {
				_lastLockReleasedOn = lockReleasedOn;
				_shouldExitLock = true;

				_logger.LogDebug("{BackgroundService} marked lock for exit", _serviceName);
			}
		}

		private readonly IApiServerConfiguration _apiServerConfiguration;
		private readonly IReaderWriterLock _globalStatisticsLock;
		private readonly DataCacherSynchronizationData _runtimeData;
		private long _lastLockAcquiredOn;
		private long _lastLockReleasedOn;
		private bool _shouldEnterLock;
		private bool _shouldExitLock;
	}
}
