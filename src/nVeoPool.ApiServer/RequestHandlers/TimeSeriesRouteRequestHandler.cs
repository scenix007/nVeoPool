#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class TimeSeriesRouteRequestHandler : RequestHandlerBase, ITimeSeriesRouteRequestHandler {
		public TimeSeriesRouteRequestHandler(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ILogger<TimeSeriesRouteRequestHandler> logger, IRepositoryCacheAdapter repositoryCache) : base(apiServerConfiguration, cache, logger) {
			_repositoryCache = repositoryCache;
		}

		#region ITimeSeriesRouteRequestHandler
		public Task HandleGetHashrateSeriesForMinerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var minerPublicKey = context.Request.Query.TryGetValue("address", out var address) ? address.ToString() : String.Empty;
				var minerWorkerId = context.Request.Query.TryGetValue("workerId", out var workerId) ? workerId.ToString() : null;
				var endTime = ParseIntOrDefault(context.Request.Query, "end", 0);
				var startTime = ParseIntOrDefault(context.Request.Query, "start", 0);
				var currentTime = DateTime.UtcNow;
				var timestampEnd = new DateTimeOffset(currentTime).ToUnixTimeSeconds();
				var timestampStart = new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumQueryAgeSeconds)).ToUnixTimeSeconds();

				var minerHashrateSeries = await _repositoryCache.GetTimeSeriesForMinerHashrateAsync(minerPublicKey, minerWorkerId, timestampStart, timestampEnd, ParseTimesQueryToSeconds(context.Request.Query));

				if (minerHashrateSeries != null) {
					return (null, JsonConvert.SerializeObject(minerHashrateSeries.Select(m => new { Window = m.WindowLengthSeconds / 3600, Series = FilterTimeSeries(timestampEnd, m.Series, startTime, endTime) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetHashrateSeriesForMinerAsync));
		}

		public Task HandleGetHashrateSeriesForPoolAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var endTime = ParseIntOrDefault(context.Request.Query, "end", 0);
				var startTime = ParseIntOrDefault(context.Request.Query, "start", 0);
				var currentTime = DateTime.UtcNow;
				var timestampEnd = new DateTimeOffset(currentTime).ToUnixTimeSeconds();
				var timestampStart = new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumQueryAgeSeconds)).ToUnixTimeSeconds();

				var poolHashrateSeries = await _repositoryCache.GetTimeSeriesForPoolHashrateAsync(timestampStart, timestampEnd, ParseTimesQueryToSeconds(context.Request.Query));

				if (poolHashrateSeries != null) {
					return (null, JsonConvert.SerializeObject(poolHashrateSeries.Select(p => new { Window = p.WindowLengthSeconds / 3600, Series = FilterTimeSeries(timestampEnd, p.Series, startTime, endTime) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetHashrateSeriesForPoolAsync));
		}

		public Task HandleGetShareSeriesForMinerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var minerPublicKey = context.Request.Query.TryGetValue("address", out var address) ? address.ToString() : String.Empty;
				var minerWorkerId = context.Request.Query.TryGetValue("workerId", out var workerId) ? workerId.ToString() : null;
				var endTime = ParseIntOrDefault(context.Request.Query, "end", 0);
				var startTime = ParseIntOrDefault(context.Request.Query, "start", 0);
				var currentTime = DateTime.UtcNow;
				var timestampEnd = new DateTimeOffset(currentTime).ToUnixTimeSeconds();
				var timestampStart = new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumQueryAgeSeconds)).ToUnixTimeSeconds();

				var minerSharesSeries = await _repositoryCache.GetTimeSeriesForMinerSharesAsync(minerPublicKey, minerWorkerId, timestampStart, timestampEnd, ParseTimesQueryToSeconds(context.Request.Query));

				if (minerSharesSeries != null) {
					return (null, JsonConvert.SerializeObject(minerSharesSeries.Select(m => new { Window = m.WindowLengthSeconds / 3600, Series = FilterTimeSeries(timestampEnd, m.Series, startTime, endTime) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetShareSeriesForMinerAsync));
		}

		public Task HandleGetShareSeriesForPoolAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var endTime = ParseIntOrDefault(context.Request.Query, "end", 0);
				var startTime = ParseIntOrDefault(context.Request.Query, "start", 0);
				var currentTime = DateTime.UtcNow;
				var timestampEnd = new DateTimeOffset(currentTime).ToUnixTimeSeconds();
				var timestampStart = new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumQueryAgeSeconds)).ToUnixTimeSeconds();

				var poolSharesSeries = await _repositoryCache.GetTimeSeriesForPoolSharesAsync(timestampStart, timestampEnd, ParseTimesQueryToSeconds(context.Request.Query));

				if (poolSharesSeries != null) {
					return (null, JsonConvert.SerializeObject(poolSharesSeries.Select(p => new { Window = p.WindowLengthSeconds / 3600, Series = FilterTimeSeries(timestampEnd, p.Series, startTime, endTime) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetShareSeriesForPoolAsync));
		}

		public Task HandleGetWorkerSeriesForMinerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var minerPublicKey = context.Request.Query.TryGetValue("address", out var address) ? address.ToString() : String.Empty;
				var endTime = ParseIntOrDefault(context.Request.Query, "end", 0);
				var startTime = ParseIntOrDefault(context.Request.Query, "start", 0);
				var currentTime = DateTime.UtcNow;
				var timestampEnd = new DateTimeOffset(currentTime).ToUnixTimeSeconds();
				var timestampStart = new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumQueryAgeSeconds)).ToUnixTimeSeconds();

				var minerWorkersSeries = await _repositoryCache.GetTimeSeriesForMinerWorkersAsync(minerPublicKey, timestampStart, timestampEnd, ParseTimesQueryToSeconds(context.Request.Query));

				if (minerWorkersSeries != null) {
					return (null, JsonConvert.SerializeObject(minerWorkersSeries.Select(m => new { Window = m.WindowLengthSeconds / 3600, Series = FilterTimeSeries(timestampEnd, m.Series, startTime, endTime) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetWorkerSeriesForMinerAsync));
		}

		public Task HandleGetWorkerSeriesForPoolAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var endTime = ParseIntOrDefault(context.Request.Query, "end", 0);
				var startTime = ParseIntOrDefault(context.Request.Query, "start", 0);
				var currentTime = DateTime.UtcNow;
				var timestampEnd = new DateTimeOffset(currentTime).ToUnixTimeSeconds();
				var timestampStart = new DateTimeOffset(currentTime - TimeSpan.FromSeconds(_apiServerConfiguration.QueryConfiguration.TimeSeriesConfiguration.MaximumQueryAgeSeconds)).ToUnixTimeSeconds();

				var poolWorkersSeries = await _repositoryCache.GetTimeSeriesForPoolWorkersAsync(timestampStart, timestampEnd, ParseTimesQueryToSeconds(context.Request.Query));

				if (poolWorkersSeries != null) {
					return (null, JsonConvert.SerializeObject(poolWorkersSeries.Select(p => new { Window = p.WindowLengthSeconds / 3600, Series = FilterTimeSeries(timestampEnd, p.Series, startTime, endTime) }), _jsonSerializerSettings));
				}

				return (404, String.Empty);
			}, nameof(HandleGetWorkerSeriesForPoolAsync));
		}
		#endregion

		private IEnumerable<T> FilterTimeSeries<T>(long currentTimestamp, IEnumerable<T> timeSeries, int startTime, int endTime) where T : TimeSeriesDatapointBase {
			var returnSeries = timeSeries;

			if (endTime < 0) {
				returnSeries = returnSeries.SkipWhile(s => s.Timestamp > (currentTimestamp + endTime));
			} else if (endTime > 0) {
				returnSeries = returnSeries.SkipWhile(s => s.Timestamp > endTime);
			}

			if (startTime < 0) {
				returnSeries = returnSeries.TakeWhile(s => s.Timestamp >= (currentTimestamp + startTime));
			} else if (startTime > 0) {
				returnSeries = returnSeries.TakeWhile(s => s.Timestamp >= startTime);
			}

			return returnSeries;
		}

		private readonly IRepositoryCacheAdapter _repositoryCache;
	}
}
