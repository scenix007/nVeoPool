#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class AdminRouteRequestHandler : RequestHandlerBase, IAdminRouteRequestHandler {
		public AdminRouteRequestHandler(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ILogger<AdminRouteRequestHandler> logger, IMultiCacheAdapter multiCacheAdapter) : base(apiServerConfiguration, cache, logger) {
			_multiCacheAdapter = multiCacheAdapter;
		}

		#region IAdminRouteRequestHandler
		public Task HandleGetAllConfigSectionsAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var serverId = (String)context.GetRouteValue("serverId");
				var configSections = await _multiCacheAdapter.GetServerConfigurationSectionsAsync(serverId);

				if (configSections != null) {
					return (null, JsonConvert.SerializeObject(configSections.Where(c => c != null).SelectMany(c => c).Distinct().OrderBy(c => c), _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null config sections for server {ServerId}", serverId);

				return (404, null);
			}, nameof(HandleGetAllConfigSectionsAsync));
		}

		public Task HandleGetAllServersAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var apiServers = await _multiCacheAdapter.GetRegisteredServersAsync(ClusterServerCategory.API);
				var poolServers = await _multiCacheAdapter.GetRegisteredServersAsync(ClusterServerCategory.Pool);

				if (poolServers != null && apiServers != null) {
					return (null, JsonConvert.SerializeObject(new { API = apiServers.SelectMany(a => a).Distinct(), Pool = poolServers.SelectMany(p => p).Distinct() }, _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null server list");

				return (404, null);
			}, nameof(HandleGetAllServersAsync));
		}

		public Task HandleGetAllServicesForServerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var serverId = (String)context.GetRouteValue("serverId");
				var services = await _multiCacheAdapter.GetRegisteredServicesForServerAsync(serverId);

				if (services != null) {
					return (null, JsonConvert.SerializeObject(services.Where(s => s != null).SelectMany(s => s).Distinct().OrderBy(s => s).ToArray(), _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null services list for server {ServerId}", serverId);

				return (404, null);
			}, nameof(HandleGetAllServicesForServerAsync));
		}

		public Task HandleGetConfigSectionForServerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var configSectionId = (String)context.GetRouteValue("configSection");
				var serverId = (String)context.GetRouteValue("serverId");
				var configSection = (await _multiCacheAdapter.GetServerConfigurationSectionAsync(serverId, configSectionId)).FirstOrDefault(c => c != null);

				if (configSection != null) {
					return (null, JsonConvert.SerializeObject(JsonConvert.DeserializeObject<dynamic>(configSection), _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null config section for {ConfigurationSection} on {ServerId}", configSectionId, serverId);

				return (404, String.Empty);
			}, nameof(HandleGetConfigSectionForServerAsync));
		}

		public Task HandleGetServiceForServerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var serverId = (String)context.GetRouteValue("serverId");
				var serviceId = (String)context.GetRouteValue("serviceId");
				var serviceStatus = (await _multiCacheAdapter.GetServiceStatusAsync(serverId, serviceId)).FirstOrDefault(s => s != null);

				if (serviceStatus != null) {
					serviceStatus.Data = !String.IsNullOrWhiteSpace(serviceStatus.Data as String) ? JsonConvert.DeserializeObject<dynamic>((String)serviceStatus.Data) : serviceStatus.Data;
					
					return (null, JsonConvert.SerializeObject(serviceStatus, _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null service status for {ServiceName} on {ServerId}", serviceId, serverId);

				return (404, String.Empty);
			}, nameof(HandleGetServiceForServerAsync));
		}

		public Task HandleGetSingleServerAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var serverId = (String)context.GetRouteValue("serverId");
				var serverStatus = (await _multiCacheAdapter.GetServerStatusAsync(serverId)).FirstOrDefault(s => s != null);

				if (serverStatus != null) {
					serverStatus.Uptime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds() - serverStatus.RegisteredOn;

					return (null, JsonConvert.SerializeObject(serverStatus, _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null server status for server {ServerId}", serverId);

				return (404, String.Empty);
			}, nameof(HandleGetSingleServerAsync));
		}
		#endregion

		private readonly IMultiCacheAdapter _multiCacheAdapter;
	}
}
