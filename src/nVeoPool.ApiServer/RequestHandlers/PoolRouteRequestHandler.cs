#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class PoolRouteRequestHandler : RequestHandlerBase, IPoolRouteRequestHandler {
		public PoolRouteRequestHandler(IApiServerConfiguration apiServerConfiguration, IDistributedCacheAdapter cache, ILogger<PoolRouteRequestHandler> logger, IRepositoryCacheAdapter repositoryCache) : base(apiServerConfiguration, cache, logger) {
			_repositoryCache = repositoryCache;
		}

		#region IPoolRouteRequestHandler
		public Task HandleGetPoolAggregateStatsAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var poolStats = await _repositoryCache.GetStatsForPoolAsync(ParseTimesQueryToSeconds(context.Request.Query));

				if (poolStats != null) {
					return (null, JsonConvert.SerializeObject(poolStats.Select(p => new { Window = p.WindowLengthSeconds / 3600, Stats = p }), _jsonSerializerSettings));
				}

				_logger.LogWarning("Cache returned null aggregate pool stats");

				return (404, String.Empty);
			}, nameof(HandleGetPoolAggregateStatsAsync));
		}

		public Task HandleGetPoolRecentBlocksAsync(HttpContext context) {
			return HandleResponseAsync(context, async () => {
				var limit = ParseLimitOrDefault(context.Request.Query, DEFAULT_POOL_BLOCK_LIMIT);
				var poolBlocks = await _repositoryCache.GetAllBlockCandidatesAsync(_apiServerConfiguration.QueryConfiguration.MaximumBlockCandidates);
				if (limit > 0) {
					poolBlocks = poolBlocks.Take(limit);
				}

				return (null, JsonConvert.SerializeObject(poolBlocks, _jsonSerializerSettings));
			}, nameof(HandleGetPoolRecentBlocksAsync));
		}
		#endregion

		private const int DEFAULT_POOL_BLOCK_LIMIT = 50;
		private readonly IRepositoryCacheAdapter _repositoryCache;
	}
}
