#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.ApiServer.Configuration.Query {
	internal class QueryConfiguration : IQueryConfiguration {
		//Attributes per https://github.com/JamesNK/Newtonsoft.Json/issues/1199
		public QueryConfiguration([JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate), DefaultValue(DEFAULT_MAXIMUM_BLOCK_CANDIDATES)]int maximumBlockCandidates, int maximumMinerPayments, int maximumTopMiners, TimeSeriesConfiguration timeSeriesConfiguration) {
			MaximumBlockCandidates = maximumBlockCandidates;
			MaximumMinerPayments = maximumMinerPayments;
			MaximumTopMiners = maximumTopMiners;
			TimeSeriesConfiguration = timeSeriesConfiguration ?? new TimeSeriesConfiguration();
		}

		#region IQueryConfiguration
		[DefaultValue(DEFAULT_MAXIMUM_BLOCK_CANDIDATES)]
		[JsonProperty(PropertyName = "MaximumBlocks", DefaultValueHandling = DefaultValueHandling.Populate)]
		public int MaximumBlockCandidates { get; set; }

		[DefaultValue(0)]
		public int MaximumMinerPayments { get; set; }

		[DefaultValue(0)]
		public int MaximumTopMiners { get; set; }

		[JsonIgnore]
		public List<uint> QueryableHourRanges => new List<uint> { 1, 6, 12, 24 };

		[JsonProperty(PropertyName = "Series")]
		public TimeSeriesConfiguration TimeSeriesConfiguration { get; set; }
		#endregion

		private const int DEFAULT_MAXIMUM_BLOCK_CANDIDATES = 50;
	}
}