#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NpgsqlTypes;
using nVeoPool.Data.Context;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Repositories {
	public class StatsRepository : IStatsRepository {
		public StatsRepository(PostgresDatabaseContext context) {
			_context = context;
		}

		#region IStatsRepository
		public async Task<IEnumerable<AggregatePeriodMinerStats>> GetStatsForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths) {
			var rawMinerStats = new List<(String WorkerId, double HashrateGhs, decimal AverageJobDifficulty, long AcceptedShareCount, long LastShareAcceptedOn, long InvalidShareCount, int WindowLengthSeconds)>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							command.CommandText = $"SELECT \"MinerWorkerId\", \"GHs\", \"AverageJobDifficulty\", \"AcceptedShareCount\", \"LastShareAcceptedOn\", \"InvalidShareCount\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" WHERE \"MinerPublicKey\" = @mPubKey; {command.CommandText}";
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									rawMinerStats.Add(((String)reader["MinerWorkerId"], (double)reader["GHs"], (decimal)reader["AverageJobDifficulty"], (long)reader["AcceptedShareCount"], (long)reader["LastShareAcceptedOn"], (long)reader["InvalidShareCount"], (int)reader["WindowLengthSeconds"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}
				}
			}

			return rawMinerStats.GroupBy(m => m.WindowLengthSeconds, m => (m.WorkerId, m.HashrateGhs, m.AverageJobDifficulty, m.AcceptedShareCount, m.LastShareAcceptedOn, m.InvalidShareCount, m.WindowLengthSeconds), (k, v) => {
				var individualWorkers = v.Select(m => new IndividualPeriodWorkerStats {
					AcceptedShareCount = m.AcceptedShareCount,
					AverageJobDifficulty = m.AverageJobDifficulty,
					Hashrate = m.HashrateGhs,
					InvalidShareCount = m.InvalidShareCount,
					LastShareAcceptedOn = m.LastShareAcceptedOn,
					WindowLengthSeconds = m.WindowLengthSeconds,
					WorkerId = m.WorkerId
				}).ToArray();

				return new AggregatePeriodMinerStats {
					AcceptedShareCount = individualWorkers.Sum(m => m.AcceptedShareCount),
					AverageJobDifficulty = individualWorkers.Any() ? individualWorkers.Select(w => w.AverageJobDifficulty * w.AcceptedShareCount).Sum() / individualWorkers.Sum(w => w.AcceptedShareCount) : 0,
					Hashrate = individualWorkers.Sum(m => m.Hashrate),
					IndividualWorkerStats = individualWorkers,
					InvalidShareCount = individualWorkers.Sum(m => m.InvalidShareCount),
					LastShareAcceptedOn = individualWorkers.Max(m => m.LastShareAcceptedOn),
					WindowLengthSeconds = individualWorkers.First().WindowLengthSeconds,
					WorkerCount = individualWorkers.Count()
				};
			}).ToList();
		}

		public async Task<IEnumerable<AggregatePeriodPoolStats>> GetStatsForPoolAsync(IEnumerable<uint> windowLengths) {
			var poolStats = new List<AggregatePeriodPoolStats>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							command.CommandText = $"SELECT COUNT(DISTINCT(\"MinerPublicKey\")) AS \"MinerCount\", COUNT(DISTINCT(\"MinerPublicKey\", \"MinerWorkerId\")) AS \"WorkerCount\", SUM(\"GHs\") AS \"GHs\", SUM(\"AcceptedShareCount\") AS \"AcceptedShareCount\", SUM(\"InvalidShareCount\") AS \"InvalidShareCount\", SUM(\"AverageJobDifficulty\" * \"AcceptedShareCount\") / SUM(\"AcceptedShareCount\") AS \"AverageJobDifficulty\", MAX(\"LastShareAcceptedOn\") AS \"LastShareAcceptedOn\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\"; {command.CommandText}";
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									poolStats.Add(new AggregatePeriodPoolStats {
										AcceptedShareCount = (reader["AcceptedShareCount"] as decimal?) ?? 0,
										AverageJobDifficulty = (reader["AverageJobDifficulty"] as decimal?) ?? 0,
										Hashrate = (reader["GHs"] as double?) ?? 0,
										InvalidShareCount = (reader["InvalidShareCount"] as decimal?) ?? 0,
										LastShareAcceptedOn = (reader["LastShareAcceptedOn"] as long?) ?? 0,
										MinerCount = (long)reader["MinerCount"],
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"],
										WorkerCount = (long)reader["WorkerCount"],
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return poolStats;
				}
			}
		}

		public async Task<IEnumerable<IndividualPeriodWorkerStats>> GetStatsForWorkerForMinerAsync(String minerPublicKey, String workerId, IEnumerable<uint> windowLengths) {
			var workerStats = new List<IndividualPeriodWorkerStats>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							command.CommandText = $"SELECT \"GHs\", \"AverageJobDifficulty\", \"AcceptedShareCount\", \"LastShareAcceptedOn\", \"InvalidShareCount\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" WHERE \"MinerPublicKey\" = @mPubKey AND \"MinerWorkerId\" = @mWorkerId; {command.CommandText}";
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);
						command.Parameters.AddWithValue("mWorkerId", NpgsqlDbType.Text, workerId);

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									workerStats.Add(new IndividualPeriodWorkerStats {
										AcceptedShareCount = (long)reader["AcceptedShareCount"],
										AverageJobDifficulty = (decimal)reader["AverageJobDifficulty"],
										Hashrate = (double)reader["GHs"],
										InvalidShareCount = (long)reader["InvalidShareCount"],
										LastShareAcceptedOn = (long)reader["LastShareAcceptedOn"],
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"]
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return workerStats;
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForMinerHashrateAsync(String minerPublicKey, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var minerHashrateSeries = new Dictionary<int, ICollection<(double Hashrate, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT SUM(\"GHs\") AS \"GHs\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesHashrate{windowLength.WindowLength}\" WHERE \"MinerPublicKey\" = @mPubKey AND \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} GROUP BY \"MinerPublicKey\", \"Timestamp\" ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							minerHashrateSeries[(int)windowLength.WindowLength] = new List<(double Hashrate, long Timestamp)>();
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);
						
						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									minerHashrateSeries[(int)reader["WindowLengthSeconds"]].Add(((double)reader["GHs"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return minerHashrateSeries.Select(kv => new TimeSeriesHashrate {
						Series = kv.Value.Select(s => new TimeSeriesHashrateDatapoint {
							Hashrate = s.Hashrate,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForMinerSharesAsync(String minerPublicKey, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var minerSharesSeries = new Dictionary<int, ICollection<(decimal AcceptedShareCount, decimal InvalidShareCount, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT SUM(\"AcceptedShareCount\") AS \"AcceptedShareCount\", SUM(\"InvalidShareCount\") AS \"InvalidShareCount\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesShares{windowLength.WindowLength}\" WHERE \"MinerPublicKey\" = @mPubKey AND \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} GROUP BY \"MinerPublicKey\", \"Timestamp\" ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							minerSharesSeries[(int)windowLength.WindowLength] = new List<(decimal AcceptedShareCount, decimal InvalidShareCount, long Timestamp)>();
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);
						
						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									minerSharesSeries[(int)reader["WindowLengthSeconds"]].Add(((decimal)reader["AcceptedShareCount"], (decimal)reader["InvalidShareCount"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return minerSharesSeries.Select(kv => new TimeSeriesShares {
						Series = kv.Value.Select(s => new TimeSeriesSharesDatapoint {
							AcceptedShareCount = s.AcceptedShareCount,
							InvalidShareCount = s.InvalidShareCount,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesWorkers>> GetTimeSeriesForMinerWorkersAsync(String minerPublicKey, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var minerWorkersSeries = new Dictionary<int, ICollection<(long Timestamp, long WorkerCount)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT \"Timestamp\", \"WorkerCount\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesWorkers{windowLength.WindowLength}\" WHERE \"MinerPublicKey\" = @mPubKey AND \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							minerWorkersSeries[(int)windowLength.WindowLength] = new List<(long Timestamp, long WorkerCount)>();
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);
						
						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									minerWorkersSeries[(int)reader["WindowLengthSeconds"]].Add(((long)reader["Timestamp"], (long)reader["WorkerCount"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return minerWorkersSeries.Select(kv => new TimeSeriesWorkers {
						Series = kv.Value.Select(s => new TimeSeriesWorkersDatapoint {
							Timestamp = s.Timestamp,
							WorkerCount = s.WorkerCount
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForPoolHashrateAsync(IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var poolHashrateSeries = new Dictionary<int, ICollection<(double Hashrate, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT \"GHs\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesPoolHashrate{windowLength.WindowLength}\" WHERE \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							poolHashrateSeries[(int)windowLength.WindowLength] = new List<(double Hashrate, long Timestamp)>();
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									poolHashrateSeries[(int)reader["WindowLengthSeconds"]].Add(((double)reader["GHs"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return poolHashrateSeries.Select(kv => new TimeSeriesHashrate {
						Series = kv.Value.Select(s => new TimeSeriesHashrateDatapoint {
							Hashrate = s.Hashrate,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForPoolSharesAsync(IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var poolSharesSeries = new Dictionary<int, ICollection<(decimal AcceptedShareCount, decimal InvalidShareCount, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT \"AcceptedShareCount\", \"InvalidShareCount\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesPoolShares{windowLength.WindowLength}\" WHERE \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							poolSharesSeries[(int)windowLength.WindowLength] = new List<(decimal AcceptedShareCount, decimal InvalidShareCount, long Timestamp)>();
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									poolSharesSeries[(int)reader["WindowLengthSeconds"]].Add(((decimal)(long)reader["AcceptedShareCount"], (decimal)(long)reader["InvalidShareCount"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return poolSharesSeries.Select(kv => new TimeSeriesShares {
						Series = kv.Value.Select(s => new TimeSeriesSharesDatapoint {
							AcceptedShareCount = s.AcceptedShareCount,
							InvalidShareCount = s.InvalidShareCount,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesPoolWorkers>> GetTimeSeriesForPoolWorkersAsync(IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var poolWorkersSeries = new Dictionary<int, ICollection<(long MinerCount, long WorkerCount, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT \"MinerCount\", \"WorkerCount\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesPoolWorkers{windowLength.WindowLength}\" WHERE \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							poolWorkersSeries[(int)windowLength.WindowLength] = new List<(long MinerCount, long WorkerCount, long Timestamp)>();
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									poolWorkersSeries[(int)reader["WindowLengthSeconds"]].Add(((long)reader["MinerCount"], (long)reader["WorkerCount"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return poolWorkersSeries.Select(kv => new TimeSeriesPoolWorkers {
						Series = kv.Value.Select(s => new TimeSeriesPoolWorkersDatapoint {
							MinerCount = s.MinerCount,
							WorkerCount = s.WorkerCount,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForWorkerHashrateAsync(String minerPublicKey, String minerWorkerId, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var workerHashrateSeries = new Dictionary<int, ICollection<(double Hashrate, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT \"GHs\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesHashrate{windowLength.WindowLength}\" WHERE \"MinerPublicKey\" = @mPubKey AND \"MinerWorkerId\" = @mWorkerId AND \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							workerHashrateSeries[(int)windowLength.WindowLength] = new List<(double Hashrate, long Timestamp)>();
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);
						command.Parameters.AddWithValue("mWorkerId", NpgsqlDbType.Text, minerWorkerId);
						
						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									workerHashrateSeries[(int)reader["WindowLengthSeconds"]].Add(((double)reader["GHs"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return workerHashrateSeries.Select(kv => new TimeSeriesHashrate {
						Series = kv.Value.Select(s => new TimeSeriesHashrateDatapoint {
							Hashrate = s.Hashrate,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForWorkerSharesAsync(String minerPublicKey, String minerWorkerId, IEnumerable<(uint WindowLength, long TimestampStart, long TimestampEnd)> windowLengthRanges) {
			var workerSharesSeries = new Dictionary<int, ICollection<(decimal AcceptedShareCount, decimal InvalidShareCount, long Timestamp)>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthRangesArray = windowLengthRanges as (uint WindowLength, long TimestampStart, long TimestampEnd)[] ?? windowLengthRanges.ToArray();

					if (windowLengthRangesArray.Length > 0) {
						foreach (var windowLength in windowLengthRangesArray) {
							command.CommandText = $"SELECT \"AcceptedShareCount\", \"InvalidShareCount\", \"Timestamp\", {windowLength.WindowLength} AS \"WindowLengthSeconds\" FROM \"SeriesShares{windowLength.WindowLength}\" WHERE \"MinerPublicKey\" = @mPubKey AND \"MinerWorkerId\" = @mWorkerId AND \"Timestamp\" >= @tStart{windowLength.WindowLength} AND \"Timestamp\" <= @tEnd{windowLength.WindowLength} ORDER BY \"Timestamp\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"tStart{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampStart);
							command.Parameters.AddWithValue($"tEnd{windowLength.WindowLength}", NpgsqlDbType.Bigint, windowLength.TimestampEnd);

							workerSharesSeries[(int)windowLength.WindowLength] = new List<(decimal AcceptedShareCount, decimal InvalidShareCount, long Timestamp)>();
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);
						command.Parameters.AddWithValue("mWorkerId", NpgsqlDbType.Text, minerWorkerId);
						
						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									workerSharesSeries[(int)reader["WindowLengthSeconds"]].Add(((decimal)(long)reader["AcceptedShareCount"], (decimal)(long)reader["InvalidShareCount"], (long)reader["Timestamp"]));
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return workerSharesSeries.Select(kv => new TimeSeriesShares {
						Series = kv.Value.Select(s => new TimeSeriesSharesDatapoint {
							AcceptedShareCount = s.AcceptedShareCount,
							InvalidShareCount = s.InvalidShareCount,
							Timestamp = s.Timestamp
						}).ToList(),
						WindowLengthSeconds = kv.Key
					}).ToList();
				}
			}
		}

		public async Task<IEnumerable<TopMiner>> GetTopMinersByAcceptedSharesAsync(IEnumerable<uint> windowLengths, int limit) {
			var topMiners = new List<TopMiner>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							var commandText = $"SELECT \"MinerPublicKey\", SUM(\"AcceptedShareCount\") AS \"AcceptedShareCount\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" GROUP BY \"MinerPublicKey\" ORDER BY \"AcceptedShareCount\" DESC";

							if (limit > 0) {
								commandText = $"{commandText} LIMIT @limit";
							}

							command.CommandText = $"{commandText}; {command.CommandText}";
						}

						if (limit > 0) {
							command.Parameters.AddWithValue("limit", NpgsqlDbType.Integer, limit);
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									topMiners.Add(new TopMiner {
										AcceptedShareCount = (decimal)reader["AcceptedShareCount"],
										MinerPublicKey = (String)reader["MinerPublicKey"],
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"]
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return topMiners;
				}
			}
		}

		public async Task<IEnumerable<TopMiner>> GetTopMinersByHashrateAsync(IEnumerable<uint> windowLengths, int limit) {
			var topMiners = new List<TopMiner>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							var commandText = $"SELECT \"MinerPublicKey\", SUM(\"GHs\") AS \"GHs\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" GROUP BY \"MinerPublicKey\" ORDER BY \"GHs\" DESC";

							if (limit > 0) {
								commandText = $"{commandText} LIMIT @limit";
							}

							command.CommandText = $"{commandText}; {command.CommandText}";
						}

						if (limit > 0) {
							command.Parameters.AddWithValue("limit", NpgsqlDbType.Integer, limit);
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									topMiners.Add(new TopMiner {
										Hashrate = (double)reader["GHs"],
										MinerPublicKey = (String)reader["MinerPublicKey"],
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"]
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return topMiners;
				}
			}
		}

		public async Task<IEnumerable<TopMiner>> GetTopMinersByInvalidSharesAsync(IEnumerable<uint> windowLengths, int limit) {
			var topMiners = new List<TopMiner>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							var commandText = $"SELECT \"MinerPublicKey\", SUM(\"InvalidShareCount\") AS \"InvalidShareCount\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" GROUP BY \"MinerPublicKey\" ORDER BY \"InvalidShareCount\" DESC";

							if (limit > 0) {
								commandText = $"{commandText} LIMIT @limit";
							}

							command.CommandText = $"{commandText}; {command.CommandText}";
						}

						if (limit > 0) {
							command.Parameters.AddWithValue("limit", NpgsqlDbType.Integer, limit);
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									topMiners.Add(new TopMiner {
										InvalidShareCount = (decimal)reader["InvalidShareCount"],
										MinerPublicKey = (String)reader["MinerPublicKey"],
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"]
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return topMiners;
				}
			}
		}

		public async Task<IEnumerable<TopMiner>> GetTopMinersByWorkerCountAsync(IEnumerable<uint> windowLengths, int limit) {
			var topMiners = new List<TopMiner>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							var commandText = $"SELECT \"MinerPublicKey\", COUNT(DISTINCT(\"MinerWorkerId\")) AS \"WorkerCount\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" GROUP BY \"MinerPublicKey\" ORDER BY \"WorkerCount\" DESC";

							if (limit > 0) {
								commandText = $"{commandText} LIMIT @limit";
							}

							command.CommandText = $"{commandText}; {command.CommandText}";
						}

						if (limit > 0) {
							command.Parameters.AddWithValue("limit", NpgsqlDbType.Integer, limit);
						}

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									topMiners.Add(new TopMiner {
										MinerPublicKey = (String)reader["MinerPublicKey"],
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"],
										WorkerCount = (long)reader["WorkerCount"]
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return topMiners;
				}
			}
		}

		public async Task<IEnumerable<IndividualPeriodWorkerStats>> GetWorkerListForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths) {
			var workerStats = new List<IndividualPeriodWorkerStats>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						foreach (var windowLength in windowLengthsArray) {
							command.CommandText = $"SELECT \"MinerWorkerId\", {windowLength} AS \"WindowLengthSeconds\" FROM \"AggregateMinerStats{windowLength}\" WHERE \"MinerPublicKey\" = @mPubKey; {command.CommandText}";
						}

						command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, minerPublicKey);

						await connection.OpenAsync();
						command.Prepare();
						
						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								while (await reader.ReadAsync()) {
									workerStats.Add(new IndividualPeriodWorkerStats {
										WindowLengthSeconds = (int)reader["WindowLengthSeconds"],
										WorkerId = reader["MinerWorkerId"] as String
									});
								}

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}

					return workerStats;
				}
			}
		}

		public async Task RefreshAggregateStatsAsync(IEnumerable<uint> windowLengths, long? cutoffSeriesTime) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var windowLengthsArray = windowLengths as uint[] ?? windowLengths.ToArray();

					if (windowLengthsArray.Length > 0) {
						for (var i = 0; i < windowLengthsArray.Length; i++) {
							var commandText = $"REFRESH MATERIALIZED VIEW CONCURRENTLY \"AggregateMinerStats{windowLengthsArray[i]}\";";
							if (cutoffSeriesTime.HasValue) {
								commandText = $"{commandText} DELETE FROM \"SeriesHashrate{windowLengthsArray[i]}\" WHERE \"Timestamp\" <= @cutoff{i}; WITH \"UpdatedSeriesHashrate{windowLengthsArray[i]}\" AS (SELECT \"GHs\", \"MinerPublicKey\", \"MinerWorkerId\", EXTRACT(epoch FROM now()) FROM \"AggregateMinerStats{windowLengthsArray[i]}\") INSERT INTO \"SeriesHashrate{windowLengthsArray[i]}\" SELECT * FROM \"UpdatedSeriesHashrate{windowLengthsArray[i]}\";";
								commandText = $"{commandText} DELETE FROM \"SeriesPoolHashrate{windowLengthsArray[i]}\" WHERE \"Timestamp\" <= @cutoff{i}; WITH \"UpdatedSeriesPoolHashrate{windowLengthsArray[i]}\" AS (SELECT COALESCE(SUM(\"GHs\"), 0), EXTRACT(epoch FROM now()) FROM \"AggregateMinerStats{windowLengthsArray[i]}\") INSERT INTO \"SeriesPoolHashrate{windowLengthsArray[i]}\" SELECT * FROM \"UpdatedSeriesPoolHashrate{windowLengthsArray[i]}\";";
								commandText = $"{commandText} DELETE FROM \"SeriesPoolShares{windowLengthsArray[i]}\" WHERE \"Timestamp\" <= @cutoff{i}; WITH \"UpdatedSeriesPoolShares{windowLengthsArray[i]}\" AS (SELECT COALESCE(SUM(\"AcceptedShareCount\"), 0), COALESCE(SUM(\"InvalidShareCount\"), 0), EXTRACT(epoch FROM now()) FROM \"AggregateMinerStats{windowLengthsArray[i]}\") INSERT INTO \"SeriesPoolShares{windowLengthsArray[i]}\" SELECT * FROM \"UpdatedSeriesPoolShares{windowLengthsArray[i]}\";";
								commandText = $"{commandText} DELETE FROM \"SeriesPoolWorkers{windowLengthsArray[i]}\" WHERE \"Timestamp\" <= @cutoff{i}; WITH \"UpdatedSeriesPoolWorkers{windowLengthsArray[i]}\" AS (SELECT COUNT(DISTINCT(\"MinerPublicKey\")), EXTRACT(epoch FROM now()), COUNT(DISTINCT(\"MinerPublicKey\", \"MinerWorkerId\")) FROM \"AggregateMinerStats{windowLengthsArray[i]}\") INSERT INTO \"SeriesPoolWorkers{windowLengthsArray[i]}\" SELECT * FROM \"UpdatedSeriesPoolWorkers{windowLengthsArray[i]}\";";
								commandText = $"{commandText} DELETE FROM \"SeriesShares{windowLengthsArray[i]}\" WHERE \"Timestamp\" <= @cutoff{i}; WITH \"UpdatedSeriesShares{windowLengthsArray[i]}\" AS (SELECT \"AcceptedShareCount\", \"InvalidShareCount\", \"MinerPublicKey\", \"MinerWorkerId\", EXTRACT(epoch FROM now()) FROM \"AggregateMinerStats{windowLengthsArray[i]}\") INSERT INTO \"SeriesShares{windowLengthsArray[i]}\" SELECT * FROM \"UpdatedSeriesShares{windowLengthsArray[i]}\";";
								commandText = $"{commandText} DELETE FROM \"SeriesWorkers{windowLengthsArray[i]}\" WHERE \"Timestamp\" <= @cutoff{i}; WITH \"UpdatedSeriesWorkers{windowLengthsArray[i]}\" AS (SELECT \"MinerPublicKey\", EXTRACT(epoch FROM now()), COUNT(*) FROM \"AggregateMinerStats{windowLengthsArray[i]}\" GROUP BY \"MinerPublicKey\") INSERT INTO \"SeriesWorkers{windowLengthsArray[i]}\" SELECT * FROM \"UpdatedSeriesWorkers{windowLengthsArray[i]}\";";

								command.Parameters.AddWithValue($"cutoff{i}", NpgsqlDbType.Bigint, cutoffSeriesTime.Value);
							}

							command.CommandText = $"{commandText}; {command.CommandText}";
						}

						await connection.OpenAsync();
						command.Prepare();

						await command.ExecuteNonQueryAsync();
					}
				}
			}
		}
		#endregion

		private readonly PostgresDatabaseContext _context;
	}
}