#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Repositories {
	public interface IPaymentRepository {
		Task<IEnumerable<MinerPayment>> GetAllPaymentsForMinerAsync(String minerPublicKey, int limit);

		Task<MinerBalance> GetBalanceForMinerAsync(String minerPublicKey);
		
		Task<IEnumerable<(String MinerPublicKey, decimal PendingBalance)>> GetMinersForPaymentAsync(decimal minimumBalance);

		Task<IEnumerable<MinerPayment>> GetUnconfirmedPaymentsAsync(int maximumBlockHeight);

		Task<int> SaveRewardsForBlockAsync(int blockHeight, IEnumerable<(decimal FeePercent, bool IsFeeRecipient, String MinerPublicKey, decimal Reward)> blockRewards);

		Task<int> SaveTransactionsAndUpdateMinerBalancesAsync(IEnumerable<(String MinerPublicKey, decimal Amount, long SubmittedOn, int SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)> minerPaymentTransactions);

		Task<int> UpdateTransactionStatusesAsync(IEnumerable<String> confirmedTransactions, IEnumerable<MinerPayment> transactionsForResend);
	}
}