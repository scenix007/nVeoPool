#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Newtonsoft.Json;

namespace nVeoPool.Data.Models {
	[Serializable]
	public class TopMiner {
		[JsonProperty(PropertyName = "Shares")]
		public decimal? AcceptedShareCount { get; set; }

		[JsonProperty(PropertyName = "PublicKey")]
		public String MinerPublicKey { get; set; }

		public double? Hashrate { get; set; }

		[JsonProperty(PropertyName = "InvalidShares")]
		public decimal? InvalidShareCount { get; set; }

		[JsonIgnore]
		public int WindowLengthSeconds { get; set; }

		[JsonProperty(PropertyName = "Workers")]
		public long? WorkerCount { get; set; }
	}
}