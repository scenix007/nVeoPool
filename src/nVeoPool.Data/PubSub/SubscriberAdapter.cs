#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading.Tasks;
using StackExchange.Redis;
using nVeoPool.Data.Extensions;

namespace nVeoPool.Data.PubSub {
	public class SubscriberAdapter : ISubscriberAdapter {
		public SubscriberAdapter(ISubscriber subscriber) {
			_subscriber = subscriber;
		}

		#region ISubscriberAdapter
		public void PublishImmediate<T>(String channelName, T message) {
			_subscriber.Publish(channelName, message.ToByteArray(), CommandFlags.FireAndForget);
		}

		public Task<long> PublishSimpleAsync(String channelName, long message) {
			return _subscriber.PublishAsync(channelName, message);
		}

		public void SubscribeImmediate<T>(String channelName, Action<String, T> messageHandler) {
			_subscriber.Subscribe(channelName, (channel, message) => {
				messageHandler(channel, (T)((byte[])message).ToObject());
			}, CommandFlags.FireAndForget);
		}

		public void SubscribeSimpleImmediate(String channelName, Action<String, long> messageHandler) {
			_subscriber.Subscribe(channelName, (channel, message) => {
				messageHandler(channel, (long)message);
			}, CommandFlags.FireAndForget);
		}
		#endregion

		private readonly ISubscriber _subscriber;
	}
}