#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StackExchange.Redis;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Configuration;
using nVeoPool.Data.Extensions;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public class DistributedCacheAdapter : IDistributedCacheAdapter {
		public DistributedCacheAdapter(ICacheConnectionFactory cacheConnectionFactory, IDistributedCacheKeyConfiguration distributedCacheKeyConfiguration) {
			_cacheConnectionFactory = cacheConnectionFactory;
			_distributedCacheKeyConfiguration = distributedCacheKeyConfiguration;
		}

		#region IDistributedCacheAdapter
		public void ClearWorkerCurrentJobDifficultyImmediate(String minerAddress, int port) {
			_cacheConnectionFactory.GetDatabase().HashDelete(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "currentDiff", CommandFlags.FireAndForget);
		}

		public async Task<bool> ClearWorkerInfoAsync(String workerAddress, int port) {
			await ClearWorkerRecentShareIntervalsAsync(workerAddress, port);
			await _cacheConnectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(workerAddress, port));
			await _cacheConnectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.ActiveWorkersKey(port), workerAddress);

			return true;
		}
		
		public async Task<bool> ClearWorkerRecentShareIntervalsAsync(String workerAddress, int port, long maximumListLength = 0) {
			if (maximumListLength < 1) {
				return _cacheConnectionFactory.GetDatabase().KeyDelete(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(workerAddress, port), CommandFlags.FireAndForget);
			} else {
				var length = await _cacheConnectionFactory.GetDatabase().ListLengthAsync(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(workerAddress, port));

				if (length > (maximumListLength * 2)) {
					await _cacheConnectionFactory.GetDatabase().ListTrimAsync(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(workerAddress, port), -maximumListLength, -1);
				}
			}

			return true;
		}

		public bool DeleteNodeMiningDataImmediate() {
			return _cacheConnectionFactory.GetDatabase().KeyDelete(_distributedCacheKeyConfiguration.NodeMiningDataKey, CommandFlags.FireAndForget);
		}

		public async Task<IEnumerable<String>> GetActiveWorkersAsync(int port) {
			return (await _cacheConnectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.ActiveWorkersKey(port))).ToStringArray();
		}

		public async Task<(String BlockHash, int BlockDifficulty, int CurrentHeight, int FieldZero, ulong NodeClientId)> GetNodeMiningDataAsync() {
			var nodeMiningData = await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.NodeMiningDataKey, new RedisValue[] { "blockHash", "blockDiff", "height", "f0", "nodeClient" });

			return (nodeMiningData[0], (int)nodeMiningData[1], (int)nodeMiningData[2], (int)nodeMiningData[3], (ulong)nodeMiningData[4]);
		}

		public async Task<IEnumerable<String>> GetRegisteredServersAsync(ClusterServerCategory serverCategory) {
			return (await _cacheConnectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.RegisteredServersKey(serverCategory.ToString()))).ToStringArray();
		}

		public async Task<IEnumerable<String>> GetRegisteredServicesForServerAsync(String serverId) {
			return (await _cacheConnectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId))).ToStringArray();
		}

		public async Task<String> GetServerConfigurationSectionAsync(String serverId, String configurationSection) {
			return await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), configurationSection);
		}

		public async Task<IEnumerable<String>> GetServerConfigurationSectionsAsync(String serverId) {
			return (await _cacheConnectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.ServerConfigSections(serverId))).ToStringArray();
		}

		public async Task<ServerStatusInfo> GetServerStatusAsync(String serverId) {
			var serverStatusData = await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), new RedisValue[] { "category", "heartbeat", "registeredOn", "type", "version" });

			return !serverStatusData[3].IsNull ? new ServerStatusInfo {
				Heartbeat = (long?)serverStatusData[1],
				RegisteredOn = (long?)serverStatusData[2],
				ServerCategory = (ClusterServerCategory)(int)serverStatusData[0],
				ServerType = (ClusterServerType)(int)serverStatusData[3],
				ServerVersion = serverStatusData[4]
			 } : null;
		}

		public async Task<ServiceStatusInfo> GetServiceStatusAsync(String serverId, String serviceName) {
			var serviceStatusData = await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new RedisValue[] { "currentStartedOn", "lastCompletedOn", "nextRunOn", "status", "data" });

			return !serviceStatusData[3].IsNull ? new ServiceStatusInfo { CurrentRunStartedOn = (long?)serviceStatusData[0], Data = ((byte[])serviceStatusData[4]).ToObject(), EstimatedNextRunOn = (long?)serviceStatusData[2], LastCompletedOn = (long)serviceStatusData[1], RuntimeStatus = (ServiceRuntimeStatusType)(int)serviceStatusData[3] } : null;
		}

		public async Task<int?> GetWorkerCurrentJobDifficultyAsync(String minerAddress, int port) {
			return (int?)(await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "currentDiff"));
		}

		public async Task<long?> GetWorkerLastSeenTimeAsync(String minerAddress, int port) {
			return (long?)(await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "heartbeat"));
		}

		public async Task<long?> GetWorkerLastShareTimeAsync(String minerAddress, int port) {
			return (long?)(await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "lastShareOn"));
		}

		public async Task<IEnumerable<int>> GetWorkerRecentShareIntervalsAsync(String minerAddress, int port) {
			return (await _cacheConnectionFactory.GetDatabase().ListRangeAsync(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(minerAddress, port))).ToIntArray();
		}

		public Task<bool> MinerIsWhitelistedAsync(String minerPublicKey) {
			return _cacheConnectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKey, minerPublicKey);
		}

		public async Task<bool> MinerShareIsDuplicateAsync(String minerPublicKey, String nonce) {
			return await _cacheConnectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey), nonce) || await _cacheConnectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.MinerRecentSharesOldKey(minerPublicKey), nonce);
		}

		public async Task<bool> RegisterServerAsync(ClusterServerCategory serverCategory, String serverId, ClusterServerType serverType, Version serverVersion, IEnumerable<(String ConfigurationSection, String ConfigurationValue)> serverConfiguration) {
			var serverStatusEntries = new List<HashEntry> { new HashEntry("category", (int)serverCategory), new HashEntry("heartbeat", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()), new HashEntry("registeredOn", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()), new HashEntry("type", (int)serverType), new HashEntry("version", serverVersion.ToString()) };
			serverStatusEntries.AddRange(serverConfiguration.Select(c => new HashEntry(c.ConfigurationSection, c.ConfigurationValue)));
			var configurationElements = serverConfiguration.Select(c => (RedisValue)c.ConfigurationSection).ToArray();

			await _cacheConnectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.RegisteredServersKey(serverCategory.ToString()), serverId);
			await _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), serverStatusEntries.ToArray());
			await _cacheConnectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.ServerConfigSections(serverId), configurationElements);

			return true;
		}

		public async Task<bool> RegisterServiceAsync(String serverId, String serviceName) {
			return await _cacheConnectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnCacheKey, serviceName) && await _cacheConnectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId), serviceName);
		}

		public async Task RemoveMinerRecentSharesAsync(String minerPublicKey, bool preserveNewEntries) {
			await _cacheConnectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.MinerRecentSharesOldKey(minerPublicKey));
			if (preserveNewEntries) {
				await _cacheConnectionFactory.GetDatabase().SetCombineAndStoreAsync(SetOperation.Union, _distributedCacheKeyConfiguration.MinerRecentSharesOldKey(minerPublicKey), _distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey), _distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey));
			}
			await _cacheConnectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey));
		}

		public Task<bool> ServiceIsRegisteredOnCacheAsync(String serviceName) {
			return _cacheConnectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnCacheKey, serviceName);
		}

		public async Task<bool> UnregisterServerAsync(ClusterServerCategory serverCategory, String serverId) {
			await _cacheConnectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.RegisteredServersKey(serverCategory.ToString()), serverId);
			await _cacheConnectionFactory.GetDatabase().KeyDeleteAsync(new [] { (RedisKey)_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId), (RedisKey)_distributedCacheKeyConfiguration.ServerConfigSections(serverId), (RedisKey)_distributedCacheKeyConfiguration.ServerStatusKey(serverId) });

			return true;
		}

		public async Task<bool> UnregisterServiceAsync(String serverId, String serviceName, bool preserveGenericEntry) {
			if (!preserveGenericEntry) {
				await _cacheConnectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnCacheKey, serviceName);
			}
			await _cacheConnectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId), serviceName);

			return true;
		}

		public void UpdateNodeMiningDataImmediate(String blockHash, int blockDifficulty, int currentHeight, int fieldZero, ulong nodeClientId) {
			_cacheConnectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.NodeMiningDataKey, new [] { new HashEntry("blockHash", blockHash), new HashEntry("blockDiff", blockDifficulty), new HashEntry("height", currentHeight), new HashEntry("f0", fieldZero), new HashEntry("nodeClient", nodeClientId) }, CommandFlags.FireAndForget);
		}

		public async Task<long> UpdatePublicKeyWhitelistAsync(IEnumerable<String> minerPublicKeys) {
			var minerPublicKeysArray = (minerPublicKeys as String[] ?? minerPublicKeys.ToArray()).Select(m => (RedisValue)m).ToArray();

			if (minerPublicKeysArray.Length > 0) {
				await _cacheConnectionFactory.GetDatabase().KeyDeleteAsync(new [] { (RedisKey)_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempEmpty, (RedisKey)_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempNew });
				
				await _cacheConnectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempEmpty, new [] { minerPublicKeysArray.First() });
				await _cacheConnectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempNew, minerPublicKeysArray);

				return await _cacheConnectionFactory.GetDatabase().SetCombineAndStoreAsync(SetOperation.Union, _distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKey, _distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempEmpty, _distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempNew);
			} else {
				await _cacheConnectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKey);

				return 0L;
			}
		}

		public Task<bool> UpdateServerHeartbeatAsync(String serverId) {
			return _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), "heartbeat", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());
		}

		public async Task<bool> UpdateServiceStatusAsRunningAsync(String serverId, String serviceName, long currentRunStartedOn) {
			await _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new [] { new HashEntry("status", (int)ServiceRuntimeStatusType.Running), new HashEntry("currentStartedOn", currentRunStartedOn) });
			await _cacheConnectionFactory.GetDatabase().HashDeleteAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), "nextRunOn");

			return true;
		}

		public async Task<bool> UpdateServiceStatusAsSleepingAsync(String serverId, String serviceName, long estimatedNextRunOn, long lastCompletedOn, bool eraseRuntimeData) {
			await _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new [] { new HashEntry("lastCompletedOn", lastCompletedOn), new HashEntry("nextRunOn", estimatedNextRunOn), new HashEntry("status", (int)ServiceRuntimeStatusType.Sleeping) });
			var keysForDeletion = eraseRuntimeData ? new RedisValue[] { "currentStartedOn", "data" } : new RedisValue[] { "currentStartedOn" };
			await _cacheConnectionFactory.GetDatabase().HashDeleteAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), keysForDeletion);

			return true;
		}

		public async Task<bool> UpdateServiceStatusAsStoppedAsync(String serverId, String serviceName) {
			await _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), "status", (int)ServiceRuntimeStatusType.Stopped);
			await _cacheConnectionFactory.GetDatabase().HashDeleteAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new RedisValue[] { "currentStartedOn", "data", "nextRunOn" });

			return true;
		}

		public Task<bool> UpdateServiceStatusDataAsync(String serverId, String serviceName, Object data) {
			return _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), "data", data.ToByteArray());
		}

		public void UpdateWorkerCurrentJobDifficultyImmediate(String minerAddress, int port, int difficulty) {
			_cacheConnectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "currentDiff", difficulty, flags: CommandFlags.FireAndForget);
		}
		
		public void UpdateWorkerHeartbeatImmediate(String minerAddress, int port) {
			_cacheConnectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "heartbeat", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(), flags: CommandFlags.FireAndForget);

			_cacheConnectionFactory.GetDatabase().SetAdd(_distributedCacheKeyConfiguration.ActiveWorkersKey(port), minerAddress, CommandFlags.FireAndForget);
		}

		public async Task UpdateWorkerRecentSharesAsync(String minerAddress, String minerPublicKey, int port, String nonce, long shareSubmittedOn) {
			_cacheConnectionFactory.GetDatabase().SetAdd(_distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey), nonce, CommandFlags.FireAndForget);

			await UpdateWorkerRecentShareIntervalsAsync(minerAddress, port, shareSubmittedOn);
		}
		#endregion

		private async Task UpdateWorkerRecentShareIntervalsAsync(String minerAddress, int port, long shareSubmittedOn) {
			var lastShareTimeTask = GetWorkerLastShareTimeAsync(minerAddress, port);

			_cacheConnectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress, port), "lastShareOn", shareSubmittedOn, flags: CommandFlags.FireAndForget);

			var lastShareTime = await lastShareTimeTask;
			if (lastShareTime.HasValue) {
				_cacheConnectionFactory.GetDatabase().ListRightPush(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(minerAddress, port), Math.Abs(shareSubmittedOn - lastShareTime.Value), flags: CommandFlags.FireAndForget);
			}
		}

		private readonly ICacheConnectionFactory _cacheConnectionFactory;
		private readonly IDistributedCacheKeyConfiguration _distributedCacheKeyConfiguration;
	}
}