#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public interface IRepositoryCacheAdapter {
		Task<IEnumerable<BlockCandidate>> GetAllBlockCandidatesAsync(int limit);

		Task<IEnumerable<MinerPayment>> GetAllPaymentsForMinerAsync(String minerPublicKey, int limit);

		Task<MinerBalance> GetBalanceForMinerAsync(String minerPublicKey);

		Task<IEnumerable<AggregatePeriodMinerStats>> GetStatsForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths);

		Task<IEnumerable<AggregatePeriodPoolStats>> GetStatsForPoolAsync(IEnumerable<uint> windowLengths);

		Task<IEnumerable<IndividualPeriodWorkerStats>> GetStatsForWorkerForMinerAsync(String minerPublicKey, String minerWorkerId, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForMinerHashrateAsync(String minerPublicKey, String minerWorkerId, long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForMinerSharesAsync(String minerPublicKey, String minerWorkerId, long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesWorkers>> GetTimeSeriesForMinerWorkersAsync(String minerPublicKey, long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForPoolHashrateAsync(long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForPoolSharesAsync(long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TimeSeriesPoolWorkers>> GetTimeSeriesForPoolWorkersAsync(long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths);

		Task<IEnumerable<TopMiner>> GetTopMinersByFieldAsync(String orderBy, IEnumerable<uint> windowLengths, int limit);

		Task<IEnumerable<IndividualPeriodWorkerStats>> GetWorkerListForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths);
	}
}