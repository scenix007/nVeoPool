#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public interface IMultiCacheAdapter {
		Task<IEnumerable<IEnumerable<String>>> GetRegisteredServersAsync(ClusterServerCategory serverCategory);

		Task<IEnumerable<IEnumerable<String>>> GetRegisteredServicesForServerAsync(String serverId);

		Task<IEnumerable<String>> GetServerConfigurationSectionAsync(String serverId, String configurationSection);

		Task<IEnumerable<IEnumerable<String>>> GetServerConfigurationSectionsAsync(String serverId);

		Task<IEnumerable<ServerStatusInfo>> GetServerStatusAsync(String serverId);

		Task<IEnumerable<ServiceStatusInfo>> GetServiceStatusAsync(String serverId, String serviceName);
	}
}