#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using nVeoPool.Common.Configuration;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public class MultiCacheAdapter : IMultiCacheAdapter {
		public MultiCacheAdapter(IDistributedCacheAdapter primaryCache, IDistributedCacheAdapterFactory distributedCacheAdapterFactory, IServerConfiguration serverConfiguration) {
			_cacheAdapters = new List<IDistributedCacheAdapter> { primaryCache };
			_cacheAdapters.AddRange(serverConfiguration.CacheConfiguration.BackupCacheConfiguration.Select(c => {
				try {
					return distributedCacheAdapterFactory.GetDistributedCacheAdapter(c.ConnectionString);
				} catch {
					return null;
				}
			}).Where(c => c != null));
		}

		#region IMultiCacheAdapter
		public Task<IEnumerable<IEnumerable<String>>> GetRegisteredServersAsync(ClusterServerCategory serverCategory) {
			return ExecuteMultiCallAsync(c => c.GetRegisteredServersAsync(serverCategory));
		}

		public Task<IEnumerable<IEnumerable<String>>> GetRegisteredServicesForServerAsync(String serverId) {
			return ExecuteMultiCallAsync(c => c.GetRegisteredServicesForServerAsync(serverId));
		}

		public Task<IEnumerable<String>> GetServerConfigurationSectionAsync(String serverId, String configurationSection) {
			return ExecuteMultiCallAsync(c => c.GetServerConfigurationSectionAsync(serverId, configurationSection));
		}

		public Task<IEnumerable<IEnumerable<String>>> GetServerConfigurationSectionsAsync(String serverId) {
			return ExecuteMultiCallAsync(c => c.GetServerConfigurationSectionsAsync(serverId));
		}

		public Task<IEnumerable<ServerStatusInfo>> GetServerStatusAsync(String serverId) {
			return ExecuteMultiCallAsync(c => c.GetServerStatusAsync(serverId));
		}

		public Task<IEnumerable<ServiceStatusInfo>> GetServiceStatusAsync(String serverId, String serviceName) {
			return ExecuteMultiCallAsync(c => c.GetServiceStatusAsync(serverId, serviceName));
		}
		#endregion

		private async Task<IEnumerable<T>> ExecuteMultiCallAsync<T>(Func<IDistributedCacheAdapter, Task<T>> objectRetriever) {
			return await Task.WhenAll(_cacheAdapters.Select(objectRetriever));
		}

		private readonly List<IDistributedCacheAdapter> _cacheAdapters;
	}
}