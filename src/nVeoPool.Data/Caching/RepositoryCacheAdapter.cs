#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using nVeoPool.Common.Configuration;
using nVeoPool.Data.Configuration;
using nVeoPool.Data.Extensions;
using nVeoPool.Data.Models;
using nVeoPool.Data.Repositories;

namespace nVeoPool.Data.Caching {
	public class RepositoryCacheAdapter : IRepositoryCacheAdapter {
		public RepositoryCacheAdapter(IBlockRepository blockRepository, ICacheConnectionFactory cacheConnectionFactory, IDistributedCacheKeyConfiguration distributedCacheKeyConfiguration, IReaderWriterLock globalStatisticsLock, ILogger<RepositoryCacheAdapter> logger, IPaymentRepository paymentRepository, IServerConfiguration serverConfiguration, IStatsRepository statsRepository) {
			_blockRepository = blockRepository;
			_cacheConnectionFactory = cacheConnectionFactory;
			_distributedCacheKeyConfiguration = distributedCacheKeyConfiguration;
			_globalStatisticsLock = globalStatisticsLock;
			_logger = logger;
			_paymentRepository = paymentRepository;
			_serverConfiguration = serverConfiguration;
			_statsRepository = statsRepository;
		}

		#region IRepositoryCacheAdapter
		public Task<IEnumerable<BlockCandidate>> GetAllBlockCandidatesAsync(int limit) {
			return RetrieveAndUpdateCacheAsync($"{nameof(GetAllBlockCandidatesAsync)}_{limit}", () => _blockRepository.GetAllBlockCandidatesAsync(limit), _serverConfiguration.CacheConfiguration.ExpirationConfiguration.AllBlockCandidatesAgeSeconds);
		}

		public Task<IEnumerable<MinerPayment>> GetAllPaymentsForMinerAsync(String minerPublicKey, int limit) {
			return RetrieveAndUpdateCacheAsync($"{nameof(GetAllPaymentsForMinerAsync)}_{minerPublicKey}_{limit}", () => _paymentRepository.GetAllPaymentsForMinerAsync(minerPublicKey, limit), _serverConfiguration.CacheConfiguration.ExpirationConfiguration.AllPaymentsForMinerAgeSeconds);
		}

		public Task<MinerBalance> GetBalanceForMinerAsync(String minerPublicKey) {
			return RetrieveAndUpdateCacheAsync($"{nameof(GetBalanceForMinerAsync)}_{minerPublicKey}", () => _paymentRepository.GetBalanceForMinerAsync(minerPublicKey), _serverConfiguration.CacheConfiguration.ExpirationConfiguration.BalanceForMinerAgeSeconds);
		}

		public Task<IEnumerable<AggregatePeriodMinerStats>> GetStatsForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetStatsForMinerAsync)}_{minerPublicKey}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetStatsForMinerAsync(minerPublicKey, new [] { w }), windowLengths);
		}

		public Task<IEnumerable<AggregatePeriodPoolStats>> GetStatsForPoolAsync(IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetStatsForPoolAsync)}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetStatsForPoolAsync(new [] { w }), windowLengths);
		}

		public Task<IEnumerable<IndividualPeriodWorkerStats>> GetStatsForWorkerForMinerAsync(String minerPublicKey, String minerWorkerId, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetStatsForWorkerForMinerAsync)}_{minerPublicKey}_{minerWorkerId}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetStatsForWorkerForMinerAsync(minerPublicKey, minerWorkerId, new [] { w }), windowLengths);
		}

		public Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForMinerHashrateAsync(String minerPublicKey, String minerWorkerId, long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths) {
			if (String.IsNullOrWhiteSpace(minerWorkerId)) {
				return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetTimeSeriesForMinerHashrateAsync)}_{minerPublicKey}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForMinerHashrateAsync(minerPublicKey, new [] { (w, timestampStart, timestampEnd) }), windowLengths);
			} else {
				return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(_statsRepository.GetTimeSeriesForWorkerHashrateAsync)}_{minerPublicKey}_{minerWorkerId}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForWorkerHashrateAsync(minerPublicKey, minerWorkerId, new [] { (w, timestampStart, timestampEnd) }), windowLengths);
			}
		}

		public Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForMinerSharesAsync(String minerPublicKey, String minerWorkerId, long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths) {
			if (String.IsNullOrWhiteSpace(minerWorkerId)) {
				return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetTimeSeriesForMinerSharesAsync)}_{minerPublicKey}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForMinerSharesAsync(minerPublicKey, new [] { (w, timestampStart, timestampEnd) }), windowLengths);
			} else {
				return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(_statsRepository.GetTimeSeriesForWorkerSharesAsync)}_{minerPublicKey}_{minerWorkerId}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForWorkerSharesAsync(minerPublicKey, minerWorkerId, new [] { (w, timestampStart, timestampEnd) }), windowLengths);
			}
		}

		public Task<IEnumerable<TimeSeriesWorkers>> GetTimeSeriesForMinerWorkersAsync(String minerPublicKey, long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetTimeSeriesForMinerWorkersAsync)}_{minerPublicKey}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetTimeSeriesForMinerWorkersAsync(minerPublicKey, new [] { (w, timestampStart, timestampEnd) }), windowLengths);
		}

		public Task<IEnumerable<TimeSeriesHashrate>> GetTimeSeriesForPoolHashrateAsync(long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetTimeSeriesForPoolHashrateAsync)}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForPoolHashrateAsync(new [] { (w, timestampStart, timestampEnd) }), windowLengths);
		}

		public Task<IEnumerable<TimeSeriesShares>> GetTimeSeriesForPoolSharesAsync(long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetTimeSeriesForPoolSharesAsync)}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForPoolSharesAsync(new [] { (w, timestampStart, timestampEnd) }), windowLengths);
		}

		public Task<IEnumerable<TimeSeriesPoolWorkers>> GetTimeSeriesForPoolWorkersAsync(long timestampStart, long timestampEnd, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetTimeSeriesForPoolWorkersAsync)}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.TimeSeriesAgeSeconds, w => _statsRepository.GetTimeSeriesForPoolWorkersAsync(new [] { (w, timestampStart, timestampEnd) }), windowLengths);
		}

		public Task<IEnumerable<TopMiner>> GetTopMinersByFieldAsync(String orderBy, IEnumerable<uint> windowLengths, int limit) {
			switch (orderBy) {
				case "hashrate":
					return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(_statsRepository.GetTopMinersByHashrateAsync)}_{w}_{limit}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetTopMinersByHashrateAsync(new [] { w }, limit), windowLengths);

				case "invalidshares":
					return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(_statsRepository.GetTopMinersByInvalidSharesAsync)}_{w}_{limit}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetTopMinersByInvalidSharesAsync(new [] { w }, limit), windowLengths);

				case "shares":
					return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(_statsRepository.GetTopMinersByAcceptedSharesAsync)}_{w}_{limit}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetTopMinersByAcceptedSharesAsync(new [] { w }, limit), windowLengths);

				case "workers":
					return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(_statsRepository.GetTopMinersByWorkerCountAsync)}_{w}_{limit}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetTopMinersByWorkerCountAsync(new [] { w }, limit), windowLengths);
				
				default:
					throw new ArgumentOutOfRangeException(nameof(orderBy), orderBy, $"Unsupported field for ordering '{orderBy}'");
			}
		}

		public Task<IEnumerable<IndividualPeriodWorkerStats>> GetWorkerListForMinerAsync(String minerPublicKey, IEnumerable<uint> windowLengths) {
			return LockRetrieveAndUpdateCacheAsync(w => $"{nameof(GetWorkerListForMinerAsync)}_{minerPublicKey}_{w}", _serverConfiguration.CacheConfiguration.ExpirationConfiguration.StatsAgeSeconds, w => _statsRepository.GetWorkerListForMinerAsync(minerPublicKey, new [] { w }), windowLengths);
		}
		#endregion

		private async Task<IEnumerable<T>> LockRetrieveAndUpdateCacheAsync<T>(Func<uint, String> cacheKeyFormatter, int maximumCacheAgeSeconds, Func<uint, Task<IEnumerable<T>>> objectRetrieverGenerator, IEnumerable<uint> windowLengths) {
			if (await _globalStatisticsLock.TryEnterReadLockAsync(0)) {
				try {
					return (await Task.WhenAll(windowLengths.Select(w => RetrieveAndUpdateCacheAsync(cacheKeyFormatter(w), () => objectRetrieverGenerator(w), maximumCacheAgeSeconds)))).SelectMany(w => w);
				} finally {
					_globalStatisticsLock.ExitReadLock();
				}
			}

			_logger.LogWarning("Unable to acquire read lock for updating cache key grouping {CacheKey}", cacheKeyFormatter(0));
			
			return (await Task.WhenAll(windowLengths.Select(w => RetrieveAndUpdateCacheAsync(cacheKeyFormatter(w), () => objectRetrieverGenerator(w), maximumCacheAgeSeconds, true)))).SelectMany(w => w);
		}

		private async Task<T> RetrieveAndUpdateCacheAsync<T>(String key, Func<Task<T>> objectRetriever, int maximumCacheAgeSeconds, bool trySkipDatabase = false) where T : class {
			var cachedObjectData = await _cacheConnectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.GenericObjectKey(key), new RedisValue[] { "timestamp", "value" });
			(T Instance, long? CachedOn) cachedObject = ((T)((byte[])cachedObjectData[1]).ToObject(), (long?)cachedObjectData[0]);

			var cachedOn = cachedObject.CachedOn ?? 0;
			var currentTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

			if ((cachedOn >= (currentTime - maximumCacheAgeSeconds)) || (trySkipDatabase && cachedOn > 0)) {
				return cachedObject.Instance;
			}

			try {
				var returnInstance = await objectRetriever();
				if (returnInstance != null) {
					await _cacheConnectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.GenericObjectKey(key), new [] { new HashEntry("timestamp", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()), new HashEntry("value", returnInstance.ToByteArray()) });
				}

				return returnInstance;
			} catch (Exception e) {
				if (cachedOn >= (currentTime - _serverConfiguration.CacheConfiguration.ExpirationConfiguration.MaximumStaleAgeSeconds)) {
					_logger.LogWarning(e, "Exception in {Type} while attempting to retrieve new repository data, returning stale data for cache key {CacheKey}", nameof(RepositoryCacheAdapter), key);

					return cachedObject.Instance;
				}

				_logger.LogError(e, "Exception in {Type} while attempting to retrieve repository data for cache key {CacheKey}, stale data unavailable", nameof(RepositoryCacheAdapter), key);
			}

			return null;
		}

		private readonly IBlockRepository _blockRepository;
		private readonly ICacheConnectionFactory _cacheConnectionFactory;
		private readonly IDistributedCacheKeyConfiguration _distributedCacheKeyConfiguration;
		private readonly IReaderWriterLock _globalStatisticsLock;
		private readonly ILogger<RepositoryCacheAdapter> _logger;
		private readonly IPaymentRepository _paymentRepository;
		private readonly IServerConfiguration _serverConfiguration;
		private readonly IStatsRepository _statsRepository;
	}
}