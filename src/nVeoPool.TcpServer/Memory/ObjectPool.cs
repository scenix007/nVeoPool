#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Concurrent;

namespace nVeoPool.TcpServer.Memory {
	public class ObjectPool<T> : IObjectPool<T> {
		public const int DefaultInitialSize = 1000;

		public ObjectPool(Func<T> factory) : this(factory, DefaultInitialSize) {
		}

		public ObjectPool(Func<T> factory, int initialPoolSize) {
			_factory = factory;
			_initialPoolSize = initialPoolSize;
			_pool = new ConcurrentStack<T>();

			Initialize();
		}

		protected virtual void Initialize() {
			for (var i = 0; i < _initialPoolSize; i++) {
				_pool.Push(_factory());
			}
		}

		#region IObjectPool
		public T Acquire() {
			return _pool.TryPop(out var instance) ? instance : _factory();
		}

		public void Release(T obj) {
			_pool.Push(obj);
		}
		#endregion

		public void Dispose() {
			while (_pool.Count > 0) {
				if (_pool.TryPop(out var instance) && instance is IDisposable disposableObj) {
					disposableObj.Dispose();
				}
			}
		}

		private readonly Func<T> _factory;
		private readonly int _initialPoolSize;
		private readonly ConcurrentStack<T> _pool;
	}
}