#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.TcpServer.Events;
using nVeoPool.TcpServer.Memory;

namespace nVeoPool.TcpServer {
	public abstract class TcpServerBase {
		protected TcpServerBase(int backlogSize, int bufferSize, int defaultBufferCount, ILogger logger, IScheduler scheduler) {
			_backlogSize = backlogSize;
			_clients = new Dictionary<ulong, TcpClientBase>();
			_disposed = false;
			_events = new Queue<IServerEvent>();
			_logger = logger;
			_nextClientId = 1;
			_receiveEventPool = new CheckableObjectPool<SocketEvent>(new SocketEventPool(new BufferPool(defaultBufferCount, bufferSize), OnIOCompleted));
			_scheduler = scheduler;
			_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) { NoDelay = true };
			EventStream = Observable.Create<IServerEvent>(observable => {
				return scheduler.Schedule(async recurse => {
					try {
						while (_events.Count > 0) {
							observable.OnNext(_events.Dequeue());
						}

						await Task.Delay(1);

						recurse();
					} catch (ObjectDisposedException) {
					} catch (Exception e) {
						observable.OnError(e);
					}
				});
			}).SubscribeOn(scheduler).Publish();

			EventStream.Connect();
		}

		public IConnectableObservable<IServerEvent> EventStream { get; }

		public TcpClientBase GetClient(ulong clientId) => _clients.ContainsKey(clientId) ? _clients[clientId] : null;

		public void Send(TcpClientBase client, byte[] data) {
			var sendEvent = new SocketAsyncEventArgs();
			sendEvent.Completed += OnIOCompleted;
			sendEvent.SetBuffer(data, 0, data.Length);

			try {
				if (client.Socket.Connected && !client.Socket.SendAsync(sendEvent)) {
					OnSent(sendEvent);
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception in {ServerType} while sending to client", nameof(TcpServerBase));
			}
		}

		public void Send(ulong clientId, byte[] data) {
			Schedule(() => {
				var client = GetClient(clientId);

				if (client != null) {
					Send(client, data);
				}
			});
		}

		public void Start(IPEndPoint ip) {
			_socket.Bind(ip);
			_socket.Listen(_backlogSize);

			StartAccept(null);
		}

		protected ICollection<TcpClientBase> Clients => _clients.Values;

		protected abstract TcpClientBase CreateClient(ulong clientId, Socket socket);

		protected virtual void Dispose(bool disposing) {
			if (disposing) {
				_disposed = true;

				if (_scheduler is IDisposable disposabelScheduler) {
					disposabelScheduler.Dispose();
				}

				_socket.Dispose();
			}
		}

		private void Disconnect(SocketEvent socketEvent) {
			Schedule(() => {
				if (socketEvent.AcceptSocket.Connected) {
					socketEvent.AcceptSocket.Shutdown(SocketShutdown.Both);
					socketEvent.AcceptSocket.Disconnect(true);
				}

				var client = (TcpClientBase)socketEvent.UserToken;
				client.Disconnect();
				_clients.Remove(client.Id, out var _);
				socketEvent.UserToken = null;
				_receiveEventPool.Release(socketEvent);
				socketEvent.AcceptSocket.Dispose();

				PublishEvent(new ClientDisconnectedEvent(client));
			});
		}

		private void OnAccepted(SocketAsyncEventArgs socketEvent) {
			Schedule(() => {
				var client = CreateClient(_nextClientId++, socketEvent.AcceptSocket);
				client.Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

				var receiveEvent = _receiveEventPool.Acquire();
				receiveEvent.AcceptSocket = client.Socket;
				receiveEvent.UserToken = client;
				_clients[client.Id] = client;
				
				StartReceive(receiveEvent);
				StartAccept(socketEvent);

				PublishEvent(new ClientConnectedEvent(client));
			});
		}

		private void OnDisconnected(SocketEvent socketEvent) {
			Disconnect(socketEvent);
		}

		private void OnIOCompleted(object sender, SocketAsyncEventArgs socketEvent) {
			switch (socketEvent.LastOperation) {
				case SocketAsyncOperation.Accept:
					OnAccepted(socketEvent);
				break;

				case SocketAsyncOperation.Receive:
					OnReceived((SocketEvent)socketEvent);
				break;

				case SocketAsyncOperation.Send:
					OnSent(socketEvent);
				break;

				case SocketAsyncOperation.Disconnect:
					OnDisconnected((SocketEvent)socketEvent);
				break;
			}
		}

		private void OnReceived(SocketEvent socketEvent) {
			Schedule(() => {
				if (socketEvent.BytesTransferred > 0) {
					var userToken = (TcpClientBase)socketEvent.UserToken;

					try {
						for (var i = 0; i < socketEvent.BytesTransferred; i++) {
							if (socketEvent.Segment[i] == 10 && userToken.CurrentReceiveLength > 0) {
								var dataReceivedEvent = new DataReceivedEvent(userToken, userToken.CurrentReceiveBuffer, userToken.CurrentReceiveLength);

								userToken.CurrentReceiveLength = 0;
								
								PublishEvent(dataReceivedEvent);

								continue;
							}
							
							userToken.CurrentReceiveBuffer[userToken.CurrentReceiveLength++] = socketEvent.Segment[i];
						}
					} catch (Exception e) {
						_logger.LogError(e, "Exception in {ServerType} while attempting to receive {BytesTransferred} bytes, user token buffer has recorded length {CurrentReceiveLength}", nameof(TcpServerBase), socketEvent.BytesTransferred, userToken.CurrentReceiveLength);

						if (userToken.CurrentReceiveLength >= userToken.CurrentReceiveBuffer.Length) {
							_logger.LogWarning("Recorded length {CurrentReceiveLength} exceeded user token buffer length {CurrentReceiveBufferLength}", userToken.CurrentReceiveLength, userToken.CurrentReceiveBuffer.Length);

							userToken.CurrentReceiveLength = 0;
						}
					}

					StartReceive(socketEvent);
				} else {
					Disconnect(socketEvent);
				}
			});
		}

		private void OnSent(SocketAsyncEventArgs socketEvent) {
			Schedule(() => {
				socketEvent.SetBuffer(null, 0, 0);
				socketEvent.Dispose();
			});
		}

		private void PublishEvent(IServerEvent serverEvent) {
			Schedule(() => {
				_events.Enqueue(serverEvent);
			});
		}

		private void Schedule(Action action) {
			if (!_disposed) {
				_scheduler.Schedule(action);
			}
		}

		private void StartAccept(SocketAsyncEventArgs socketEvent) {
			Schedule(() => {
				if (socketEvent == null) {
					socketEvent = new SocketAsyncEventArgs();
					socketEvent.Completed += OnIOCompleted;
				} else {
					socketEvent.AcceptSocket = null;
				}

				if (!_socket.AcceptAsync(socketEvent)) {
					OnAccepted(socketEvent);
				}
			});
		}

		private void StartReceive(SocketEvent socketEvent) {
			Schedule(() => {
				try {
					if (socketEvent.AcceptSocket.Connected && !socketEvent.AcceptSocket.ReceiveAsync(socketEvent)) {
						OnReceived(socketEvent);
					}
				} catch (Exception e) {
					_logger.LogError(e, "Exception in {ServerType} while starting receive from client", nameof(TcpServerBase));
				}
			});
		}

		private readonly int _backlogSize;
		private readonly Dictionary<ulong, TcpClientBase> _clients;
		private bool _disposed;
		private readonly Queue<IServerEvent> _events;
		protected readonly ILogger _logger;
		private ulong _nextClientId;
		private readonly IObjectPool<SocketEvent> _receiveEventPool;
		private readonly IScheduler _scheduler;
		private readonly Socket _socket;
	}
}