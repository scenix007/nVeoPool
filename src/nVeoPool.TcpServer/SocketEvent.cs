#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Net.Sockets;
using nVeoPool.TcpServer.Memory;

namespace nVeoPool.TcpServer {
	public sealed class SocketEvent : SocketAsyncEventArgs, IPoolable {
		public SocketEvent(BufferPool pool, EventHandler<SocketAsyncEventArgs> callback) {
			_pool = pool;

			Completed += callback;
		}

		public ArraySegment<byte> Segment { get; private set; }

		#region IPoolable
		public void CheckIn() {
			_pool.Release(Segment);
			SetBuffer(null, 0, 0);
		}

		public void CheckOut() {
			Segment = _pool.Acquire();

			SetBuffer(Segment.Array, Segment.Offset, Segment.Count);
		}
		#endregion

		private readonly BufferPool _pool;
	}
}