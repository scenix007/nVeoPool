#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.Common.Configuration.Caching {
	public class CacheExpirationConfiguration {
		[DefaultValue(DEFAULT_ALL_BLOCK_CANDIDATES_AGE_SECONDS)]
		[JsonProperty(PropertyName = "BlockCandidates")]
		public int AllBlockCandidatesAgeSeconds { get; set; } = DEFAULT_ALL_BLOCK_CANDIDATES_AGE_SECONDS;

		[DefaultValue(DEFAULT_ALL_PAYMENTS_FOR_MINER_AGE_SECONDS)]
		[JsonProperty(PropertyName = "MinerPayments")]
		public int AllPaymentsForMinerAgeSeconds { get; set; } = DEFAULT_ALL_PAYMENTS_FOR_MINER_AGE_SECONDS;

		[DefaultValue(DEFAULT_BALANCE_FOR_MINER_AGE_SECONDS)]
		[JsonProperty(PropertyName = "MinerBalance")]
		public int BalanceForMinerAgeSeconds { get; set; } = DEFAULT_BALANCE_FOR_MINER_AGE_SECONDS;

		[DefaultValue(DEFAULT_MAXIMUM_STALE_AGE_SECONDS)]
		[JsonProperty(PropertyName = "Stale")]
		public int MaximumStaleAgeSeconds { get; set; } = DEFAULT_MAXIMUM_STALE_AGE_SECONDS;

		[DefaultValue(DEFAULT_STATS_AGE_SECONDS)]
		[JsonProperty(PropertyName = "Stats")]
		public int StatsAgeSeconds { get; set; } = DEFAULT_STATS_AGE_SECONDS;

		[DefaultValue(DEFAULT_TIME_SERIES_AGE_SECONDS)]
		[JsonProperty(PropertyName = "Series")]
		public int TimeSeriesAgeSeconds { get; set; } = DEFAULT_TIME_SERIES_AGE_SECONDS;

		private const int DEFAULT_ALL_BLOCK_CANDIDATES_AGE_SECONDS = 1*60;
		private const int DEFAULT_ALL_PAYMENTS_FOR_MINER_AGE_SECONDS = 2*60;
		private const int DEFAULT_BALANCE_FOR_MINER_AGE_SECONDS = 2*60;
		private const int DEFAULT_MAXIMUM_STALE_AGE_SECONDS = 15*60;
		private const int DEFAULT_STATS_AGE_SECONDS = 2*60;
		private const int DEFAULT_TIME_SERIES_AGE_SECONDS = 2*60;
	}
}
