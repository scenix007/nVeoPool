#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Newtonsoft.Json;

namespace nVeoPool.Common.Configuration.Clustering {
	public class ClusteringConfiguration : IClusteringConfiguration {
		public ClusteringConfiguration(String serverId, ClusterServerType? serverType) {
			ServerId = !String.IsNullOrWhiteSpace(serverId) ? serverId : CreateServerId();
			ServerType = serverType ?? DefaultServerType;
		}

		#region IClusteringConfiguration
		[JsonProperty(PropertyName = "Id")]
		public String ServerId { get; }

		[JsonProperty(PropertyName = "Type")]
		public ClusterServerType ServerType { get; }
		#endregion

		public static String CreateServerId() => Guid.NewGuid().ToString();

		public const ClusterServerType DefaultServerType = ClusterServerType.Master;
	}
}