#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.Common.Extensions {
	public static class DecimalExtensionMethods {
		public static decimal FromVeoBaseUnits(this decimal value) {
			return value / VEO_UNIT_MULTIPLIER;
		}

		public static decimal ToVeoBaseUnits(this decimal value) {
			return value * VEO_UNIT_MULTIPLIER;
		}

		public static decimal TruncateToPaymentPrecision(this decimal value) {
			return TruncateToPrecision(value, 8);
		}

		public static decimal TruncateToPrecision(this decimal value, int precision) {
			var step = (decimal)Math.Pow(10, precision);

			return Math.Truncate(step * value) / step;
		}

		private const decimal VEO_UNIT_MULTIPLIER = 100000000;
	}
}