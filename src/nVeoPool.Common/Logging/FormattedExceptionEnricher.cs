#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Serilog.Core;
using Serilog.Events;

namespace nVeoPool.Common.Logging {
	public class FormattedExceptionEnricher : ILogEventEnricher {
		#region ILogEventEnricher
		public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory) {
			if (logEvent.Exception != null) {
				logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("FormattedException", FormatExceptionString(logEvent.Exception)));
			}
		}
		#endregion

		private String FormatExceptionString(Exception rootException) {
			var formattedString = $"{rootException.GetType().FullName}: {rootException.Message}{Environment.NewLine}";
			var innerException = rootException.InnerException;
			var padding = "  ";

			while (innerException != null) {
				formattedString = $"{formattedString}{padding}{innerException.GetType().FullName}: {innerException.Message}{Environment.NewLine}";

				innerException = innerException.InnerException;

				padding = $"{padding}  ";
			}

			return formattedString;
		}
	}
}
