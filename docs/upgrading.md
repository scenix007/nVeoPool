# Upgrading
Before upgrading it is always a good idea to first backup the database, even if there are no database upgrades involved.

## Steps
1) Stop the pool & API server(s) by pressing [ENTER] at the command line.

2) Backup the database.

3) Pull the latest code from the **master** branch. Any new configuration options will be noted in the [CHANGELOG](../CHANGELOG.md) and documented in the [Configuration](configuration.md) document.

4) If there are any database upgrades required (also noted in the [CHANGELOG](../CHANGELOG.md)), run the appropriate script(s). The **UpgradeHistory** database table contains a list of which scripts have been run, so make sure to only run scripts that have not already been run. Scripts are intended to be run from the PSQL command line app (set username/password/DB options for your installation):

```
psql --username=postgres --dbname=nVeoPool --file=SCRIPT_NAME.sql
```
If there are multiple upgrade scripts to be run since the last update, run one single script at a time from earliest to latest. The scripts are named chronologically by date in YYYYMMDD_### format (e.g. 20180612_000.sql) to make it easy to determine their order.

IMPORTANT: Ensure that you never run the **setup.sql** script against an existing database, it is intended only for new installations.

5) Restart the pool & API server(s).