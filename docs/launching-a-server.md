# Launching a Server
The bin/ directory contains two run-server scripts, one each for Linux and Windows. Each script is run the same way, by passing either "api" or "pool" to launch the API or pool server. For example, to launch the pool server on Linux:
```
$ chmod +x bin/run-server.sh
$ ./bin/run-server.sh pool [CONFIG_FILE_PATH]
```
To launch the API server on Windows:
```
> bin\run-server.bat api [CONFIG_FILE_PATH]
```
The [CONFIG_FILE_PATH] argument is optional and defaults to nVeoPool.ApiServer/bin/Release/netcoreapp2.0/api-settings.json or nVeoPool.PoolServer/bin/Release/netcoreapp2.0/pool-settings.json for the API server and pool server, repsectively.