# Database Configuration
The included setup.sql script (in the "scripts" directory) is meant to be run from the [psql console application](https://www.postgresql.org/docs/current/static/app-psql.html) included with PostgreSQL. This script will create a database called "nVeoPool" for the "postgres" user. For now the only way to modify these values is to edit the script before you run it. Make note of the database and user you use as these are required for the database connection string.

# Pool Server Configuration
Here is a full sample configuration file for the pool server. Each section will be explained below.
```
{
	"banning": {
		"$type":"nVeoPool.PoolServer.Configuration.Banning.BanningConfiguration, nVeoPool.PoolServer",
		"whitelistEnabled": false
	},
	"cache": {
		"$type":"nVeoPool.Common.Configuration.Caching.RedisCacheConfiguration, nVeoPool.Common",
		"connectionString": "localhost"
	},
	"clustering": {
		"$type":"nVeoPool.Common.Configuration.Clustering.ClusteringConfiguration, nVeoPool.Common",
		"id": "pool-master-00",
		"type": "Master"
	},
	"database": {
		"$type":"nVeoPool.Common.Configuration.Database.DatabaseConfiguration, nVeoPool.Common",
		"connectionString": "host=localhost;port=5432;database=nVeoPool;username=postgres;password=[PASSWORD]"
	},
	"logging": {
		"$type":"nVeoPool.Common.Configuration.Logging.LoggingConfiguration, nVeoPool.Common",
		"enableSelfLogging": false,
		"fileName": "log-poolServer.txt",
		"includeStackTrace": false,
		"level": "Warning"
	},
	"nodeRpc": {
		"$type":"nVeoPool.Common.Configuration.Daemon.DaemonClientConfiguration, nVeoPool.Common",
		"backup": {
			"nodes": [
				{
					"id": "backup-00",
					"ip": "192.168.1.1",
					"externalApiPort": 8080,
					"internalApiPort": 8081
				},
				{
					"id": "backup-01",
					"ip": "192.168.1.2",
					"externalApiPort": 8080,
					"internalApiPort": 8081
				}
			],
			"strategy": {
				"breakDuration": 180,
				"failureThreshold": 70,
				"minimumRequests": 20,
				"sampleDuration": 90
			}
		},
		"primary": {
			"id": "primary",
			"ip": "127.0.0.1",
			"externalApiPort": 8080,
			"internalApiPort": 8081
		}
	},
	"notifications": {
		"$type":"nVeoPool.PoolServer.Configuration.Notifications.NotificationsConfiguration, nVeoPool.PoolServer",
		"discord": [
			{
				"delayOnThrottle": 5000,
				"eventIds": [ 8000, 9000, 10000, 110000 ],
				"includeEventContext": false,
				"maximumRetries": 3,
				"webHookId": "WEBHOOK_ID",
				"webHookToken": "WEBHOOK_TOKEN"
			}
		]
	},
	"payment": {
		"$type":"nVeoPool.PoolServer.Configuration.Payment.PaymentConfiguration, nVeoPool.PoolServer",
		"additionalFees": {
			"BMWxXLrnUyCMENdFbmC0Ej4qIEMv8Lv6ezfpfhdlZI6VxCb7YDqOz0x6in3AtNP+LVW+Subt3EKO/wY5WVBfHBY=": 0.05
		},
		"batchSendDelay": 0,
		"batchSendSize": 0,
		"blockMaturityDepth": 10,
		"devDonation": 0.1,
		"groupPayments": false,
		"individualSendDelay": 1000,
		"minimumPayoutThreshold": 0.1,
		"paymentMaturityDepth": 10,
		"poolFee": 1.95,
		"roundLength": 10,
		"strategy": "pplns",
		"transactionFee": 0.0015205
	},
	"serviceTimers": {
		"$type":"nVeoPool.PoolServer.Configuration.Services.ServicesConfiguration, nVeoPool.PoolServer",
		"archivedShareLifetime": 5,
		"banManagement": 60,
		"blockCandidateProcessor": 300,
		"cacheCleanup": 1800,
		"difficultyCalculator": 30,
		"heartbeat": 15,
		"invalidShareCleanup": 604800,
		"invalidShareStorage": 30,
		"nodeFailover": 60,
		"nodeSync": 2,
		"notification": 5,
		"paymentConfirmation": 600,
		"paymentSender": 43200,
		"shareArchiver": 43200,
		"shareStorage": 3,
		"workRetriever": 2000,
	},
	"varDiff": {
		"$type":"nVeoPool.PoolServer.Configuration.VarDiff.VarDiffConfiguration, nVeoPool.PoolServer",
		"initialDifficulty": 9800,
		"intervalSamples": 5,
		"intervalVariance": 25,
		"maximumDifficulty": 11000,
		"maximumDifficultyJump": 3,
		"minimumDifficulty": 9000,
		"targetShareInterval": 30
	},
	"workProtocol": {
		"$type":"nVeoPool.PoolServer.Configuration.WorkProtocol.WorkProtocolConfiguration, nVeoPool.PoolServer",
		"getWork": {
			"enableRequestLogging": false,
			"keepAliveTimeout": 120,
			"listenIpAddress": "0.0.0.0",
			"listenPort": 8085,
			"maximumConcurrentConnections": 15000,
			"maximumRequestBodySize": 384
			"maximumRequestBufferSize": 1048576,
			"maximumRequestHeaders": 10,
			"maximumRequestHeadersSize": 256,
			"maximumRequestLineSize": 128,
			"maximumResponseBufferSize": 65536,
			"maximumWaitOnClose": 30,
			"requestHeadersTimeout": 5,
			"route": "work"
		},
		"maximumInvalidShareBacklog": 1000,
		"maximumShareBacklog": 1000,
		"maximumStaleWorkDuration": 300,
		"maximumTrailingBlocks": 2,
		"requireSubscribe": false,
		"stratum": [
			{
				"backlog": 1000,
				"idleTimeout": 120,
				"listenIpAddress": "0.0.0.0",
				"listenPort": 8086,
				"maximumMessageSize": 768,
				"maximumWaitOnIdleDisconnect": 5,
				"varDiff": {
					"initialDifficulty": 9800
				}
			}
		]
	}
}
```

## **banning**
### $type (String, Required)
> Leave the default/example value for now.

### whitelistEnabled (Boolean, Optional)
> Whether or not to enable checking miner public keys against the whitelist before allowing mining.

## **cache**
### $type (String, Required)
> Leave the default/example value for now.

### connectionString (String, Required)
> The Redis connection string. Full configuration options can be found [here](https://stackexchange.github.io/StackExchange.Redis/Configuration#configuration-options).

## **clustering**
### $type (String, Required)
> Leave the default/example value for now.

### id (String, Optional)
> The id of the server.

### type (String, Optional)
> The type of the server, either Master or Slave.

## **database**
### $type (String, Required)
> Leave the default/example value for now.

### connectionString (String, Required)
> The PostgreSQL connection string. Full configuration options can be found [here](http://www.npgsql.org/doc/connection-string-parameters.html).

## **logging**
### $type (String, Required)
> Leave the default/example value for now.

### enableSelfLogging (Boolean, Optional)
> Whether or not to enable internal/self logging of the logger itself.

### fileName (String, Required)
> The name of the file to log to. The recommended configuration is to specify the full file path, like "/var/logs/nVeoPool/log-poolServer.txt" or "C:\Logs\log-poolServer.txt".

### includeStackTrace (Boolean, Optional)
> Whether or not to include the full stack trace with logged exceptions.

### level (String, Required)
> The level to log at. Possible values: Verbose, Debug, Information, Warning, Error, Fatal.

## **nodeRpc**
### $type (String, Required)
> Leave the default/example value for now.

### backup (Object, Optional)
> Define this object to enable backup nodes.

```
{
  *nodes (Array<Object>, Required)
    -The collection of backup nodes each defined with the same format as the "primary" key under the "nodeRpc" key. Each backup node can also contain a "strategy" key that overrides the failover strategy defined in the object below.

  *strategy (Object, Optional)
    -Define this object to override the default failover strategy.
      {
        *breakDuration (Number, Optional)
          -The number of seconds for the node to remain unreachable to allow it to recover.

        *failureThreshold (Number, Optional)
          -The minimum number of requests that must fail, expressed as percentage points.

        *minimumRequests (Number, Optional)
          -The minimum numer of requests that must be attempted during the sample duration before any potential failure.

        *sampleDuration (Number, Optional)
          -The number of seconds to watch for failing requests, operated as a moving window.
      }
}
```

### primary (Object, Optional)
> Define this object to override the default primary node configuration.

```
{
  *id (String, Optional)
    -The custom id used for logging & reporting, defaults to the node address.

  *ip (String, Optional)
    -The IP address of the primary Amoveo node used for mining.

  *externalApiPort (Number, Optional)
    -The port number for accessing the external API of the primary node.

  *internalApiPort (Number, Optional)
    -The port number for accessing the internal API of the primary node.
}
```

## **notifications**
### $type (String, Required)
> Leave the default/example value for now.

### discord (Array<Object>, Optional)
> Define this array to receive webhook notifications for desired events. Multiple WebHooks can be configured to received different events.

```
[
  {
    *delayOnThrottle (Number, Optional)
      -The number of milliseconds to wait after receiving a 429 "Too Many Requests" response. Set to 0 for no delay.

    *includeEventContext (Boolean, Optional)
      -Whether or not to prefix the message with additional logging context

    *eventIds (Array<Number>, Required)
      -The events to forward to this Discord WebHook.

    *maximumRetries (Number, Optional)
      -The maximum number of attempts to retry a failed delivery. The total number of attempts will be 1 + maximumRetries.

    *webHookId (String, Required)
      -The Discord WebHook Id.

    *webHookToken (String, Required)
      -The Discord WebHook token.
  }
]
```

> The following is a list of available event ids:

```
1000   - Block candidate accepted by node
2000   - Block candidate rejected by node
3000   - Block candidates successfully processed
3250   - Error processing block candidates
3500   - Error processing individual block candidate
4000   - Unrecorded block candidate successfully found & saved
5000   - Error retrieving height from node for work
6000   - Error retrieving mining data from node for work
6250   - Error saving accepted shares
6500   - Error saving invalid shares
6750   - Maximum accepted share backlog size exceeded
6875   - Maximum invalid share backlog size exceeded
7000   - No work available
8000   - Possible backup node(s) sync issue
9000   - Possible primary node sync issue
10000  - Primary node recovered
10250  - Error processing block rewards
10500  - Error processing individual block reward
11000  - Error saving successfully sent miner payments
12000  - Error sending individual payment
12500  - No shares found for block reward
13000  - Payment service is starting payment process
14000  - Block rewards successfully saved for individual block
15000  - Payment service halted to avoid possible double-spend
16000  - Payment confirmation service failed to find individual transaction
```

## **payment**
### $type (String, Required)
> Leave the default/example value for now.

### additionalFees (Dictionary<String, Number>, Optional)
> Additional fees (beyond the base pool fee) specified as public key/percent pairs. The example value of 0.05 is a 0.05% fee.

### batchSendDelay (Number, Optional)
> The number of seconds between sending each batch of payments.

### batchSendSize (Number, Optional)
> The number of individual payments that make up a single batch of payments.

### blockMaturityDepth (Number, Required)
> The number of blocks that are mined before a block is considered confirmed and processed for payment.

### devDonation (Number, Optional)
> The percent of each block reward donated to the pool software developer, expressed as percentage points. The example (and default) value of 0.1 is a 0.1% donation.

### groupPayments (Boolean, Optional)
> Whether or not to group payments using multi-spend into a single transaction. If batchSendSize is set, each transaction will have that many payments, otherwise all payments will be sent in one transaction.

### individualSendDelay (Number, Optional)
> The number of milliseconds between submitting each individual payment to the node. Ignored if using groupPayments.

### minimumPayoutThreshold (Number, Required)
> Minimum number of VEO required before a miner will be paid their pending balance.

### paymentMaturityDepth (Number, Optional)
> The number of blocks that are mined before a payment transaction is considered confirmed and marked for resend.

### poolFee (Number, Required)
> The percent of each block reward kept for the pool, expressed as percentage points. The example value of 1.95 is a 1.95% fee.

### roundLength (Number, Optional)
> The number of blocks considered when calculating payments. This only applies to PPLNS and does not only count blocks mined by the pool but instead looks at the overall network.

### strategy (String, Optional)
> The type of payment strategy used, either "pplns" for PPLNS or "prop" for PROP. When using PPLNS also set the roundLength option, for PROP the roundLength option is ignored.

### transactionFee (Number, Optional)
> The fee charged to a miner for payment, measured in VEO. This fee is not used for the network spend call but instead will be subtracted from a miner's payment before spend.

## **serviceTimers** (Don't touch these unless you know what you're doing!)
### $type (String, Required)
> Leave the default/example value for now.

### archivedShareLifetime (Number, Optional)
> The number of days to keep archived shares before deleting them completely.

### blockCandidateProcessor (Number, Optional)
> The number of seconds to wait between checks for mature/orphan blocks.

### cacheCleanup (Number, Optional)
> The number of seconds to wait between freeing up some cache space.

### difficultyCalculator (Number, Optional)
> The number of seconds to wait between recalculating difficulties for active workers.

### heartbeat (Number, Optional)
> The number of seconds between each server heartbeat (available via the API).

### invalidShareCleanup (Number, Optional)
> The number of seconds to wait between permanently deleting all invalid shares since the last run.

### invalidShareStorage (Number, Optional)
> The number of seconds to wait between checks for more new invalid shares to store in the database.

### nodeFailover (Number, Optional)
> The number of seconds to wait between checks for potentially out of sync primary/backup nodes.

### nodeSync (Number, Optional)
> The number of seconds to wait between checks for whether a sync should be forced.

### notification (Number, Optional)
> The number of seconds to wait between checks for new notifications to process.

### paymentConfirmation (Number, Optional)
> The number of seconds to wait between checks for (un)confirmed payment transactions.

### paymentSender (Number, Optional)
> The number of seconds to wait between sending payments for miners over the minimum payout threshold.

### shareArchiver (Number, Optional)
> The number of seconds to wait between pruning old shares.

### shareStorage (Number, Optional)
> The number of seconds to wait between checks for more new shares to store in the database.

### workRetriever (Number, Optional)
> The number of milliseconds to wait between checks for new work from the Amoveo node.


## **varDiff**
### $type (String, Required)
> Leave the default/example value for now.

### initialDifficulty (Number, Required)
> The initial job difficulty for a new miner.

### intervalSamples (Number, Optional)
> The number of share interval samples used to calculate difficulty.

### intervalVariance (Number, Optional)
> The percent by which the target share interval can vary for a miner, expressed as percentage points. The example value of 25 indicates an allowed variance of 25% up or down from the target share interval.

### maximumDifficulty (Number, Optional)
> The maximum job difficulty for a miner. Leave this (along with minimumDifficulty) empty to use initialDifficulty as the fixed difficulty for all jobs.

### maximumDifficultyJump (Number, Optional)
> The maximum percent by which the current job difficulty can be adjusted in one difficulty adjustment period, expressed as percentage points. The example value of 3 indicates 3%.

### minimumDifficulty (Number, Optional)
> The minimum job difficulty for a miner. Leave this (along with maximumDifficulty) empty to use initialDifficulty as the fixed difficulty for all jobs.

### targetShareInterval (Number, Optional)
> The ideal number of seconds between a miner's share submissions. Their difficulty will be adjusted periodically to try to get close to this value.

## **workProtocol**
### $type (String, Required)
> Leave the default/example value for now.

### getWork (Object, Optional)
> Define this object to enable the HTTP-based GetWork protocol.

```
{
  *enableRequestLogging (Boolean, Optional)
    -Whether or not to enable HTTP request logging. Requests are logged at Information level so ensure that "level" in the "logging" configuration section is set to "Information" (or lower) to fully enable request logging.

  *keepAliveTimeout (Number, Optional)
    -The number of seconds to keep-alive connections.

  *listenIpAddress (String, Required)
    -The IP address to listen on.

  *listenPort (Number, Required)
    -The port to listen on. Make sure to use a different port than Stratum and the API server.

  *maximumConcurrentConnections (Number, Optional)
    -The maximum number of open HTTP(S) connections.

  *maximumRequestBodySize (Number, Optional)
    -The maximum size of a request body in bytes.

  *maximumRequestBufferSize (Number, Optional)
    -The maximum size of the request buffer in bytes.

  *maximumRequestHeaders (Number, Optional)
    -The maximum number of allowed HTTP headers in a request.

  *maximumRequestHeadersSize (Number, Optional)
    -The maximum size of the request headers in bytes.

  *maximumRequestLineSize (Number, Optional)
    -The maximum size of the HTTP request line in bytes.

  *maximumResponseBufferSize (Number, Optional)
    -The maximum size of the response buffer in bytes.

  *maximumWaitOnClose (Number, Optional)
    -The maximum number of seconds to wait to gracefully close existing connections before forcefully closing them on server shutdown.

  *requestHeadersTimeout (Number, Optional)
    -The maximum number of seconds to spend receiving headers for a request.

  *route (String, Optional)
    -The route suffix to retrieve/submit work, can be left empty to use simply server:port for work.
}
```

### maximumInvalidShareBacklog (Number, Optional)
> The maximum number of allowed outstanding invalid shares waiting to be saved before logging a warning. Set to 0 to disable warnings.

### maximumShareBacklog (Number, Optional)
> The maximum number of allowed outstanding accepted shares waiting to be saved before logging a warning. Set to 0 to disable warnings.

### maximumStaleWorkDuration (Number, Optional)
> The maximum number of seconds the current mining data can remain unchanged before signalling "No Work Available".

### maximumTrailingBlocks (Number, Optional)
> The maximum number of blocks the current node can be behind other configured nodes before switching.

### requireSubscribe (Boolean, Optional)
> Whether or not a miner must call GetWork/subscribe before submitting a share.

### stratum (Array<Object>, Optional)
> Define this object to enable the TCP-based Stratum protocol. When using a master-slave setup make sure the slaves use the same port configuration as the master to ensure proper operation.

```
[
  {
    *backlog (Number, Optional)
      -The default backlog size of the TCP server.

    *idleTimeout (Number, Optional)
      -The maximum number of seconds that can pass between submits before a client will be automatically disconnected. Set to 0 to disable automatically disconnecting idle clients.

    *listenIpAddress (String, Required)
      -The IP address to listen on.

    *listenPort (Number, Required)
      -The port to listen on. Make sure to use a different port than GetWork and the API server.

    *maximumMessageSize (Number, Optional)
      -The maximum number of bytes allowed in one single message (i.e. per line).

    *maximumWaitOnIdleDisconnect (Number, Optional)
      -The maximum number of seconds to wait to gracefully disconnect an idle client before forcefully disconnecting them.

    *varDiff (Object, Optional)
      -Define this to override the global "varDiff" configuration. This object has the same format/properties as the global "varDiff" configuration.
  }
]
```

# API Server Configuration
Here is a full sample configuration file for the API server. Each section will be explained below.
```
{
	"cache": {
		"$type": "nVeoPool.Common.Configuration.Caching.RedisCacheConfiguration, nVeoPool.Common",
		"backup": [
			{
				"connectionString": "192.168.1.1"
			},
			{
				"connectionString": "192.168.1.2"
			}
		],
		"connectionString": "localhost"
	},
	"database": {
		"$type": "nVeoPool.Common.Configuration.Database.DatabaseConfiguration, nVeoPool.Common",
		"connectionString": "host=localhost;port=5432;database=nVeoPool;username=postgres;password=[PASSWORD]"
	},
	"http": {
		"$type": "nVeoPool.Common.Configuration.Http.HttpConfiguration, nVeoPool.Common",
		"enableRequestLogging": false,
		"listenIpAddress": "0.0.0.0",
		"listenPort": 8087,
		"maximumWaitOnClose": 30,
		"origins": [ "http://mypool.com", "http://www.mypool.com" ]
	},
	"logging": {
		"$type":"nVeoPool.Common.Configuration.Logging.LoggingConfiguration, nVeoPool.Common",
		"enableSelfLogging": false,
		"fileName": "log-apiServer.txt",
		"includeStackTrace": false,
		"level": "Warning"
	},
	"query": {
		"$type":"nVeoPool.ApiServer.Configuration.Query.QueryConfiguration, nVeoPool.ApiServer",
		"maximumBlocks": 50,
		"maximumMinerPayments": 0,
		"maximumTopMiners": 0,
		"series": {
			"maximumQueryAge": 86400,
			"maximumStorageDuration": 172800,
			"minimumCaptureInterval": 60
		}
	},
	"serviceTimers": {
		"$type":"nVeoPool.ApiServer.Configuration.Services.ServicesConfiguration, nVeoPool.ApiServer",
		"dataCacher": 60,
		"dataCacherSync": 1,
		"heartbeat": 15,
	}
}
```

## **cache**
### $type (String, Required)
> Leave the default/example value for now.

### backup (Array<Object>, Optional)
> Define this collection to monitor servers attached to backup Redis instances.
```
[
  {
    *connectionString (String, Required)
      -The Redis connection string.
  }
]
```

### connectionString (String, Required)
> The Redis connection string. Full configuration options can be found [here](https://stackexchange.github.io/StackExchange.Redis/Configuration#configuration-options).

## **database**
### $type (String, Required)
> Leave the default/example value for now.

### connectionString (String, Required)
> The PostgreSQL connection string. Full configuration options can be found [here](http://www.npgsql.org/doc/connection-string-parameters.html).

## **http**
### $type (String, Required)
> Leave the default/example value for now.

### enableRequestLogging (Boolean, Required)
> Whether or not to enable HTTP request logging. Requests are logged at Information level so ensure that "level" in the "logging" configuration section is set to "Information" (or lower) to fully enable request logging.

### listenIpAddress (String, Required)
> The IP address to listen on.

### listenPort (Number, Required)
> The port to listen on. If the API server is run on the same server as the pool make sure to use different port numbers.

### maximumWaitOnClose (Number, Optional)
> The maximum number of seconds to wait to gracefully close existing connections before forcefully closing them on server shutdown.

### origins (Array<String>, Optional)
> A list of valid "Origin" domains. By default the server sends "*" for the Access-Control-Allow-Origin header. See [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) for more information.

## **logging**
### $type (String, Required)
> Leave the default/example value for now.

### enableSelfLogging (Boolean, Optional)
> Whether or not to enable internal/self logging of the logger itself.

### fileName (String, Required)
> The name of the file to log to. The recommended configuration is to specify the full file path, like "/var/logs/nVeoPool/log-apiServer.txt" or "C:\Logs\log-apiServer.txt".

### includeStackTrace (Boolean, Optional)
> Whether or not to include the full stack trace with logged exceptions.

### level (String, Required)
> The level to log at. Possible values: Verbose, Debug, Information, Warning, Error, Fatal.

## **query**
### $type (String, Required)
> Leave the default/example value for now.

### maximumBlocks (Number, Optional)
> The maximum number of blocks that can be returned from the /api/pool/blocks endpoint.

### maximumMinerPayments (Number, Optional)
> The maximum number of miner payments that can be returned from the /api/miners/payment endpoint.

### maximumTopMiners (Number, Optional)
> The maximum number of miners that can be returned from the /api/miners/top endpoint.

### series (Object, Optional)
> Define this object to override default time-series configuration.

```
{
  *maximumQueryAge (Number, Optional)
    -The maximum age of data in seconds returned by the time series API.

  *maximumStorageDuration (Number, Optional)
    -The maximum age of data in seconds stored for use by the time series API.

  *minimumCaptureInterval (Number, Optional)
    -The minimum number of seconds to wait between captures by the Data Cacher service.
}
```

## **serviceTimers** (Don't touch these unless you know what you're doing!)
### $type (String, Required)
> Leave the default/example value for now.

### dataCacher (Number, Optional)
> The number of seconds to wait between caching data used for various API responses.

### dataCacherSync (Number, Optional)
> The number of seconds to wait between checks for acquiring or releasing the global statistics update lock.

### heartbeat (Number, Optional)
> The number of seconds between each server heartbeat (available via the API).