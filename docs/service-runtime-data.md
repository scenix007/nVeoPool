# Service Runtime Data
All services are built to report at a minimum basic runtime information, such as whether they're sleeping, running, and when their estimated next run will occur. A number of services also report additional information about their specific runtime activity. Regardless of whether a service reports additional information, all services runtime data is accessed via the /api/admin/servers/{ServerId}/services/{ServiceId} endpoint. See the [API Endpoints](api-endpoints.md) document for more information on accessing the admin endpoints.

# Specific Runtime Data
The format for the minimum basic runtime information reported for all services resembles the following:

```
Running service:
{
	"currentRunStartedOn": 1527847958,
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}

Sleeping service:
{
	"estimatedNextRunOn": 1529977050,
	"lastCompletedOn": 1529976930,
	"runtimeStatus": "Sleeping"
}
```

> currentRunStartedOn - The timestamp of when the service started the current run. Not present when the service is "Sleeping".

> estimatedNextRunOn - The estimated timestamp of when the next run for the service will start. Not present when the service is "Running".

> lastCompletedOn - The timestamp of when the service completed the last run. Always present in the response.

> runtimeStatus - One of either "Running" or "Sleeping".

In addition to these fields, services that report additional information always do so via an additional "data" field that resembles the following:

```
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"someField": "someValue"
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

Exactly which fields are present in the "data" object depend on the service. Each service's "data" object format is described below. The text in brackets after each service name indicates which type of server the service runs on.

## DataCacherService [API]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"aggregateStatsUpdateResults": [
			{
				"updateDuration": 3,
				"windowLength": 1
			},
			{
				"updateDuration": 3,
				"windowLength": 6
			},
			{
				"updateDuration": 4,
				"windowLength": 12
			},
			{
				"updateDuration": 4,
				"windowLength": 24
			}
		],
		"lastSeriesRefreshOn": 1527847701
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> aggregateStatsUpdateResults - An array of results indicating how long each individual aggregate stat window refresh took. The update duration is measured in seconds, the window length in hours.

> lastSeriesRefreshOn - The timestamp of when the last time series refresh completed.

## DataCacherSynchronizationService [API]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847978,
	"data": {
		"writeLockHeld": true
	},
	"lastCompletedOn": 1527847741,
	"runtimeStatus": "Running"
}
```

> writeLockHeld - True if the global statistics update lock has been acquired, false if it has been released.

## InvalidShareStorageService [Pool]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"bufferCount": 0,
		"lastSavedOn": 1527847691,
		"queueCount": 0,
		"totalSavedCount": 10
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> bufferCount - The number of invalid shares pulled off the incoming queue waiting to be saved.

> lastSavedOn - The timestamp of when the last actual save occurred, i.e. when at least one invalid share was saved.

> queueCount - The number of incoming invalid shares waiting to be put in the storage buffer.

> totalSavedCount - The total number of invalid shares successfully saved since the service started, i.e. since pool startup.

## NodeFailoverService [Pool]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"backupNodes": [
			{
				"height": 26789,
				"nodeId": "secondary-primary"
			},
			{
				"nodeId": "node-0"
			},
			{
				"nodeId": "192.168.1.2"
			}
		],
		"orderedBackupNodes": [
			"secondary-primary",
			"node-0",
			"192.168.1.2"
		],
		"preferredNodeHeight": 26789,
		"preferredNodeId": "primary",
		"primaryNodeHeight": 26789
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> backupNodes - An array of backup nodes and their last retrieved height. Custom node id's will be shown if set, otherwise the node address is shown. If nodes lack a height that indicates a failure to retrieve their height on the latest run.

> orderedBackupNodes - An ordered array of the nodes not currently preferred sorted from highest to lowest block height. These are used by the Work Retriever service if retrieving work from the preferredNodeId fails.

> preferredNodeHeight - The current block height of the node in use.

> preferredNodeId - The node id of the current node in use, will show node address if no custom id was set.

> primaryNodeHeight - The primary node (which is not necessarily the preferred node) block height.

## NotificationService [Pool]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"queueCount": 0,
		"successfullyHandled": 0,
		"successfullyProcessed": 3,
		"unsuccessfullyHandled": 2,
		"unsuccessfullyProcessed": 0
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> queueCount - The number of notifications waiting to be processed.

> successfullyHandled - The number of successfully handled notifications. If a single notification is delivered to multiple sources each source is counted towards this total.

> successfullyProcessed - The number of successully processed notifications. If a single notification is delivered to multiple sources it is still only considered processed once.

> unsuccessfullyHandled - The number of unsuccessfully handled notifications. If a single notification is delivered to multiple sources only the sources which failed count towards this total.

> unsuccessfullyProcessed - The number of unsuccessfully processed notifications.

## PaymentSenderService [Pool]
The "data" field is only available while this service is "Running".

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"batchCount": 6,
		"currentBatch": 1,
		"errorCount": 0,
		"paymentCount": 16,
		"processedCount": 2,
		"remainingInBatch": 1,
		"remainingTotal": 14,
		"successfulCount": 2
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> batchCount - The total number of batches to be sent.

> currentBatch - The current batch number being processed.

> errorCount - The number of errors, either a failure to send or save a payment.

> paymentCount - The overall number of payments to be sent across all batches.

> processedCount - The number of payments that have been processed, i.e. total successes + failures.

> remainingInBatch - The remaining number of payments in the current batch.

> remainingTotal - The overall number of remaining payments yet to be processed.

> successfulCount - The number of successfully sent + saved payments.

## ShareStorageService [Pool]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"bufferCount": 15,
		"lastSavedOn": 1527847721,
		"queueCount": 0,
		"totalSavedCount": 15650
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> bufferCount - The number of valid shares pulled off the incoming queue waiting to be saved.

> lastSavedOn - The timestamp of when the last actual save occurred, i.e. when at least one valid share was saved.

> queueCount - The number of incoming valid shares waiting to be put in the storage buffer.

> totalSavedCount - The total number of valid shares successfully saved since the service started, i.e. since pool startup.

## WorkRetrieverService [Pool]
The "data" field is always available for this service.

```
Example response:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"currentNodeId": "primary",
		"blockHeight": 26789,
		"headerHeight": 26789,
		"isStale": false,
		"lastWorkRetrievedOn": 1529979579,
		"miningData": {
			"blockDifficulty": 14155,
			"blockHash": "xqu4EZ4q+NH9+VuYp/sJQJB3XBZOFrHmUmmGkHSHCU0="
		}
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```

> currentNodeId - The id of the node used to retrieve the current mining data.

> blockHeight - The block height of the node being used for mining.

> headerHeight - The header height of the node being used for mining.

> isStale - Whether or not the current mining data is stale. Miners will receive "No work available" if this is 'true'.

> lastWorkRetrievedOn - The timestamp of the last time the mining data was actually changed.

> miningData - The current mining data being given to miners.