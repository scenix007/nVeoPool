# API Migration From v1.x to v2.x
The API has been significantly refactored for v2 to improve overall performance and allow for more flexibility in how stats are queried. In v1.x of the API, all of the endpoints for retrieving stats included all information for 1 hour, 6 hour, and 24 hour windows. In v2.x of the API, all of the endpoints for retrieving stats now offer an optional "times" query string parameter to specify which windows are desired. Additionally, a 12-hour stat window is now available in addition to the previous 1 hour, 6 hour, and 24 hour windows.

Instead of using pre-emptive caching as in v1.x of the API, v2.x of the API now uses on-demand caching. This significantly reduces the amount of network traffic between the database and the API server. Additionally, the use of concurrently updated materialized views allows for quicker lock-free stats updates and retrievals.

Finally, many of the JSON field names in the API responses have been slightly renamed from v1.x to v2.x. The core data is the same but the field names have been shortened to reduce the number of bytes transferred, in some scenarios offering a signficant size reduction of the API response when combined with the "times" query string parameter.

# Endpoint Changes
Every endpoint that has changed from v1.x to v2.x is listed below. See the [API Endpoints](api-endpoints.md) document for the full documentation of the v2.x API.

## /api/miners?address={PublicKey}&includeWorkers={true|false}

> Offers the new "times" query string parameter for selecting desired stat windows.

> General response format changed to an array, with each array element being the stats for a specific stat window/time range.

```
Field name changes:

"acceptedShareCount[N]Hours" -> "shares"
"averageJobDifficulty[N]Hours" -> "avgJobDiff"
"individualWorkerStats" -> "workerList"
"invalidShareCount[N]Hours" -> "invalidShares"
"lastShareAcceptedOn" -> "lastShare"
"workerId" -> "id"
```

## /api/miners/workers?address={PublicKey}

> Offers the new "times" query string parameter for selecting desired stat windows.

> General response format changed to an array, with each array element being the worker list for a specific stat window/time range.

## /api/miners/workers?address={PublicKey}&workerId={WorkerId}

> Offers the new "times" query string parameter for selecting desired stat windows.

> General response format changed to an array, with each array element being the worker stats for a specific stat window/time range.

```
Field name changes:

"acceptedShareCount[N]Hours" -> "shares"
"averageJobDifficulty[N]Hours" -> "avgJobDiff"
"invalidShareCount[N]Hours" -> "invalidShares"
"lastShareAcceptedOn" -> "lastShare"
```

## /api/pool/stats

> Offers the new "times" query string parameter for selecting desired stat windows.

> General response format changed to an array, with each array element being the pool stats for a specific stat window/time range.

> No longer offers "includeMiners" query string parameter. A new API endpoint is planned for the scenario of retrieving "top N pool miners".

```
Field name changes:

"acceptedShareCount[N]Hours" -> "shares"
"averageJobDifficulty[N]Hours" -> "avgJobDiff"
"invalidShareCount[N]Hours" -> "invalidShares"
"lastShareAcceptedOn" -> "lastShare"
```