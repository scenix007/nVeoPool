# Changelog
All notable changes to this project will be documented in this file.

## v3.0.0.0 - 2018-08-14
### Added
- 20180814_000.sql database upgrade script (see [Upgrading](docs/upgrading.md))
- Event ids 3250, 3500, 10250, 10500, and 12500 (see [Notifications](docs/configuration.md#notifications) for their descriptions)
- Proportional payout option via "strategy" configuration option in the "payment" section (see [Configuration](docs/configuration.md#strategy-string-optional))
- Stratum multi-port support for defining multiple ports with optional per-port fixed/variable difficulty configuration (see [Configuration](docs/configuration.md#stratum-array-optional))
- Time-series API for hashrate (pool/miner/worker), shares (accepted & invalid pool/miner/worker), and worker count (miner/pool). Includes new "series" API configuration section and "dataCacherSync" API service timer option. See [API Endpoints](docs/api-endpoints.md#apiseriesminershashrateaddresspublickeystartstartendend) and [Configuration](docs/configuration.md#query).
- Worker count via "workers" field in the /api/miners?address={PublicKey} response

### Changed
- Renamed "local" configuration key in the "nodeRpc" section to "primary" (the fields within are unchanged)
- Removed "enableLegacyReceive" option
- Removed "poolPaysTransactionFee" option in favor of only using existing "transactionFee" option
- Removed "useLegacyDifficultyCheck" option

### Fixed
- Spelling error in payment service runtime data, "sucessfullCount" -> "successfulCount"

## v2.9.2.0 - 2018-07-31
### Added
- Event id 16000 for failed transaction confirmation (see [Configuration](docs/configuration.md#notifications) for all event ids)

## v2.9.1.0 - 2018-07-30
### Added
- "delayOnThrottle" Discord notification option to pause when being throttled (see [Configuration](docs/configuration.md#notifications))
- "maximumRetries" Discord notification option for maximum retry attempts on failed delivery (see [Configuration](docs/configuration.md#notifications))

### Fixed
- Pool startup when "poolPaysTransactionFee" is false

## v2.9.0.0 - 2018-07-21
### Added
- "limit" query string parameter to /api/miners/payment (see [API Endpoints](docs/api-endpoints.md#apiminerspaymentaddresspublickeylimitlimit))
- "limit" query string parameter to /api/pool/blocks (see [API Endpoints](docs/api-endpoints.md#apipoolblockslimitlimit))
- API "query" configuration section to set maximum "limit" query string parameter values (see [Configuration](docs/configuration.md#query))

### Changed
- Improve share processing performance

### Fixed
- Startup when GetWork is disabled

## v2.8.1.0 - 2018-07-17
### Added
- Additional check for possible node sync issue

## v2.8.0.0 - 2018-07-11
### Added
- Notification service reporting
- "notification" service timer configuration option

### Changed
- Include server id as part of event context

### Fixed
- Bug with "enableSelfLogging" when specifying full log file path

## v2.7.0.0 - 2018-07-08
### Added
- "enableSelfLogging" configuration option for additional diagnostic logging
- "includeEventContext" configuration option to include additional information with notifications

## v2.6.0.0 - 2018-07-02
### Added
- "origins" option for CORS support in the API server

### Fixed
- Multi-spend support

## v2.5.0.0 - 2018-06-29
### Added
- "useLegacyDifficultyCheck" to revert to the VeoCL difficulty check
- "registeredOn" and "uptime" server status fields (see [API Endpoints](docs/api-endpoints.md#apiadminserversserverid))

### Fixed
- Default difficulty check, set "useLegacyDifficultyCheck" to 'true' to undo this fix

## v2.4.0.0 - 2018-06-28
### Added
- Public route /api/miners/top to retrieve top miners ranked by accepted/invalid shares, hashrate, or worker count (see [API Endpoints](docs/api-endpoints.md#apiminerstoplimitlimitorderbyorderby))

### Changed
- Improved stale data cleanup performance

### Fixed
- Automatically unregister stale/inactive servers (and their services) after a period of inactivity

## v2.3.1.0 - 2018-06-26
### Changed
- Minor share storage reporting

## v2.3.0.0 - 2018-06-26
### Added
- "maximumInvalidShareBacklog" configuration option (see also event id 6875)
- "maximumShareBacklog" configuration option (see also event id 6750)
- New event ids 6250, 6500, 6750, and 6875 (see [Notifications](docs/configuration.md#notifications) for their descriptions)

## v2.2.0.0 - 2018-06-26
### Added
- 20180626_000.sql database upgrade script (see [Upgrading](docs/upgrading.md))
- Invalid share storage service reporting
- [Service Runtime Data](docs/service-runtime-data.md) document

## v2.1.0.0 - 2018-06-25
### Added
- 20180625_000.sql database upgrade script (see [Upgrading](docs/upgrading.md))
- Data cacher service reporting
- "backup" configuration option for API monitoring of additional Redis instances

## v2.0.0.0 - 2018-06-23
### Added
- 20180623_000.sql database upgrade script (see [Upgrading](docs/upgrading.md))
- Register API server for monitoring

### Changed
- API endpoints used for pool/miner/worker stat retrieval. See the new [API Migration From v1.x to v2.x](docs/api-v1-v2-migration.md) and the updated [API Endpoints](docs/api-endpoints.md) for more information.

## v1.5.2.0 - 2018-06-18
### Changed
- Additional minor improvements to Stratum

### Fixed
- Minor error handling improvement in API

## v1.5.1.0 - 2018-06-18
### Changed
- Minor improvement to Stratum receive

## v1.5.0.0 - 2018-06-17
### Added
- "backlog" option for TCP server backlog size
- "enableLegacyReceive" option for Stratum (see 'Fixed' section below)
- "maximumMessageSize" option for controlling the maximum Stratum message (i.e. one line) size

### Fixed
- Most common causes of Stratum "malformed message" errors. Occasional errors may still arise from out-of-order partial messages, but that scenario is rare and should occur only when a single client sends multiple messages in quick succession (i.e. a few milliseconds apart) on the same connection. This fix can be completely disabled by setting the "enableLegacyReceive" configuration option to 'true'.

## v1.4.0.0 - 2018-06-15
### Added
- "includeStackTrace" option to include stack traces in logs (now excluded by default)

## v1.3.0.0 - 2018-06-14
### Added
- Admin route /api/admin/{ServerId}/config for listing available configuration sections
- Admin route /api/admin/{ServerId}/config/{ConfigSection} for retrieving a single configuration section

## v1.2.1.0 - 2018-06-13
### Added
- 20180612_000.sql database upgrade script (see [Upgrading](docs/upgrading.md))

### Changed
- Payment automatically uses archived shares if necessary

### Fixed
- Missing IsConfirmed field from /api/miners/payment response

## v1.2.0.0 - 2018-06-12
### Added
- "id" option for node configuration
- Multi-spend support at block 26900 via "groupPayments" option
- Override default work route via "route" option

## v1.1.0.0 - 2018-06-10
### Added
- Node failover, share storage, and work retriever service reporting

### Changed
- Some configuration defaults

## v1.0.0.0 - 2018-06-07
### Added
- Initial release